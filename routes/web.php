<?php

use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InfoUserController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ResetController;
use App\Http\Controllers\SessionsController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth'], function () {

    Route::get('/', [HomeController::class, 'home']);
	// Route::get('dashboard', function () {
	// 	return view('dashboard');
	// })->name('dashboard');

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
	Route::get('/hobby-chart-data', [UserController::class, 'getHobbyChartData'])->name('hobby_chart_data');


    Route::get('/logout', [SessionsController::class, 'destroy'])->name('logout');
	Route::get('/user-profile', [InfoUserController::class, 'create']);
	Route::post('/user-profile', [InfoUserController::class, 'store']);
    Route::get('/login', function () {
		return view('dashboard');
	})->name('sign-up');

    // User
    Route::get('/users', [UserController::class, 'index'])->name('users.index');
    Route::get('/users/list', [UserController::class, 'getUsers'])->name('users.list');
    Route::get('/users/list/{id}', [UserController::class, 'getUserById'])->name('user.get');
    Route::get('/users/create', [UserController::class, 'create'])->name('users.create');
    Route::post('/users', [UserController::class, 'store'])->name('users.store');
    Route::get('/users/{user}', [UserController::class, 'show'])->name('users.show');
    Route::get('/users/{user}/edit', [UserController::class, 'edit'])->name('users.edit');
    Route::put('/users/{user}', [UserController::class, 'update'])->name('users.update');
    Route::delete('/users/{user}', [UserController::class, 'destroy'])->name('users.destroy');
	Route::post('/users/{id}/update-photo', [UserController::class, 'updatePhoto'])->name('users.updatePhoto');
    Route::get('/users/datatables/ajax', [UserController::class, 'getDatatableData'])->name('users.datatables.ajax');
    Route::get('/users/organization/datatables/ajax', [UserController::class, 'getOrganizationDatatableData'])->name('users.organization.datatables.ajax');
    Route::get('/users/{user_id}/organization/{organization_id}', [UserController::class, 'organizationDetail'])->name('users.organization_show');
    Route::get('/users/{user_id}/organization', [UserController::class, 'organizationCreate'])->name('users.organization_create');
    Route::delete('/users/{user_id}/organization/{organization_id}', [UserController::class, 'organizationDestroy'])->name('users.organization_destroy');
    Route::get('/users/member/ajax', [UserController::class, 'getUserMember'])->name('users.member.ajax');
    Route::get('/users/company/ajax', [UserController::class, 'getCompanyRelationOption'])->name('fetch.company.options');

	// Organization
    Route::get('/organization', [OrganizationController::class, 'index'])->name('organization');
    Route::get('/organization/user', [OrganizationController::class, 'user'])->name('organization.user');
    Route::get('/organization/user/division', [OrganizationController::class, 'userDivision'])->name('organization.user.division');
    // Route::get('/organization/user/detail', [OrganizationController::class, 'show'])->name('organization.user.show');
	Route::get('/organization/{id}', [OrganizationController::class, 'show'])->name('organization.show');
	Route::delete('/organization/{id}', [OrganizationController::class, 'destroy'])->name('organization.destroy');

});



Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', [RegisterController::class, 'create']);
    Route::post('/register', [RegisterController::class, 'store']);
    Route::get('/login', [SessionsController::class, 'create']);
    Route::post('/session', [SessionsController::class, 'store']);
	Route::get('/login/forgot-password', [ResetController::class, 'create']);
	Route::post('/forgot-password', [ResetController::class, 'sendEmail']);
	Route::get('/reset-password/{token}', [ResetController::class, 'resetPass'])->name('password.reset');
	Route::post('/reset-password', [ChangePasswordController::class, 'changePassword'])->name('password.update');

});

Route::get('/login', function () {
    return view('session/login-session');
})->name('login');
