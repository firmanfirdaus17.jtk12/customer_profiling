@extends('layouts.user_type.auth')

@section('content')

<div class="row">
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
    <div class="card">
      <div class="card-body p-3">
        <div class="row">
          <div class="col-9 col-xl-9 col-lg-9 col-md-9 col-sm-9">
            <div class="numbers">
              <p class="text-sm mb-0 text-capitalize font-weight-bold"></p>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h5 class="font-weight-bolder mb-0 col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <span>Happy Birthday to you:</span>
                    </h5>
                    <h5 class="mb-0 col-xl-4 col-lg-4 col-md-4 col-sm-4 text-end" style="color: #0B516A; font-size: 15px; padding-left:5px"> <!-- Menggunakan ml-auto untuk menjorokkan ke kanan -->
                        <i class="fas fa-calendar-alt" style="margin-right: 5px;"></i>
                        <span id="currentDate"></span>
                    </h5>
                </div>

                @foreach($birthday as $birthday_key => $birthday_value)
                <span class="text-sm font-weight-bolder" style="color: #4AAAC0">
                    - {{ $birthday_value->fullname }} |
                    {{ isset($birthday_value['organizations'][0]['company']['name']) ? $birthday_value['organizations'][0]['company']['name'] : '' }}
                </span><br>
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 text-end">
            <div class="icon icon-shape shadow text-center border-radius-md" style="background-color: #00526B">
                <i class="fas fa-birthday-cake text-lg opacity-10" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 mb-4">
    <div class="card">
      <div class="card-body p-3">
        <div class="row">
          <div class="col-9">
            <div class="numbers">
              <p class="text-sm mb-0 text-capitalize font-weight-bold"></p>
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h5 class="font-weight-bolder mb-0 col-xl-8 col-lg-8 col-md-8 col-sm-8">
                        <span>Upcoming Birthday:</span>
                    </h5>
                    <h5 class="mb-0 col-xl-4 col-lg-4 col-md-4 col-sm-4 text-end" style="color: #0B516A; font-size: 15px; padding-left:5px"> <!-- Menggunakan ml-auto untuk menjorokkan ke kanan -->
                        <i class="fas fa-calendar-alt" style="margin-right: 5px;"></i>
                        <span id="currentDateNext"></span>
                    </h5>
                </div>

                @foreach($next_birthday as $next_birthday_key => $next_birthday_value)
                <span class="text-sm font-weight-bolder" style="color: #4AAAC0">
                    - {{ $next_birthday_value->fullname }} |
                    {{ isset($next_birthday_value['organizations'][0]['company']['name']) ? $next_birthday_value['organizations'][0]['company']['name'] : '' }}
                </span><br>
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-3 text-end">
            <div class="icon icon-shape shadow text-center border-radius-md" style="background-color: #00526B">
                <i class="fas fa-birthday-cake text-lg opacity-10" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row mt-4">
  <div class="col-lg-6 col-md-12 mb-4">
    <div class="card" style="background-color: #ffffff">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="d-flex flex-column h-100">
              <h5 class="font-weight-bolder">User's Hobby</h5>
              <div class="card mb-3">
                <div class="card-body p-3">
                  <div class="chart">
                    <canvas id="pie-chart-hobby" class="chart-canvas content-dashboard" width="100%"></canvas>
                  </div>
                </div>
              </div>
              <a class="text-body text-sm font-weight-bold mb-0 icon-move-right mt-auto" href="javascript:;">
                {{-- Read More
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i> --}}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-6 col-md-12 mb-4">
    <div class="card" style="background-color: #ffffff">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="d-flex flex-column h-100">
              <h5 class="font-weight-bolder">User by Organization</h5>
              <div class="card mb-3">
                <div class="card-body p-3">
                  <div class="chart" style="overflow-x: auto; min-width: 50px;">
                    <canvas id="bar-chart-company" class="chart-canvas content-dashboard" width="100%"></canvas>
                  </div>
                </div>
              </div>
              <a class="text-body text-sm font-weight-bold mb-0 icon-move-right mt-auto" href="javascript:;">
                {{-- Read More
                <i class="fas fa-arrow-right text-sm ms-1" aria-hidden="true"></i> --}}
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row my-4">
    <div class="col-12">
        <div class="card mb-4">
        <div class="card-header" style="background-color: #4AAAC0;">
            <div class="d-flex flex-row justify-content-between">
                <h5 class="text-white font-weight-bolder">All Users</h5>
            </div>
        </div>
        <div class="card-header" style="margin-top:0 ; padding-top:0; margin-bottom:0 ; padding-bottom:0">
            <div class="row d-flex align-items-center justify-content-center" style="min-height: 100px;">
                <!-- Add Button -->
                <div class="col-12 col-md-3 col-lg-2 mb-2 mb-md-0 d-flex align-items-center justify-content-center" style="padding-top: 10px">
                    <a href="{{ url('users/create') }}" class="btn btn-sm btn-dashboard w-100 d-flex align-items-center justify-content-center" style="background-color: #00526B; color: white; height: 40px;">
                        <i class="fa fa-plus text-btn-dashboard me-2"></i><span>Add</span>
                    </a>
                </div>              
                
                <!-- Search Controls -->
                <div class="col-12 col-md-9 col-lg-10">
                    <div class="row g-2">
                        <!-- Company Dropdown -->
                        <div class="offset-lg-1 col-lg-3 col-md-4 col-sm-6 d-flex align-items-center">
                            <label for="searchCompany" class="form-label me-2 mb-0">Company</label>
                            <select id="searchCompany" class="form-control form-control-sm select2 w-100">
                                <option value="">Choose Company</option>
                                @foreach($company_options as $company_value => $company_label)
                                    <option value="{{$company_value}}">{{$company_label}}</option>
                                @endforeach
                            </select>
                        </div>
                        
                        <!-- Area Dropdown -->
                        <div class="col-lg-3 col-md-4 col-sm-6 d-flex align-items-center">
                            <label for="searchArea" class="form-label me-2 mb-0">Area</label>
                            <select id="searchArea" class="form-control form-control-sm select2 w-100">
                                <option value="">Choose Area</option>
                                @foreach($area_options as $area_value => $area_label)
                                    <option value="{{$area_value}}">{{$area_label}}</option>
                                @endforeach
                            </select>
                        </div>
        
                        <!-- Search Input -->
                        <div class="col-lg-3 col-md-4 col-sm-6" style="padding-top: 15px">
                            <input type="text" id="searchInput" class="form-control form-control-sm" placeholder="Search...">
                        </div>
        
                        <!-- Search Button -->
                        <div class="col-lg-2 col-md-4 col-sm-6" style="padding-top: 10px">
                            <button class="btn btn-sm btn-dashboard w-100" type="button" style="background-color: #00526B; color: white; display: flex; align-items: center; justify-content: center; height: 40px;" onclick="searchData()">
                                <span class="text-btn-dashboard">Search</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="card-body">
            <div class="table-responsive">
            <table class="table align-items-center">
                <thead>
                <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">NO</th>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">NAMA</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">COMPANY</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">AREA</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">SUB AREA</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">CONTACT PERSON</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">EMAIL</th>
                    <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ACTION</th>
                </tr>
                </thead>
                <tbody>
                <!-- Table rows will be populated dynamically using JavaScript -->
                </tbody>
            </table>
            <div class="pagination-links">
                {{ $users->links('pagination::bootstrap-5') }}
            </div>
            </div>
        </div>
    </div>
  </div>
</div>

@endsection


@push('dashboard')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />

    <!-- Your script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <style>
        .select2-container--default .select2-selection--single {
            height: auto !important;
        }
    </style>
<script>
    let temporarySelect = [];
    $(document).ready(function() {
        $('#searchCompany').select2();
        $('#searchArea').select2();
        $('[data-bs-toggle="tooltip"]').tooltip();

        // Ajax request to get data for the chart
        $.ajax({
            url: '{{ route("hobby_chart_data") }}',
            method: 'GET',
            success: function(response) {
                var labels = [];
                var data = [];

                // Extract labels and data from the response
                response.forEach(function(item) {
                    labels.push(item.label);
                    data.push(item.value);
                });

                // Chart.js code to create a pie chart
                var ctx = document.getElementById('pie-chart-hobby').getContext('2d');
                var backgroundColors = []; // Array to store random background colors

                // Generate random background colors for each data point
                for (var i = 0; i < data.length; i++) {
                    var randomColor = 'rgba(' + Math.floor(Math.random() * 256) + ',' + Math.floor(Math.random() * 256) + ',' + Math.floor(Math.random() * 256) + ', 0.7)';
                    backgroundColors.push(randomColor);
                }

                var myPieChart = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            data: data,
                            backgroundColor: backgroundColors, // Use random background colors
                            borderWidth: 0 // Set border width to 0 to remove border
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        layout: {
                            padding: {
                                top: 10,
                                bottom: function(context) {
                                            var width = context.chart.width;
                                            var bottom = 100;
                                            if (width < 768 ) {
                                                bottom = 50;
                                            }
                                            return bottom;
                                }
                            }
                        },
                        plugins: {
                            legend: {
                                display: true,
                                position: 'bottom',
                                labels: {
                                    font: {
                                        size: function(context) {
                                            var width = context.chart.width;
                                            var sizeFont = 14;
                                            if (width < 470 ) {
                                                sizeFont = 10;
                                            }
                                            if (width < 400 ) {
                                                sizeFont = 7;
                                            }
                                            if (width < 350 ) {
                                                sizeFont = 6;
                                            }
                                            return sizeFont;
                                        }
                                    }
                                }
                            }
                        }
                    }
                });

            }
        });

        const companyNames = <?php echo json_encode($company_names); ?>;
        const userCounts = <?php echo json_encode($company_count); ?>;

        // Custom plugin to split long labels into multiple lines at spaces
        const multiLinePlugin = {
            id: 'multiLinePlugin',
            beforeInit: function(chart) {
                chart.data.labels.forEach(function(value, index, array) {
                    let formattedLabel = [];
                    let maxLineLength = 30; // Maximum characters per line

                    while (value.length > 0) {
                        let cutOffIndex = Math.min(maxLineLength, value.length);
                        let part = value.substring(0, cutOffIndex);

                        // Ensure we split at a space
                        if (cutOffIndex < value.length) {
                            let lastSpaceIndex = part.lastIndexOf(' ');
                            if (lastSpaceIndex !== -1) {
                                cutOffIndex = lastSpaceIndex;
                            }
                        }

                        formattedLabel.push(value.substring(0, cutOffIndex).trim());
                        value = value.substring(cutOffIndex).trim();
                    }
                    array[index] = formattedLabel;
                });
            }
        };

        // Get the context of the canvas element we want to select
        const ctx = document.getElementById('bar-chart-company').getContext('2d');

        // Create a new bar chart
        const barChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: companyNames,
                datasets: [{
                    label: 'Number of Users',
                    data: userCounts,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            },
            options: {
                indexAxis: 'y',
                scales: {
                    y: {
                        beginAtZero: true,
                        ticks: {
                            font: {
                                size: function(context) {
                                    var width = context.chart.width;
                                    var sizeFont = 10;
                                    if (width < 470) {
                                        sizeFont = 8;
                                    }
                                    if (width < 400) {
                                        sizeFont = 7;
                                    }
                                    if (width < 350) {
                                        sizeFont = 6;
                                    }
                                    return sizeFont;
                                }
                            },
                            // Add padding between labels
                            padding: 10,
                        }
                    },
                    x: {
                        beginAtZero: true,
                        ticks: {
                            font: {
                                size: function(context) {
                                    var width = context.chart.width;
                                    var sizeFont = 10;
                                    if (width < 470) {
                                        sizeFont = 8;
                                    }
                                    if (width < 400) {
                                        sizeFont = 7;
                                    }
                                    if (width < 350) {
                                        sizeFont = 6;
                                    }
                                    return sizeFont;
                                }
                            },
                            stepSize: 5 // Ensures only integer values on the x-axis
                        }
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    zoom: {
                        pan: {
                            enabled: true,
                            mode: 'x'
                        },
                        zoom: {
                            enabled: true,
                            mode: 'x',
                            rangeMin: {
                                x: 0
                            },
                            rangeMax: {
                                x: 10
                            }
                        }
                    }
                }
            },
            plugins: [multiLinePlugin]
        });

        function getCurrentDate() {
            const date = new Date();
            const options = { day: 'numeric', month: 'short', year: 'numeric' };
            return date.toLocaleDateString('id-ID', options); // Using "id-ID" for Indonesian format
        }

        function getCurrentDateNext() {
            const date = new Date();
            // Add one day to the current date
            date.setDate(date.getDate() + 1);
            const options = { day: 'numeric', month: 'short', year: 'numeric' };
            return date.toLocaleDateString('id-ID', options); // Using "id-ID" for Indonesian format
        }

        // Set the current date into the span element
        document.getElementById('currentDate').textContent = getCurrentDate();
        document.getElementById('currentDateNext').textContent = getCurrentDateNext();

        $('#searchCompany').change(function() {
            var companyId = $(this).val(); // Get the selected company ID
            companyChoosen = companyId;
            temporarySelect['searchCompany'] = $(this).val();
            // Trigger AJAX request to fetch options for group company, area, and sub area
            $.ajax({
                url: "{{ route('fetch.company.options') }}", // Replace with your route for fetching options
                method: 'GET',
                data: { company_id: companyId },
                success: function(response) {
                    // Update options for group company select
                    updateSelectOptions(response.area_options, '#searchArea', 0, null);
                    // Initialize select2 for the newly added select element
                    $('#searchCompany').select2();
                    $('#searchArea').select2();
                    $('#searchSubarea').select2();
                },
                error: function(xhr, status, error) {
                    console.error(error); // Handle error response
                }
            });
        });
    });

    function updateSelectOptions(options, selectId, user, userId) {
        var selectElement = $(selectId);
        selectElement.empty(); // Clear existing options
        var textOption = '';

        if (user == 1) {
            $.each(options, function(key, value) {
                selectElement.append($('<option>', { value: key, text: value, selected: 'selected' }));
                selectElement.prop('disabled', true);
            });
        } else if(user == 0) {
            if (selectId == '#searchArea') {
                textOption = 'Area';
            } else if (selectId == '#searchSubarea') {
                textOption = 'Divisi/Sub Area';
            } else if (selectId == '#searchUser') {
                textOption = 'User';
            }
            selectElement.append($('<option>', { value: '', text: 'Choose '+textOption })); // Add default option
            // Add options from the response
            $.each(options, function(key, value) {
                selectElement.append($('<option>', { value: key, text: value }));
            });
            selectElement.prop('disabled', false);
        }
    }
    
    $('#searchCompany').select2();
    $('#searchArea').select2();
    fetchData(); // Call the function to fetch and populate data when the page loads

    // Event listener for pagination links (delegate to a parent element)
    $(document).on('click', '.pagination a.page-link', function(event) {
        event.preventDefault();
        let url = $(this).attr('href');
        fetchData(url); // Fetch data for the clicked pagination link
    });

    function fetchData(url = null) {
        let requestUrl = url || "{{ route('users.datatables.ajax') }}"; // Use the provided URL or the default route
        $.ajax({
            url: requestUrl,
            method: 'GET',
            data: {
                search: $('#searchInput').val(), // Get the value of the search input field
                searchCompany: $('#searchCompany').val(),
                searchArea: $('#searchArea').val()
            },
            success: function(response) {
                updateTable(response.data, response.req_page); // Call the function to update the table with received data
                updatePagination(response.pagination); // Call the function to update pagination links
            },
            error: function(xhr, status, error) {
                console.error(error); // Handle errors if any
            }
        });
    }

    function searchData() {
        fetchData(); // Call the fetchData function to fetch data based on the search query
    }

    $('#searchInput').keypress(function(event) {
        // Check if the Enter key is pressed (key code 13)
        if (event.keyCode === 13) {
            // Call the searchData function to perform search
            searchData();
        }
    });

    // Function to update the table with received data
   function updateTable(data, currentPage) {
    // Clear the existing table rows
    $('tbody').empty();

    // Calculate the starting counter value
    let startCounter = (currentPage - 1) * 10 + 1;

    // Loop through the received data and append rows to the table
    $.each(data, function(index, item) {
        $('tbody').append(`
            <tr>
                <td class="ps-4">${startCounter++}</td>
                <td>${item.NAMA !== null ? item.NAMA : ''}</td>
                <td class="text-center">${item.COMPANY !== null ? item.COMPANY : ''}</td>
                <td class="text-center">${item.AREA !== null ? item.AREA : ''}</td>
                <td class="text-center">${item.SUB_AREA !== null ? item.SUB_AREA : ''}</td>
                <td class="text-center">${item.PHONE !== null ? item.PHONE : ''}</td>
                <td class="text-center">${item.EMAIL !== null ? item.EMAIL : ''}</td>
                <td class="text-center">
                    ${item.ACTION !== null ? item.ACTION : ''}
                </td>
            </tr>
        `);
    });
}


    // Function to update pagination links
    function updatePagination(pagination) {
        $('.pagination-links').html(pagination); // Update the pagination links
    }
    $(document).on('click', '.delete-user', function(e) {
        e.preventDefault();

        var deleteUrl = $(this).data('delete-url');
        var csrfToken = $('meta[name="csrf-token"]').attr('content'); // Get CSRF token from meta tag

        // Make AJAX request with CSRF token included in headers
        $.ajax({
            url: deleteUrl,
            method: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include CSRF token in headers
            },
            success: function(response) {
                fetchData();
            },
            error: function(xhr, status, error) {
                // Handle error response
                console.error(error);
            }
        });
    });

    $(document).on('click', '.delete-organization', function(e) {
        e.preventDefault();

        var deleteUrl = $(this).data('delete-url');
        var csrfToken = $('meta[name="csrf-token"]').attr('content'); // Get CSRF token from meta tag
        console.log(deleteUrl);
        // Make AJAX request with CSRF token included in headers
        $.ajax({
            url: deleteUrl,
            method: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': csrfToken // Include CSRF token in headers
            },
            success: function(response) {
                fetchData();
            },
            error: function(xhr, status, error) {
                // Handle error response
                console.error(error);
            }
        });
    });
  </script>
@endpush

