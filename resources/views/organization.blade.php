@extends('layouts.user_type.auth')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2">
                <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
                    <div class="col-12">
                        <h5 class="text-white font-weight-bolder dynamic-org-size">Organization Structure</h5>
                    </div>
                </div>
                <div class="mx-4">
                    <div class="row col-12 pt-3">
                        <div class="row">
                            <div class=" col-lg-2 col-sm-6 pt-sm-1">
                                <!-- Select2 input for searching users -->
                                <select id="searchUser" class="form-control select2" style="width: 100%;" onchange="updateChart(this.value)">
                                    <option value="">Search User</option>
                                    <!-- Populate options dynamically from $users -->
                                    @if (isset($user_id) && isset($user_fullname))
                                        <option value="{{$user_id}}">{{$user_fullname}}</option>
                                    @endif
                                </select>
                            </div>
                            <div class=" col-lg-2 col-sm-6 pt-sm-1">
                                <!-- Select2 input for Instansi -->
                                <select id="searchCompany" class="form-control select2" style="width: 100%;">
                                    <option value="">Choose Company</option>
                                    <!-- Populate options dynamically -->
                                    @foreach($company_options as $company_value => $company_label)
                                        <option value="{{$company_value}}">{{$company_label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-lg-2 col-sm-6 pt-sm-1">
                                <!-- Select2 input for Divisi -->
                                <select id="searchArea" class="form-control select2" style="width: 100%;" disabled>
                                    <option value="">Choose Area</option>
                                    <!-- Populate options dynamically -->
                                    @foreach($area_options as $area_value => $area_label)
                                        <option value="{{$area_value}}">{{$area_label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-lg-2 col-sm-6 pt-sm-1">
                                <!-- Select2 input for Divisi -->
                                <select id="searchSubarea" class="form-control select2" style="width: 100%;" disabled>
                                    <option value="">Choose Divisi/Sub Area</option>
                                    <!-- Populate options dynamically -->
                                    @foreach($sub_area_options as $sub_area_value => $sub_area_label)
                                        <option value="{{$sub_area_value}}">{{$sub_area_label}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-lg-2 col-sm-6 pt-sm-1" style="margin-top: 3px">
                                <button id="clearButton" class="btn btn-sm" style="background-color: #00526B; color: white;">Clear</button>
                            </div>
                        </div>
                        <div style="width:100%; height:700px;" id="tree"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loadingOverlay" style="display:none; position:fixed; top:50%; left:50%; transform:translate(-50%, -50%); background:#50A9BF; padding:30px; border-radius:10px; z-index:9999; color:white;">
    <div class="loading-content">
        <div class="spinner"></div>
        <div style="margin-top:20px; font-size:18px;">Loading...</div>
    </div>
</div>
@endsection

@push('organization')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Your script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <style>
        .select2-container--default .select2-selection--single {
            height: auto !important;
        }
        #loadingOverlay {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 200px; /* Sesuaikan ukuran sesuai kebutuhan */
            height: 200px; /* Sesuaikan ukuran sesuai kebutuhan */
            text-align: center;
        }

        .spinner {
            border: 8px solid rgba(255, 255, 255, 0.3); /* Warna lingkaran latar belakang */
            border-top: 8px solid white; /* Warna lingkaran utama */
            border-radius: 50%;
            width: 50px;
            height: 50px;
            animation: spin 1s linear infinite;
        }

        /* Animasi lingkaran berputar */
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .loading-content {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 100%;
        }

    </style>

    <!-- Your script -->
    <script>
        let temporarySelect = [];

        $('.select2').select2();
        $('#searchUser').select2({
            ajax: {
                url: '{{ route('users.list') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.results,
                        pagination: {
                            more: data.pagination.more
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Select a user',
            minimumInputLength: 1,
        });
        @if(isset($user_id) && isset($user_fullname))
            // Create a new option with the user's id and fullname
            var newOption = new Option('{{ $user_fullname }}', '{{ $user_id }}', true, true);
            // Append it to the select element
            $('#searchUser').append(newOption).trigger('change');
            // Optionally, trigger a change event to update any dependent elements
            $('#searchUser').trigger({
                type: 'select2:select',
                params: {
                    data: {
                        id: '{{ $user_id }}',
                        text: '{{ $user_fullname }}'
                    }
                }
            });
        @endif
        document.getElementById('clearButton').addEventListener('click', function() {
            window.location.href = "{{ url('organization') }}";
        });
        var userChoosen = "";
        var companyChoosen = "";
        var areaChoosen = "";
        var subareaChoosen = "";

        // Function to update the chart based on the selected user
        function updateChart(selectedUserId) {
            // Perform AJAX request to fetch user information
            $.ajax({
                url: "{{ route('organization.user') }}",
                method: 'GET',
                data: { userId: selectedUserId },
                success: function(response) {
                    // Remove all existing nodes from the chart
                    var dataNodes = [];
                    $.each(response.users, function(i, userInfo) {
                        dataNodes.push({
                            userId: userInfo.userId,
                            id: userInfo.id,
                            pid: userInfo.parentId,
                            Name: userInfo.name,
                            Company: userInfo.company,
                            Area: userInfo.area,
                            Subarea: userInfo.subarea,
                            Position: userInfo.position,
                            Email: userInfo.email,
                            Phone: userInfo.phone,
                            img: userInfo.photo
                        });
                    });
                    // Ensure the template object exists
                    OrgChart.templates.myTemplate = Object.assign({}, OrgChart.templates.ana);
                    OrgChart.templates.myTemplate.minus =
                        '<circle cx="15" cy="5" r="13" fill="#fff" stroke="#388A94" stroke-width="1"></circle>' +
                        '<line x1="8" y1="5" x2="22" y2="5" stroke-width="1" stroke="#388A94"></line>';
                    OrgChart.templates.myTemplate.plus =
                        '<circle cx="15" cy="5" r="13" fill="#ffffff" stroke="#388A94" stroke-width="1"></circle>' +
                        '<line x1="8" y1="5" x2="22" y2="5" stroke-width="1" stroke="#388A94"></line><line x1="15" y1="-2" x2="15" y2="12" stroke-width="1" stroke="#388A94"></line>';

                    OrgChart.templates.myTemplate.expandCollapseSize = 30;
                    // OrgChart.templates.myTemplate.field_0 = '<text width="230" class="field_0"  style="font-size: 18px;" fill="#ffffff" x="125" y="95" text-anchor="middle">{val}</text>';
                    // OrgChart.templates.myTemplate.field_1 = '<text width="130" text-overflow="multiline" class="field_1"  style="font-size: 14px;" fill="#ffffff" x="230" y="30" text-anchor="end">{val}</text>';
                   // Initialize the chart
                    var chart = new OrgChart(document.getElementById("tree"), {
                        template: "myTemplate",
                        mouseScrool: OrgChart.action.scroll,
                        defs: "",
                        linkAdjuster: {
                            fromX: 0,
                            fromY: 0,
                            toX: 0,
                            toY: 0
                        },
                        ripple: {
                            radius: 0,
                            color: "#e6e6e6",
                            rect: null
                        },
                        editForm: {
                            buttons: {
                                map: {
                                    icon: '<i class="fas fa-eye" style="font-size: 24px;color:white; margin-top:13px"></i>',
                                    text: 'User Profile'
                                },
                                edit: null,
                                share: null,
                                pdf: null,
                                remove: null
                            }
                        },
                        expandCollapseSize: 30,
                        size: [250, 120],
                        enableSearch: false,
                        nodeBinding: {
                            field_0: "Name",
                            field_1: "Company",
                            field_2: "Area",
                            field_3: "Subarea",
                            field_4: "Position",
                            field_5: "Email",
                            field_6: "Phone",
                            img_0: "img"
                        },
                        nodes: dataNodes
                    });

                    chart.editUI.on('button-click', function (sender, args) {
                        if (args.name == 'map') {
                            var data = chart.get(args.nodeId);
                            sessionStorage.setItem('tabName', 'profile');
                            window.location.href = "{{ url('users') }}/" + data.userId;
                        }
                    });

                },
                error: function(xhr, status, error) {
                    console.error('Error fetching user information:', error);
                }
            });
        }

        function updateChartDivision(subAreaId) {
            // Show loading overlay
            document.getElementById("loadingOverlay").style.display = "block";

            // Perform AJAX request to fetch user information
            $.ajax({
                url: "{{ route('organization.user.division') }}",
                method: 'GET',
                data: {
                    companyId: temporarySelect['searchCompany'],
                    areaId: temporarySelect['searchArea'],
                    subAreaId: subAreaId,
                },
                success: function(response) {
                    if (!response || response.length === 0) {
                        // If the response is empty, show the message and hide loading overlay
                        document.getElementById("tree").innerHTML = "<div style='text-align:center; padding-top:50px; font-size:18px; color:#ff0000;'>Data user is empty</div>";
                        document.getElementById("loadingOverlay").style.display = "none"; // Hide loading overlay
                        return; // Exit the function here if response is empty
                    }

                    // OrgChart templates setup
                    OrgChart.templates.myTemplate = Object.assign({}, OrgChart.templates.ana);
                    OrgChart.templates.myTemplate.minus =
                        '<circle cx="15" cy="5" r="13" fill="#fff" stroke="#388A94" stroke-width="1"></circle>' +
                        '<line x1="8" y1="5" x2="22" y2="5" stroke-width="1" stroke="#388A94"></line>';
                    OrgChart.templates.myTemplate.plus =
                        '<circle cx="15" cy="5" r="13" fill="#ffffff" stroke="#388A94" stroke-width="1"></circle>' +
                        '<line x1="8" y1="5" x2="22" y2="5" stroke-width="1" stroke="#388A94"></line><line x1="15" y1="-2" x2="15" y2="12" stroke-width="1" stroke="#388A94"></line>';
                    OrgChart.templates.myTemplate.expandCollapseSize = 30;

                    // Initialize the chart
                    var chart = new OrgChart(document.getElementById("tree"), {
                        template: "myTemplate",
                        mouseScrool: OrgChart.action.scroll,
                        linkAdjuster: {
                            fromX: 0,
                            fromY: 0,
                            toX: 0,
                            toY: 0
                        },
                        ripple: {
                            radius: 0,
                            color: "#e6e6e6",
                            rect: null
                        },
                        editForm: {
                            buttons: {
                                map: {
                                    icon: '<i class="fas fa-eye" style="font-size: 24px;color:white; margin-top:13px"></i>',
                                    text: 'User Profile'
                                },
                                edit: null,
                                share: null,
                                pdf: null,
                                remove: null
                            }
                        },
                        expandCollapseSize: 30,
                        size: [250, 120],
                        enableSearch: false,
                        nodeBinding: {
                            field_0: "Name",
                            field_1: "Company",
                            field_2: "Area",
                            field_3: "Subarea",
                            field_4: "Position",
                            field_5: "Email",
                            field_6: "Phone",
                            img_0: "img"
                        },
                        nodes: response
                    });

                    // Hide loading overlay once the chart is built
                    chart.on('render', function () {
                        document.getElementById("loadingOverlay").style.display = "none"; // Hide loading overlay
                    });

                    chart.editUI.on('button-click', function (sender, args) {
                        if (args.name == 'map') {
                            var data = chart.get(args.nodeId);
                            sessionStorage.setItem('tabName', 'profile');
                            window.location.href = "{{ url('users') }}/" + data.userId;
                        }
                    });
                },
                error: function(xhr, status, error) {
                    console.error('Error fetching user information:', error);
                    document.getElementById("loadingOverlay").style.display = "none"; // Hide loading overlay in case of error
                }
            });
        }


        function updateAjaxUser(userId) {
            // Trigger AJAX request to fetch options for group company, area, and sub area
            $.ajax({
                url: "{{ route('fetch.company.options') }}", // Replace with your route for fetching options
                method: 'GET',
                data: { user_id: userId },
                success: function(response) {
                    // Update options for company select
                    updateSelectOptions(response.company_options, '#searchCompany', 1, userId);
                    // Update options for area select
                    updateSelectOptions(response.area_options, '#searchArea', 1, userId);
                    // Update options for sub area select
                    updateSelectOptions(response.sub_area_options, '#searchSubarea', 1, userId);
                    // Initialize select2 for the newly added select element
                    $('#searchCompany').select2();
                    $('#searchArea').select2();
                    $('#searchSubarea').select2();
                },
                error: function(xhr, status, error) {
                    $('#searchCompany').empty().select2();
                    $('#searchArea').empty().select2();
                    $('#searchSubarea').empty().select2();
                }
            });
        }

        function updateSelectOptions(options, selectId, user, userId) {
            var selectElement = $(selectId);
            selectElement.empty(); // Clear existing options
            var textOption = '';

            if (user == 1) {
                $.each(options, function(key, value) {
                    selectElement.append($('<option>', { value: key, text: value, selected: 'selected' }));
                    selectElement.prop('disabled', true);
                });
            } else if(user == 0) {
                if (selectId == '#searchArea') {
                    textOption = 'Area';
                } else if (selectId == '#searchSubarea') {
                    textOption = 'Divisi/Sub Area';
                } else if (selectId == '#searchUser') {
                    textOption = 'User';
                }
                selectElement.append($('<option>', { value: '', text: 'Choose '+textOption })); // Add default option
                // Add options from the response
                $.each(options, function(key, value) {
                    selectElement.append($('<option>', { value: key, text: value }));
                });
                selectElement.prop('disabled', false);
            }
        }
        // Check if the user_id variable exists and call updateChart if it does
        @if(isset($user_id))
            var userId = {{ $user_id }};
            updateChart(userId);
            updateAjaxUser(userId);
        @endif

        // Initialize Select2 for the searchUser, searchCompany, and searchSubarea input fields
        $(document).ready(function() {

            $('#searchCompany').select2({
                placeholder: 'Choose Company',
                // Additional options can be added here if needed
            });

            $('#searchArea').select2({
                placeholder: 'Choose Area',
                // Additional options can be added here if needed
            });

            $('#searchSubarea').select2({
                placeholder: 'Choose Divisi/Sub Area',
                // Additional options can be added here if needed
            });

            $('#searchUser').change(function() {
                var userId = $(this).val(); // Get the selected company ID
                updateAjaxUser(userId);
                temporarySelect['searchUser'] = $(this).val();
            });

            $('#searchCompany').change(function() {
                var companyId = $(this).val(); // Get the selected company ID
                companyChoosen = companyId;
                temporarySelect['searchCompany'] = $(this).val();
                // Trigger AJAX request to fetch options for group company, area, and sub area
                $.ajax({
                    url: "{{ route('fetch.company.options') }}", // Replace with your route for fetching options
                    method: 'GET',
                    data: { company_id: companyId },
                    success: function(response) {
                        // Update options for group company select
                        updateSelectOptions(response.area_options, '#searchArea', 0, null);
                        // Initialize select2 for the newly added select element
                        $('#searchCompany').select2();
                        $('#searchArea').select2();
                        $('#searchSubarea').select2();
                    },
                    error: function(xhr, status, error) {
                        console.error(error); // Handle error response
                    }
                });
            });

            $('#searchArea').change(function() {
                var areaId = $(this).val(); // Get the selected company ID
                areaChoosen = areaId;
                temporarySelect['searchArea'] = $(this).val();
                // Trigger AJAX request to fetch options for group company, area, and sub area
                $.ajax({
                    url: "{{ route('fetch.company.options') }}", // Replace with your route for fetching options
                    method: 'GET',
                    data: {
                            company_id: companyChoosen,
                            area_id: areaId },
                    success: function(response) {
                        // Update options for group company select
                        updateSelectOptions(response.sub_area_options, '#searchSubarea', 0, null);
                        // Initialize select2 for the newly added select element
                        $('#searchCompany').select2();
                        $('#searchArea').select2();
                        $('#searchSubarea').select2();
                    },
                    error: function(xhr, status, error) {
                        console.error(error); // Handle error response
                    }
                });
            });

            $('#searchSubarea').change(function() {
                var subAreaId = $(this).val(); // Get the selected company ID
                temporarySelect['searchSubarea'] = $(this).val();
                updateChartDivision($(this).val());
                // Trigger AJAX request to fetch options for group company, area, and sub area
                $.ajax({
                    url: "{{ route('fetch.company.options') }}", // Replace with your route for fetching options
                    method: 'GET',
                    data: {
                            company_id: companyChoosen,
                            area_id: areaChoosen,
                            sub_area_id: subAreaId},
                    success: function(response) {
                        // Update options for user select
                        updateSelectOptions(response.user_options, '#searchUser', 0, null);
                        // Initialize select2 for the newly added select element
                        $('#searchCompany').select2();
                        $('#searchArea').select2();
                        $('#searchSubarea').select2();
                    },
                    error: function(xhr, status, error) {
                        console.error(error); // Handle error response
                    }
                });
            });

            $('#searchUser').select2({
                ajax: {
                    url: '{{ route('users.list') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term, // search term
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.results,
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Select User',
                minimumInputLength: 1,
            });
        });


    </script>
@endpush
