@extends('layouts.user_type.auth')

  @section('content')
<div style="margin-left: 20px">
    <div class="d-flex flex-row justify-content-between">
        <div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" >
                    <a class="nav-link  badge bg-gradient-info" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true" style="background-color: #0D7EA2; height: 50px; width:100px; border-bottom-left-radius: 0px; border-bottom-right-radius: 0px;">
                        <h6 class="text-white font-weight-bolder pt-1">Profile</h6>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active badge bg-gradient-info" id="personality-tab" data-bs-toggle="tab" href="#personality" role="tab" aria-controls="personality" aria-selected="false" style="background-color: #0D7EA2; height: 50px; width:130px;  border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                        <h6 class="text-white font-weight-bolder  pt-1">Personality</h6>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link badge bg-gradient-info" id="family-tab" data-bs-toggle="tab" href="#family" role="tab" aria-controls="family" aria-selected="false" style="background-color: #0D7EA2; height: 50px; width:100px;  border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                        <h6 class="text-white font-weight-bolder  pt-1">Family</h6>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link badge bg-gradient-info" id="organization-tab" data-bs-toggle="tab" href="#organization" role="tab" aria-controls="organization" aria-selected="false" style="background-color: #0D7EA2; height: 50px; width:150px;  border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                        <h6 class="text-white font-weight-bolder  pt-1">Organization</h6>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link badge bg-gradient-info" id="social_media-tab" data-bs-toggle="tab" href="#social_media" role="tab" aria-controls="social_media" aria-selected="false" style="background-color: #0D7EA2; height: 50px; width:190px;  border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                        <h6 class="text-white font-weight-bolder  pt-1">Personal Account</h6>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                        <!-- User form inputs -->
                        <form id="userForm">
                            <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
                                <div class="col-7">
                                    <h5 class="text-white font-weight-bolder">Personal Profile</h5>
                                </div>
                                <div class="col-5">
                                    @if(session('success'))
                                        <div class="alert alert-success">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    @if(session('failed'))
                                        <div class="alert alert-failed">
                                            {{ session('failed') }}
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="mx-4">
                                <div class="row col-12">
                                    <div class="row my-4 col-3 mx-auto justify-content-center" >
                                        <div class="row card col-3 d-flex justify-content-center align-items-center" style="width: 250px; height: 250px; box-shadow: 100px">
                                            <div style="width: 150px; height: 150px; display: flex; justify-content: center; align-items: center;">
                                                <img src="../assets/img/people.png" class="navbar-brand-img h-100" alt="..." style="">

                                            </div>
                                            <div class="d-flex justify-content-center align-items-center">
                                                Upload here
                                            </div>
                                        </div>
                                    </div>
                                    <div class="my-4 col-7">
                                        <form class="form-material" role="form" method="put" action="{{ route('users.update', $user->id) }}" autocomplete="off">
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="PUT">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right">
                                                        <strong>Full Name :</strong>
                                                        <span style="color: red;">*</span>
                                                    </label>
                                                    <div class="col-lg-11">
                                                        <input name="fullname" type="text" value="{{$user->fullname}}" class="form-control" required disabled>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Nick Name</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="nickname" type="text" value="{{$user->nickname}}" class="form-control" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Status</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <select name="status_marital" class="form-control select2" style="width: 100%;" disabled>
                                                            <option value="">Choose Status</option>
                                                            <option value="single" {{$user->status_marital == 'single' ? 'selected' : ''}}>Single</option>
                                                            <option value="married" {{$user->status_marital == 'married' ? 'selected' : ''}}>Married</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Religion</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <select name="religion" class="form-control select2" style="width: 100%;" disabled>
                                                            <option value="">Choose Religion</option>
                                                            <option value="islam" {{$user->religion == 'islam' ? 'selected' : ''}}>Islam</option>
                                                            <option value="kristen_protestan" {{$user->religion == 'kristen_protestan' ? 'selected' : ''}}>Protestan</option>
                                                            <option value="kristen_katolik" {{$user->religion == 'kristen_katolik' ? 'selected' : ''}}>Katolik</option>
                                                            <option value="hindu" {{$user->religion == 'hindu' ? 'selected' : ''}}>Hindu</option>
                                                            <option value="buddha" {{$user->religion == 'buddha' ? 'selected' : ''}}>Buddha</option>
                                                            <option value="khonghucu" {{$user->religion == 'khonghucu' ? 'selected' : ''}}>Khonghucu</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Gender</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <select name="gender" class="form-control select2" style="width: 100%;" disabled>
                                                            <option value="">Choose Gender</option>
                                                            <option value="male" {{$user->gender == 'male' ? 'selected' : ''}}>Male</option>
                                                            <option value="female" {{$user->gender == 'female' ? 'selected' : ''}}>Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Ethnic</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <select name="ethnic" class="form-control select2" style="width: 100%;" disabled>
                                                            <option value="">Choose Ethnic</option>
                                                            @foreach($ethnic_options as $ethnic_option_value => $ethnic_option_label)
                                                                <option value="{{$ethnic_option_value}}" {{$user->ethnic == $ethnic_option_value ? 'selected' : ''}}>{{$ethnic_option_label}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Place of Birth</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="place_birth" type="text" value="{{$user->place_birth}}" class="form-control" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Date of birth</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <div class="input-group date" data-provide="datepicker">
                                                            <input type="text" class="form-control" id="date_birth" name="date_birth" value="{{old('date_birth')}}" autocomplete="off" disabled>
                                                            <div class="input-group-addon">
                                                                <span class="glyphicon glyphicon-th"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Address</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <textarea name="address" rows="4" cols="50" value="{{$user->address}}" class="form-control" disabled></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Email</strong> :  <span style="color: red;">*</span></label>
                                                    <div class="col-lg-11">
                                                        <input name="email" type="text" value="{{$user->email}}" class="form-control" required disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <div class="phone-section">
                                                        <label class="col-lg-12 col-form-label text-right"><strong>Phone 1</strong> :</label>
                                                        <div class="col-lg-11">
                                                            <input name="phone" type="text" value="{{$user->phone}}" class="form-control" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="add-phone pt-2">
                                                        <a href="#" onclick="addPhone()" style="color: red;" hidden>Add more phone</a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="school-section">
                                                        <label class="col-lg-12 col-form-label text-right"><strong>School 1</strong> :</label>
                                                        <div class="col-lg-11">
                                                            <input name="school" type="text" value="{{$user->school}}" class="form-control" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="add-school pt-2">
                                                        <a href="#" onclick="addSchool()" style="color: red;" hidden>Add more school</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-lg-10 col-lg-10 pt-5">
                                                    <button class="btn btn-sm text-white font-weight-bolder" type="button" style="background-color: #00526B" onclick="enableEdit()">
                                                        <i class="fa fa-pencil"></i> Edit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="my-4 col-1">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="personality" role="tabpanel" aria-labelledby="personality-tab">
                        <!-- Sport form inputs -->
                        <form id="sportForm">
                            <div class="card-header" style="background-color: #4AAAC0">
                                <h5 class="text-white font-weight-bolder">Personality</h5>
                            </div>
                            <div class="mx-4">
                                <div class="row col-12">
                                    <div class="my-4 col-7">
                                        <form class="form-material" role="form" method="post" action="{{ route('users.update', $user->id) }}" autocomplete="off">
                                            {{ csrf_field() }}
                                            <input name="_method" type="hidden" value="PUT">
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Full Name</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="fullname" type="text" value="{{$user->fullname}}" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Nick Name</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="nickname" type="text" value="{{$user->nickname}}" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Place of Birth</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="place_birth" type="text" value="{{$user->place_birth}}" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Date of birth</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <div class="input-daterange input-group daterange">
                                                            <input type="text" class="form-control" id="date_birth" name="date_birth" value="{{old('date_birth')}}" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Religion</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="religion" type="text" value="{{$user->religion}}" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Status</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="status_marital" type="text" value="{{$user->status_marital}}" class="form-control" required placeholder="Choose status">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Phone</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="phone" type="text" value="{{$user->phone}}" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Email</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="email" type="text" value="{{$user->email}}" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Gender</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="gender" type="text" value="{{$user->gender}}" class="form-control" required placeholder="Choose gender">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Ethnic</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <input name="ethnic" type="text" value="{{$user->ethnic}}" class="form-control" required placeholder="Choose ethnic">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-6">
                                                    <label class="col-lg-12 col-form-label text-right"><strong>Address</strong> :</label>
                                                    <div class="col-lg-11">
                                                        <textarea name="address" rows="8" cols="50" value="{{$user->address}}" class="form-control" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="offset-lg-10 col-lg-10 pt-5">
                                                    <button class="btn btn-sm text-white font-weight-bolder" type="submit" style="background-color: #00526B"><i class="fa fa-check"></i>  Save</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="my-4 col-1">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Add more tab panes for additional categories -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('dashboard')
<!-- jQuery -->

<!-- Select2 CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<!-- Select2 JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


<style>
    .select2-container--default .select2-selection--single {
        height: auto !important;
    }
</style>

<!-- Your script -->
<script>
    function enableEdit() {
        // Enable all input fields
        $('input, select, textarea').prop('disabled', false);

        // Show the "Add more phone" link
        $('.add-phone a, .add-school a').removeAttr('hidden');

        // Change button text and icon to "Save"
        var button = $('.btn');
        button.html('<i class="fa fa-save"></i> Save');
        button.attr('onclick', 'saveChanges()');

        // Remove any existing "Cancel" button
        $('.cancel-btn').remove();

        // Add a "Cancel" button
        var cancelButton = $('<button class="btn btn-sm text-white font-weight-bolder cancel-btn" type="button" style="background-color: #6c757d"><i class="fa fa-times"></i> Cancel</button>');
        cancelButton.on('click', function() {
            // Disable all input fields
            $('input, select, textarea').prop('disabled', true);

            // Hide the "Add more phone" link
            $('.add-phone a, .add-school a').attr('hidden', true);

            // Revert the button back to "Edit"
            $('.btn').html('<i class="fa fa-pencil"></i> Edit');
            $('.btn').attr('onclick', 'enableEdit()');

            // Remove the cancel button
            $(this).remove();
        });

        // Insert the "Cancel" button before the "Save" button
        button.before(cancelButton);
    }

    function saveChanges() {
        // Enable form fields
        $('form :input').prop('disabled', false);

        // Disable the button to prevent multiple submissions
        $('.btn').prop('disabled', true);

        // Serialize the form data
        var formData = $('form').serialize();

        // Perform AJAX request to submit the form data
        $.ajax({
            url: $('form').attr('action'), // Get the form action URL
            method: 'PUT', // Use the PUT method as specified in the form
            data: formData, // Use the serialized form data
            success: function(response) {
                // Handle success response
                location.reload();
            },
            error: function(xhr, status, error) {
                // Handle error response
                console.error(error);
                location.reload();
            },
            complete: function() {
                // Re-disable the form fields after the AJAX request completes
                $('form :input').prop('disabled', true);
                // Re-enable the button after the AJAX request completes
                $('.btn').prop('disabled', false);
            }
        });
    }


    var maxPhones = 3;
    var currentPhone = 1;

    function addPhone() {
        if (currentPhone < maxPhones) {
            currentPhone++;
            var newPhoneSection = document.createElement('div');
            newPhoneSection.classList.add('phone-section');
            newPhoneSection.innerHTML = `
                <div class="col-lg-12">
                    <label class="col-lg-12 col-form-label text-right"><strong>Phone ${currentPhone}</strong> :</label>
                    <div class="col-lg-11">
                        <input name="phone_${currentPhone}" type="text" class="form-control" required>
                    </div>
                </div>
            `;
            document.querySelector('.add-phone').insertAdjacentElement('beforebegin', newPhoneSection);
        } else {
            var errorMessage = document.createElement('div');
            errorMessage.textContent = 'Max phone is 3';
            errorMessage.style.color = 'red';
            document.querySelector('.add-phone').appendChild(errorMessage);
        }
    }

    var maxSchools = 3;
    var currentSchool = 1;

    function addSchool() {
        if (currentSchool < maxSchools) {
            currentSchool++;
            var newSchoolSection = document.createElement('div');
            newSchoolSection.classList.add('school-section');
            newSchoolSection.innerHTML = `
                <div class="col-lg-12">
                    <label class="col-lg-12 col-form-label text-right"><strong>School ${currentSchool}</strong> :</label>
                    <div class="col-lg-11">
                        <input name="school_${currentSchool}" type="text" class="form-control" required>
                    </div>
                </div>
            `;
            document.querySelector('.add-school').insertAdjacentElement('beforebegin', newSchoolSection);
        } else {
            var errorMessage = document.createElement('div');
            errorMessage.textContent = 'Max schools is 3';
            errorMessage.style.color = 'red';
            document.querySelector('.add-school').appendChild(errorMessage);
        }
    }

    $(document).ready(function() {
        $('.select2').select2();


        // Add event listener for tab clicks
        $('.nav-link').click(function(e) {
            e.preventDefault();
            var tabName = $(this).attr('href').substring(1); // Get the tab name from href attribute
            loadForm(tabName); // Load the corresponding form
            $(this).tab('show');
            // Change background color of active tab
            $('.nav-link').removeClass('active-tab');
            $(this).addClass('active-tab');
        });

        // Load user form initially
        loadForm('profile');

        // Function to load form based on tab clicked
        function loadForm(tabName) {
            $('.tab-pane').hide(); // Hide all tab panes
            $('#' + tabName).show(); // Show the corresponding tab pane
        }

        // Event listener for form submission
        $('#userForm').submit(function(e) {
            e.preventDefault(); // Prevent default form submission
            var formData = $(this).serialize(); // Serialize form data
            submitForm(formData); // Call function to submit form data via AJAX
        });

        $('#sportForm').submit(function(e) {
            e.preventDefault(); // Prevent default form submission
            var formData = $(this).serialize(); // Serialize form data
            submitForm(formData); // Call function to submit form data via AJAX
        });

        // Function to submit form data via AJAX
        function submitForm(formData) {
            $.ajax({
                url: "",
                method: 'POST',
                data: formData,
                success: function(response) {
                    // Handle success response
                    // Optionally, you can update the UI or perform other actions
                },
                error: function(xhr, status, error) {
                    // Handle error response
                    console.error(error);
                    // Optionally, you can display an error message to the user
                }
            });
        }

        $('#date_birth').datepicker({
            format: 'yyyy-MM-dd', // Set the desired date format
            autoclose: true, // Close the datepicker when a date is selected
            todayHighlight: true // Highlight today's date
        });
    });
</script>
@endpush

