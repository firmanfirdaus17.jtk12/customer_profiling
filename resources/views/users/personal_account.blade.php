<div class="tab-pane fade" id="personal_account" role="tabpanel" aria-labelledby="personal_account-tab">
    <!-- Personal account form -->
    <form id="personalAccountForm" class="form-material" role="form" action="{{ route('users.update', $user->id) }}" method="POST">
        <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
            <div class="col-12">
                <h5 class="text-white font-weight-bolder">Personal Account</h5>
            </div>
        </div>
        <div class="mx-4 col-11">
            <div class="row col-12">
                <div class="offset-lg-2 my-4 offset-sm-1 col-sm-10 col-12">
                    <div class="form-material">
                        @method('PUT')
                        {{ csrf_field() }}
                        <input name="tabName" type="hidden" value="personal_account" class="form-control" hidden>
                            @if ($user->socialMediaAccounts->count() < 1)
                            <span id="data_empty_id_PA">
                                    Data Personnal Account is empty.
                            </span>
                            <div class="personal_account-section row pt-3" id="section1" hidden>
                                <div class="col-lg-3 col-sm-4 col-5">
                                    <label class="col-form-label text-right"><strong>Application</strong> :</label>
                                </div>
                                <div class="col-lg-4 col-sm-7 col-6" style="margin-top: 8px">
                                    <select name="personal_account[0]" class="form-control select2" style="width: 100%;" disabled>
                                        <option value="">Choose App</option>
                                        @foreach($personal_account_options as $personal_account_option_value => $personal_account_option_label)
                                        <option value="{{$personal_account_option_value}}">{{$personal_account_option_label}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-1" style="margin-top: 13px; margin-left:-15px">
                                    <img style="height: 15px; width:15px" class="plus_btn" id="plus_btn" src="{{ asset('assets/img/plus.png') }}" alt="..." onclick="addValue(document.getElementById('section1'))">
                                </div>
                                <div class="col-lg-7 offset-lg-3 col-md-10 col-sm-10 col-12 offset-md-4 offset-sm-4 offset-1 row" id="input_app_0_0" style="margin-top: 10px">
                                    <div class="col-11 input_PA_0" style="margin-left:-13px">
                                        <input name="character[0][0]" type="text" value="" class="form-control" disabled>
                                    </div>
                                    <div class="col-1" style="margin-top: 5px; margin-left:-15px">
                                        <img style="height: 15px; width:15px" class="minus_btn" id="minus_btn" src="{{ asset('assets/img/minus.png') }}" alt="..."onclick="deleteValue(document.getElementById('input_app_0_0'))">
                                    </div>
                                </div>
                            </div>
                            @else
                                @foreach($socialMediaAccountsFormat as $personal_account_key => $personal_account_value)
                                <div class="personal_account-section row pt-3" id="section{{$personal_account_key+1}}">
                                    <div class="col-lg-3 col-sm-4 col-5">
                                        <label class="col-form-label text-right"><strong>Application</strong> :</label>
                                    </div>
                                    <div class="col-lg-4 col-sm-7 col-6" style="margin-top: 8px">
                                        <select name="personal_account[]" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose App</option>
                                            @foreach($personal_account_options as $personal_account_option_value => $personal_account_option_label)
                                            <option value="{{$personal_account_option_value}}" {{$personal_account_value['social_media_id'] == $personal_account_option_value ? 'selected' : ''}}>{{$personal_account_option_label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-1" style="margin-top: 13px; margin-left:-15px">
                                        <img style="height: 15px; width:15px" class="plus_btn" id="plus_btn" src="{{ asset('assets/img/plus.png') }}" alt="..." onclick="addValue(document.getElementById('section{{$personal_account_key+1}}'))">
                                    </div>
                                    @foreach ($personal_account_value['value'] as $value_key => $value_label)
                                        <div class="col-lg-7 offset-lg-3 col-md-10 col-sm-10 col-12 offset-md-4 offset-sm-4 offset-1 row" id="input_app_{{ $personal_account_key }}_{{ $value_key }}" style="margin-top: 10px">
                                            <div class="col-11 input_PA_{{ $personal_account_key }}" style="margin-left:-13px">
                                                <input name="character[{{ $personal_account_key }}][{{ $value_key }}]" type="text" value="{{$value_label}}" class="form-control" disabled>
                                            </div>
                                            <div class="col-1" style="margin-top: 5px; margin-left:-15px">
                                                <img style="height: 15px; width:15px" class="minus_btn" id="minus_btn" src="{{ asset('assets/img/minus.png') }}" alt="..."onclick="deleteValue(document.getElementById('input_app_{{$personal_account_key}}_{{$value_key}}'))">
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                @endforeach
                            @endif
                            <div class="pt-3 addPersonnalAccount row">
                                <div id="add_family_member" class="col-6">
                                    <a href="#" onclick="addPersonalAccount()" style="color: #009DB6;" hidden>+ Add more application</a>
                                </div>
                                <div id="delete_family_member" class="col-6">
                                    <a href="#" onclick="deletePersonalAccount()" style="color: #009DB6;" hidden>- Delete last application</a>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-lg-6 col-lg-6 offset-md-6 col-md-6 pt-5 offset-sm-6 col-sm-6 offset-1 col-11">
                @if ($user->socialMediaAccounts->count() < 1)
                    <button class="btn btn-sm text-white font-weight-bolder" type="button" style="background-color: #00526B" id="edit_btn_personal_account" onclick="editPersonalAccountChanges()">
                        <i class="fa fa-plus" style="font-size: 11px; margin-right:8px"></i> Add
                    </button>
                @else
                    <button class="btn btn-sm text-white font-weight-bolder" type="button" style="background-color: #00526B" id="edit_btn_personal_account" onclick="editPersonalAccountChanges()">
                        <i class="fa fa-pencil" style="font-size: 11px; margin-right:8px"></i>  Edit
                    </button>
                @endif
                <button class="btn btn-sm text-white font-weight-bolder" type="button" style="background-color: #00526B" id="cancel_btn_personal_account" onclick="cancelPersonalAccountChanges()">
                    <i class="fa fa-times" style="font-size: 11px; margin-right:8px"></i>  Cancel
                </button>
                <button class="btn btn-sm text-white font-weight-bolder" type="submit" style="background-color: #00526B" id="save_btn_personal_account">
                    <i class="fa fa-save" style="font-size: 11px; margin-right:8px"></i>  Save
                </button>
            </div>
        </div>
    </form>
</div>


<script>
    function editPersonalAccountChanges() {
        // Enable all input fields within the personal account form
        $('#personalAccountForm input, #personalAccountForm select').prop('disabled', false);

        // Show the "Add more personal account" link
        $('.addPersonnalAccount a').removeAttr('hidden');

        // Hide the edit button and show the cancel and save buttons
        $('#edit_btn_personal_account').hide();
        $('#cancel_btn_personal_account').show();
        $('#save_btn_personal_account').show();

        $('#section1').removeAttr('hidden');
        $('#data_empty_id_PA').attr('hidden', true);
        $('.plus_btn, .minus_btn').show();
    }

    function cancelPersonalAccountChanges() {
        // Disable all input fields within the personal account form
        $('#personalAccountForm input, #personalAccountForm select').prop('disabled', true);

        // Hide the "Add more personal account" link
        $('.addPersonnalAccount a').attr('hidden', true);

        // Show the edit button and hide the cancel and save buttons
        $('#edit_btn_personal_account').show();
        $('#cancel_btn_personal_account').hide();
        $('#save_btn_personal_account').hide();
        $('.plus_btn, .minus_btn').hide();
        if ({{ $user->socialMediaAccounts->count() }} < 1) {
            $('#section1').attr('hidden', true);
            $('#data_empty_id_PA').removeAttr('hidden');
        }
    }

    function savePersonalAccountChanges() {
        // Prevent default form submission
        event.preventDefault();

        // Check if any option is selected
        var selectedOption = $('#personalAccountForm select').val();
        if (!selectedOption) {
            // No option selected, return false to prevent form submission
            return false;
        }
        // Serialize the form data within the personal account form
        var formData = $('#personalAccountForm').serialize();

        // Perform AJAX request to submit the form data
        $.ajax({
            url: "{{ route('users.update', $user->id) }}",
            method: 'PUT',
            data: formData,
            success: function(response) {
                // Handle success response
                location.reload();
                // Reload alert message
                $('#alert_message').load(window.location.href + ' #alert_message');

                // Disable all input fields within the personal account form
                $('#personalAccountForm input, #personalAccountForm select').prop('disabled', true);

                // Hide the "Add more personal account" link
                $('.addPersonnalAccount a').attr('hidden', true);

                // Show the edit button and hide the cancel and save buttons
                $('#edit_btn_personal_account').show();
                $('#cancel_btn_personal_account').hide();
                $('#save_btn_personal_account').hide();
                s$('.plus_btn, .minus_btn').hide();

                // Enable all buttons
                $('.btn').prop('disabled', false);
            },
            error: function(xhr, status, error) {
                // Handle error response
                // Reload alert message
                $('#alert_message').load(window.location.href + ' #alert_message');

                // Enable all buttons
                $('.btn').prop('disabled', false);
            }
        });
    }

    function addValue(parentElement) {
        // Get the index from the parent element's ID
        var index = parentElement.id.replace('section', '');
        // Find all existing input divs with class 'col-11' inside the parent element
        var existingInputs = parentElement.querySelectorAll('.input_PA_' + (index - 1));

        // Get the count of existing inputs
        var inputCount = existingInputs.length;

        // Check if the maximum limit (3) is reached
        if (inputCount >= 3) {
            alert('Max personnal account link limit reached.');
            return;
        }

        // Create a new input element
        var newInput = document.createElement('input');
        newInput.setAttribute('name', 'character[' + (index - 1) + '][' + (inputCount) + ']');
        newInput.setAttribute('type', 'text');
        newInput.setAttribute('value', '');
        newInput.classList.add('form-control');

        // Create a div for the input element
        var inputDiv = document.createElement('div');
        inputDiv.classList.add('col-11');
        inputDiv.classList.add('input_PA_' + (index-1));
        inputDiv.style.marginLeft = '-13px';
        inputDiv.appendChild(newInput);

        // Create a div for the minus button
        var minusBtnDiv = document.createElement('div');
        minusBtnDiv.classList.add('col-1');
        minusBtnDiv.style.marginTop = '5px';
        minusBtnDiv.style.marginLeft = '-15px';

        var minusBtn = document.createElement('img');
        minusBtn.setAttribute('src', '{{ asset("assets/img/minus.png") }}');
        minusBtn.setAttribute('alt', '...');
        minusBtn.style.height = '15px';
        minusBtn.style.width = '15px';
        minusBtn.classList.add('minus_btn');
        minusBtn.onclick = function () {
            deleteValue(document.getElementById('input_app_' + (index - 1) + '_' + (inputCount-1)));
        };

        // Append the minus button to the minus button div
        minusBtnDiv.appendChild(minusBtn);

        // Create a new div to wrap both input and minus button divs
        var newDiv = document.createElement('div');
        newDiv.setAttribute('id', 'input_app_' + (index - 1) + '_' + (inputCount-1));
        newDiv.classList.add('col-lg-7', 'offset-lg-3',  'col-sm-10', 'col-12', 'offset-sm-4', 'offset-1',  'row');
        newDiv.style.marginTop = '10px';

        // Append input and minus button divs to the new div
        newDiv.appendChild(inputDiv);
        newDiv.appendChild(minusBtnDiv);

        // Append the new div to the parent element
        parentElement.appendChild(newDiv);
    }

    function deleteValue(parentElement) {
        // Get the ID of the minus button's parent element
        var parentId = parentElement.id;

        // Remove the element with the corresponding ID
        var elementToRemove = document.getElementById(parentId);
        if (elementToRemove) {
            elementToRemove.parentNode.removeChild(elementToRemove);
        }
    }

    function deletePersonalAccount() {
        // Get all personal_account sections
        var personalAccount = $('.personal_account-section');

        // Check if there is more than one family section
        if (personalAccount.length > 1) {
            // Remove the last family section
            personalAccount.last().remove();
        } else {
            // If there is only one family section, clear its input values instead of removing it
            personalAccount.find('input, select').val('');
        }
    }

</script>
