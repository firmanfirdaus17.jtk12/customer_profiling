<div class="tab-pane fade" id="organization" role="tabpanel" aria-labelledby="organization-tab">
    <div>
        <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
            <div class="col-12">
                <h5 class="text-white font-weight-bolder">Organization</h5>
            </div>
        </div>
        <div class="mx-4">
            <div class="row col-12">
                <div class="mb-4">
                    <div class="pt-3">
                        <div class="d-flex flex-row justify-content-between pt-2" style="margin-left: 30px">
                            <h5 class="font-weight-bolder">History Organization</h5>
                        </div>
                    </div>
                    <div class="card-header">
                        <div class="d-flex flex-row justify-content-between">
                            <div>
                                <a href="#" class="mx-3 detail-organization btn btn-sm mb-0" data-organization-id="" type="button" data-bs-toggle="modal" style="background-color: #00526B; color: white; font-size: 11px; margin-right:8px" data-bs-target="#organizationDetailModal" data-bs-original-title="Add Organization"><i class="fa fa-plus" style="font-size: 11px; margin-right:8px"></i> Add</a>
                            </div>
                            <div class="form-group mb-0">
                                <!-- Search input placeholder -->
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table align-items-center">
                                <thead>
                                    <tr>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">NO</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">START YEAR</th>
                                        <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">END YEAR</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">COMPANY</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">POSITION</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">AREA</th>
                                        <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Table rows will be populated dynamically using JavaScript -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="offset-0">
                        <a href="{{ url('organization', ['user_id' => $user->id]) }}" class="mx-3 organization btn btn-sm mb-0" type="button" style="background-color: #00526B; color: white; margin-top:30px">Organization Chart</a>
                    </div>
                    <div class="form-group row offset-0" style="margin-left: 5px">
                        <div class="col-10">
                            <label class="col-12 col-form-label text-right"><strong>List members of {{ $user->fullname }}:</strong></label>
                            <div class="col-11">
                                @if(count($member_list) < 1)
                                <div style="margin-left:5px; color:gray">
                                        - Data member is empty
                                </div>
                                @endif
                                @php
                                    $counter = 1;
                                @endphp
                                @foreach($member_list as $member_list_value => $member_list_label)
                                    {{($counter).'. '.$member_list_label}} <br>
                                    @php
                                        $counter++;
                                    @endphp
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <form id="organizationForm" class="form-material" role="form" action="{{ route('users.update', $user->id) }}" method="POST">
            @method('PUT')
            {{ csrf_field() }}
            <input name="tabName" type="hidden" value="organization_detail" class="form-control" hidden>
            <div class="modal fade" id="organizationDetailModal" tabindex="-1" role="dialog" aria-labelledby="organizationDetailModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div class="modal-content" autocomplete="off">
                        <div class="modal-header">
                            <h5 class="modal-title" id="organizationDetailModalLabel">Organization Detail</h5>
                            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="organizationDetailModalBody">
                        </div>
                        <div class="modal-footer">
                            <div class="offset-lg-8 col-lg-4 offset-md-8 col-md-4 pt-2 offset-sm-8 col-sm-4 offset-1 col-11">
                                <button class="btn btn-sm text-white font-weight-bolder" type="button" style="background-color: #00526B" id="edit_btn_organization" onclick="editOrganizationChanges()">
                                    <i class="fa fa-pencil" style="font-size: 11px; margin-right:8px"></i>  Edit
                                </button>
                                <button class="btn btn-sm text-white font-weight-bolder" type="button" style="background-color: #00526B" id="cancel_btn_organization" onclick="cancelOrganizationChanges()">
                                    <i class="fa fa-times" style="font-size: 11px; margin-right:8px"></i>  Cancel
                                </button>
                                <button class="btn btn-sm text-white font-weight-bolder" type="submit" style="background-color: #00526B" id="save_btn_organization">
                                    <i class="fa fa-save" style="font-size: 11px; margin-right:8px"></i>  Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    function editOrganizationChanges() {
        // Enable all input fields within the organization form
        $('#organizationForm input, #organizationForm select, #organization_detail').prop('disabled', false);
        $('#group_company, #area, #sub_area').prop('disabled', true);

        // Show the edit button and hide the cancel and save buttons
        $('#edit_btn_organization').hide();
        $('#cancel_btn_organization').show();
        $('#save_btn_organization').show();
    }

    function cancelOrganizationChanges() {
        // Disable all input fields within the organization form
        $('#organizationForm input, #organizationForm select').prop('disabled', true);

        // Show the edit button and hide the cancel and save buttons
        $('#edit_btn_organization').show();
        $('#cancel_btn_organization').hide();
        $('#save_btn_organization').hide();
    }
</script>
