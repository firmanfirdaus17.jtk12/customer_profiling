<div class="tab-pane fade" id="personality" role="tabpanel" aria-labelledby="personality-tab">
    <!-- Personality form inputs -->
    <form id="personalityForm" class="form-material" role="form" action="{{ route('users.update', $user->id) }}" method="POST">
        <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
            <div class="col-12">
                <h5 class="text-white font-weight-bolder">Personality</h5>
            </div>
        </div>
        <div class="mx-4 col-11">
            <div class="row col-12">
                @method('PUT')
                {{ csrf_field() }}
                <input name="tabName" type="hidden" value="personality" class="form-control" hidden>
                <div class="offset-lg-2 offset-sm-1 my-4 col-lg-8 col-sm-10">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right">
                                <strong>Character of Person :</strong>
                            </label>
                            <div class="col-lg-11">
                                <textarea name="character" id="character" class="form-control" rows="1" disabled>{{ $user->character }}</textarea>
                            </div>
                            @error('character')
                                <div class="alert alert-danger col-11" style="margin-top: 5px; color: white; font-size: small;">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Allergy :</strong></label>
                            <div class="col-lg-11">
                                <textarea name="allergy" id="allergy" class="form-control" rows="1" disabled>{{ $user->allergy }}</textarea>
                            </div>
                            @error('allergy')
                                <div class="alert alert-danger col-11" style="margin-top: 5px; color: white; font-size: small;">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Favorite Drink :</strong></label>
                            <div class="col-lg-11">
                                <textarea name="favorite_drink" id="favorite_drink" class="form-control" rows="1" disabled>{{ $user->favorite_drink }}</textarea>
                            </div>
                            @error('favorite_drink')
                                <div class="alert alert-danger col-11" style="margin-top: 5px; color: white; font-size: small;">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Knowledge :</strong></label>
                            <div class="col-lg-11">
                                <textarea name="knowledge" id="knowledge" class="form-control" rows="1" disabled>{{ $user->knowledge }}</textarea>
                                <div style="padding-left: 2%">
                                    <span style="font-size: smaller;">*Kompetensi, keahlian dan pengalaman yang dimiliki (contoh: SCADA, Distribusi, dll)</span>
                                </div>
                                @error('knowledge')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <div>
                                <label class="col-lg-12 col-form-label text-right"><strong>Favorite Food :</strong></label>
                                <div class="col-lg-11">
                                    <textarea name="favorite_food" id="favorite_food" class="form-control" rows="1" disabled>{{ $user->favorite_food }}</textarea>
                                </div>
                                @error('favorite_food')
                                    <div class="alert alert-danger col-11" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>

                            <div>
                                <label class="col-lg-12 col-form-label text-right"><strong>Favorite Color :</strong></label>
                                <div class="col-lg-11">
                                    <input name="favorite_color" type="text" value="{{$user->favorite_color}}" class="form-control" disabled>
                                </div>
                                @error('favorite_color')
                                    <div class="alert alert-danger col-11" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="hobby-section">
                                <div id="hobby_1">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Hobby 1</strong> :</label>
                                    <div class="col-lg-11">
                                        <select name="hobby" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose Hobby</option>
                                            @foreach($hobby_options as $hobby_option_value => $hobby_option_label)
                                                <option value="{{$hobby_option_value}}" {{$user->hobby == $hobby_option_label ? 'selected' : ''}}>{{$hobby_option_label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (!empty($user->hobby_2))
                                <div id="hobby_2">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Hobby 2</strong> :</label>
                                    <div class="col-lg-11">
                                        <select name="hobby_2" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose Hobby</option>
                                            @foreach($hobby_options as $hobby_option_value => $hobby_option_label)
                                                <option value="{{$hobby_option_value}}" {{$user->hobby_2 == $hobby_option_label ? 'selected' : ''}}>{{$hobby_option_label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                                @if (!empty($user->hobby_3) || $user->hobby_3 != "")
                                <div id="hobby_3">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Hobby 3</strong> :</label>
                                    <div class="col-lg-11">
                                        <select name="hobby_3" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose Hobby</option>
                                            @foreach($hobby_options as $hobby_option_value => $hobby_option_label)
                                                <option value="{{$hobby_option_value}}" {{$user->hobby_3 == $hobby_option_label ? 'selected' : ''}}>{{$hobby_option_label}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="add-hobby pt-2 row">
                                <div id="add_hobby" class="col-6">
                                    <a href="#" onclick="addHobby()" style="color: #009DB6;" hidden>+ Add more hobby</a>
                                </div>
                                <div id="delete_hobby" class="col-6">
                                    <a href="#" onclick="deleteHobby()" style="color: #009DB6;" hidden>- Delete hobby</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-lg-6 col-lg-10 pt-2 offset-sm-6 col-sm-10 offset-1 col-11">
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="edit_btn_personality" onclick="editPersonalityChanges()">
                    <i class="fa fa-pencil icon-tab-action"></i>  Edit
                </button>
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="cancel_btn_personality" onclick="cancelPersonalityChanges()">
                    <i class="fa fa-times icon-tab-action"></i>  Cancel
                </button>
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="submit" style="background-color: #00526B" id="save_btn_personality">
                    <i class="fa fa-save icon-tab-action"></i>  Save
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    var errorMessageHobby;
    function editPersonalityChanges() {
        // Enable all input fields within the personality form
        $('#personalityForm input, #personalityForm select, #personalityForm textarea').prop('disabled', false);

        // Show the "Add more hobby" link
        $('.add-hobby a').removeAttr('hidden');

        // Hide the edit button and show the cancel and save buttons
        $('#edit_btn_personality').hide();
        $('#cancel_btn_personality').show();
        $('#save_btn_personality').show();
    }

    function cancelPersonalityChanges() {
        // Disable all input fields within the personality form
        $('#personalityForm input, #personalityForm select, #personalityForm textarea').prop('disabled', true);

        // Hide the "Add more hobby" link
        $('.add-hobby a').attr('hidden', true);

        // Show the edit button and hide the cancel and save buttons
        $('#edit_btn_personality').show();
        $('#cancel_btn_personality').hide();
        $('#save_btn_personality').hide();
        if (errorMessageHobby) {
            errorMessageHobby.remove();
        }
    }

    function savePersonalityChangessssss() {
        // Prevent default form submission
        event.preventDefault();

        // Serialize the form data within the personality form
        var formData = $('#personalityForm').serialize();

        // Perform AJAX request to submit the form data
        $.ajax({
            url: "{{ route('users.update', $user->id) }}",
            method: 'PUT',
            data: formData,
            success: function(response) {
                // Handle success response
                // window.location.href = window.location.href.split('?')[0];

                location.reload();
                // Reload alert message
                $('#alert_message').load(window.location.href + ' #alert_message');

                // Disable all input fields within the personality form
                $('#personalityForm input, #personalityForm select, #personalityForm textarea').prop('disabled', true);

                // Hide the "Add more hobby" link
                $('.add-hobby a').attr('hidden', true);

                // Show the edit button and hide the cancel and save buttons
                $('#edit_btn_personality').show();
                $('#cancel_btn_personality').hide();
                $('#save_btn_personality').hide();

                // Enable all buttons
                $('.btn').prop('disabled', false);
            },
            error: function(xhr, status, error) {
                // Handle error response
                // Reload alert message
                $('#alert_message').load(window.location.href + ' #alert_message');

                // Enable all buttons
                $('.btn').prop('disabled', false);
            }
        });
    }
</script>
