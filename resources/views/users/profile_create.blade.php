<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <!-- User form inputs -->
    <form id="profileForm" class="form-material" role="form" enctype="multipart/form-data" action="{{ route('users.store') }}" method="POST">
        <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
            <div class="col-12">
                <h5 class="text-white font-weight-bolder">Personal Profile</h5>
            </div>
        </div>
        <div class="mx-4 col-11">
            <div class="row col-12">
                {{ csrf_field() }}
                <input name="tabName" type="hidden" value="profile" class="form-control" hidden>
                <div class="row my-4 col-lg-3 col-sm-3 mx-auto justify-content-center">
                    <div class="row card col-3 d-flex justify-content-center align-items-center photo_card">
                        <div class="photo_card_box">
                            <!-- Image area -->
                            @if (isset($user->photo) && !empty($user->photo))
                                <img id="uploaded-image" src="{{ asset($user->photo) }}" class="navbar-brand-img photo_card_body" alt="...">
                            @else
                                <img id="uploaded-image" src="{{ asset('assets/img/people.png') }}" class="navbar-brand-img photo_card_body" alt="...">
                            @endif
                        </div>
                        Photo Profile
                        <div class="d-flex justify-content-center align-items-center pt-1 photo_upload" id="photo_upload_btn" style="display: none;">
                            <label id="photo_upload_label" for="file-upload" class="photo_upload" style="cursor: pointer;"><span style="color: rgb(0, 11, 157);">Upload Here</span></label>
                            <input id="file-upload" name="photo" type="file" style="display: none;" accept="image/*" onchange="previewImage(this)">
                        </div>
                    </div>
                </div>
                <div class="my-4 col-lg-8 col-sm-9">
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right">
                                <strong>Full Name :</strong>
                                <span style="color: red;">*</span>
                            </label>
                            <div class="col-lg-11">
                                <input name="fullname" type="text" value="{{ old('fullname') }}" class="form-control" required>
                                @error('fullname')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Nick Name</strong> :</label>
                            <div class="col-lg-11">
                                <input name="nickname" type="text" value="{{ old('nickname') }}" class="form-control">
                                @error('nickname')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>NIP</strong> :  <span style="color: red;">*</span></label>
                            <div class="col-lg-11">
                                <input name="nip" type="text" value="{{ old('nip') }}" class="form-control" disabled>
                                @error('nip')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Email</strong> :  <span style="color: red;">*</span></label>
                            <div class="col-lg-11">
                                <input name="email" type="text" value="{{ old('email') }}" class="form-control" required>
                                <div class="text-danger" id="error_email" hidden>The email field is required.</div>
                                @error('email')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Status</strong> :</label>
                            <div class="col-lg-11">
                                <select name="status_marital" class="form-control select2" style="width: 100%;">
                                    <option value="">Choose Status</option>
                                    <option value="single" {{ old('status_marital') == 'single' ? 'selected' : '' }}>Single</option>
                                    <option value="married" {{ old('status_marital') == 'married' ? 'selected' : '' }}>Married</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Religion</strong> :</label>
                            <div class="col-lg-11">
                                <select name="religion" class="form-control select2" style="width: 100%;">
                                    <option value="">Choose Religion</option>
                                    <option value="islam" {{ old('religion') == 'islam' ? 'selected' : '' }}>Islam</option>
                                    <option value="kristen_protestan" {{ old('religion') == 'kristen_protestan' ? 'selected' : '' }}>Protestan</option>
                                    <option value="kristen_katolik" {{ old('religion') == 'kristen_katolik' ? 'selected' : '' }}>Katolik</option>
                                    <option value="hindu" {{ old('religion') == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                    <option value="buddha" {{ old('religion') == 'buddha' ? 'selected' : '' }}>Buddha</option>
                                    <option value="khonghucu" {{ old('religion') == 'khonghucu' ? 'selected' : '' }}>Khonghucu</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Gender</strong> :</label>
                            <div class="col-lg-11">
                                <select name="gender" class="form-control select2" style="width: 100%;">
                                    <option value="">Choose Gender</option>
                                    <option value="male" {{ old('gender') == 'male' ? 'selected' : '' }}>Male</option>
                                    <option value="female" {{ old('gender') == 'female' ? 'selected' : '' }}>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Ethnic</strong> :</label>
                            <div class="col-lg-11">
                                <select name="ethnic" class="form-control select2" style="width: 100%;">
                                    <option value="">Choose Ethnic</option>
                                    @foreach($ethnic_options as $ethnic_option_value => $ethnic_option_label)
                                        <option value="{{$ethnic_option_value}}" {{ old('ethnic') == $ethnic_option_value ? 'selected' : '' }}>{{$ethnic_option_label}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Address</strong> :</label>
                            <div class="col-lg-11">
                                <textarea name="address" rows="4" cols="50" class="form-control">{{ old('address') }}</textarea>
                                @error('address')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <label class="col-lg-12 col-form-label text-right"><strong>Place of Birth</strong> :</label>
                            <div class="col-lg-11">
                                <input name="place_birth" type="text" value="{{ old('place_birth') }}" class="form-control">
                                @error('place_birth')
                                    <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <label class="col-lg-12 col-form-label text-right"><strong>Date of Birth</strong> :</label>
                            <div class="col-lg-11">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" id="date_birth" name="date_birth" value="{{ old('date_birth') }}" autocomplete="off">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-th"></span>
                                        @error('date_birth')
                                            <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6 col-sm-6">
                            <div class="phone-section">
                                <div id="phone_1">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Phone 1</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="phone" type="text" value="{{ old('phone') }}" class="form-control">
                                        @error('phone')
                                            <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="add-phone pt-2 row">
                                <div id="add_phone" class="col-6">
                                    <a href="#" onclick="addPhone()" style="color: #009DB6;" hidden>+ Add more phone</a>
                                </div>
                                <div id="delete_phone" class="col-6">
                                    <a href="#" onclick="deletePhone()" style="color:#009DB6;" hidden>- Delete phone</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6">
                            <div class="school-section">
                                <div id="school_1">
                                    <label class="col-lg-12 col-form-label text-right"><strong>School 1</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="school" type="text" value="{{ old('school') }}" class="form-control">
                                        @error('school')
                                            <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="add-school pt-2 row">
                                <div id="add_school" class="col-6">
                                    <a href="#" onclick="addSchool()" style="color: #009DB6;" hidden>+ Add more school</a>
                                </div>
                                <div id="delete_school" class="col-6">
                                    <a href="#" onclick="deleteSchool()" style="color: #009DB6;" hidden>- Delete school</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-lg-6 col-lg-6 offset-md-6 col-md-6 pt-5 offset-sm-6 col-sm-6 offset-1 col-11">
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="edit_btn" onclick="editProfileChanges()">
                    <i class="fa fa-pencil icon-tab-action"></i> Edit
                </button>
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="cancel_btn" onclick="cancelProfileChanges()">
                    <i class="fa fa-times icon-tab-action"></i>  Cancel
                </button>
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="submit" style="background-color: #00526B" id="save_btn">
                    <i class="fa fa-save icon-tab-action"></i>  Save
                </button>
            </div>
        </div>
    </form>
</div>

<script>
    var errorMessagePhone, errorMessageSchool;

    function editProfileChanges() {
        // Enable all input fields
        $('#profileForm input, #profileForm select, #profileForm textarea').prop('disabled', false);

        // Show the "Add more school" link
        $('.add-phone a, .add-school a').removeAttr('hidden');

        // Show the "Add more hobby" link
        $('.add-phone a, .add-hobby a').removeAttr('hidden');

        // Show the "Upload Here" label
        $('#photo_upload_label').show();

        $('#edit_btn').hide();
        $('#cancel_btn').show();
        $('#save_btn').show();
    }


    function cancelProfileChanges() {
        // Disable all input fields
        $('#profileForm input, #profileForm select, #profileForm textarea').prop('disabled', true);

        // Hide the "Add more phone" link
        $('.add-phone a, .add-school a').attr('hidden', true);
        $('#photo_upload_label').hide();

        $('#edit_btn').show();
        $('#cancel_btn').hide();
        $('#save_btn').hide();
        // Remove error message if it exists
        if (errorMessagePhone) {
            errorMessagePhone.remove();
        }
        if (errorMessageSchool) {
            errorMessageSchool.remove();
        }
    }
</script>
