@extends('layouts.user_type.auth')

@section('content')
<div style="margin-left: 20px">
    <div class="d-flex flex-row justify-content-between">
        <div>
           <!-- Nav tabs -->
           <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true" onclick="toggleActiveTab(this)" style="border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                        <h6 class="text-white font-weight-bolder pt-1">PROFILE</h6>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="offset-8 col-4" style="position: relative;">
                        <div id="alert_message" style="position: absolute; top: 50%; left: 0; transform: translateY(-50%); z-index: 999;">
                            @if(session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert" style="width:300px; color: rgb(255, 255, 255);">
                                    {{ session('success') }}
                                    <button type="button" class="close" aria-label="Close" style="background-color: transparent; border: none;" onclick="alertClose()">
                                        <span aria-hidden="true" style="color: rgb(255, 255, 255);">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if(session('failed'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width:300px; color: rgb(255, 255, 255);">
                                    {{ session('failed') }}
                                    <button type="button" class="close" aria-label="Close" style="background-color: transparent; border: none;" onclick="alertClose()">
                                        <span aria-hidden="true" style="color: rgb(255, 255, 255);">&times;</span>
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>

                </div>
                    <!-- Content Personal Profile -->
                    @include('users.profile_create')

                    {{-- <!-- Content Personality -->
                    @include('users.personality_create')

                    <!-- Content Family -->
                    @include('users.family_create')

                    <!-- Content Organization -->
                    @include('users.organization_create')

                    <!-- Content Personal Account -->
                    @include('users.personal_account_create') --}}

                    <!-- Add more tab panes for additional categories -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('dashboard')
<!-- jQuery -->

<!-- Select2 CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">

<!-- Select2 JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>


<style>
    .select2-container--default .select2-selection--single {
        height: auto !important;
    }
</style>

<!-- Your script -->
<script>
    $(document).ready(function() {
        loadForm('profile');
        $('#profile').addClass('show active');
        sessionStorage.setItem('tabName', 'profile');
        $('a[href="#profile"]').tab('show');

        $('.select2').select2();

        $('#cancel_btn, #save_btn, #photo_upload_btn, #photo_upload_label').hide();

        $('.add-account-link').hide();

        $('.nav-link').click(function(e) {
            var tabName = $(this).attr('href').substring(1);
            var allowedTabs = ['profile', 'personality', 'family', 'organization', 'personal_account'];
            if (allowedTabs.includes(tabName)) {
                loadForm(tabName);
                $(this).tab('show');
                $('.nav-link').removeClass('active-tab');
                $(this).addClass('active-tab');
                sessionStorage.setItem('tabName', tabName);
            }
        });

        function loadForm(tabName) {
            $('.tab-pane').hide();
            $('#' + tabName).show();
            if (tabName == 'profile') {
                $('#address').each(function() {
                    adjustTextareaHeight(this);
                });
            }
            if (tabName == 'personality') {
                $('#character, #favorite_food, #favorite_drink, #allergy, #knowledge').each(function() {
                    adjustTextareaHeight(this);
                });
            }
        }

        $(".datepicker").datepicker({
            format: 'dd-mm-yy',
            autoclose: true
        });

        $("#date_birth").datepicker({
            format: 'dd-mm-yy',
            autoclose: true
        });

        // Function to dynamically adjust textarea height
        function adjustTextareaHeight(textarea) {
            // Reset rows to 1 to calculate correct scroll height
            textarea.rows = 1;
            // Set the rows attribute based on the scroll height divided by the line height
            textarea.rows = Math.ceil(textarea.scrollHeight / parseFloat($(textarea).css('line-height')));
        }

        // Call adjustTextareaHeight when textarea content changes for all relevant textareas
        $('#character, #favorite_food, #favorite_drink, #allergy, #knowledge, #address').on('input', function() {
            adjustTextareaHeight(this);
        });

        setTimeout(function(){
            $('#alert_message').fadeOut();
        }, 3000);
        editProfileChanges();
    });

    function alertClose() {
        sessionStorage.removeItem('success');
        sessionStorage.removeItem('failed');
        $('#alert_message').load(location.href + ' #alert_message');
    }

    // Functionality for adding and deleting phones
    var maxPhones = 3;
    var currentPhone = 1;

    function addPhone() {
        if (currentPhone < maxPhones) {
            currentPhone++;
            var name_input = 'phone_'+currentPhone;
            if (currentPhone > 0) {
                $('#delete_phone').show();
            }
            var newPhoneSection = document.createElement('div');
            newPhoneSection.classList.add('phone-section');
            if (currentPhone == 1) {
                name_input = 'phone';
            }
            newPhoneSection.innerHTML = `
                <div id="phone_${currentPhone}">
                    <label class="col-lg-12 col-form-label text-right"><strong>Phone ${currentPhone}</strong> :</label>
                    <div class="col-lg-11">
                        <input name="${name_input}" type="text" class="form-control" required>
                    </div>
                </div>
            `;
            document.querySelector('.add-phone').insertAdjacentElement('beforebegin', newPhoneSection);
        } else {
            // Add error message if it doesn't exist
            if (!errorMessagePhone) {
                errorMessagePhone = document.createElement('div');
                errorMessagePhone.textContent = 'Max phone is 3';
                errorMessagePhone.style.color = 'red';
                document.querySelector('.add-phone').appendChild(errorMessagePhone);
            }
        }
    }

    function deletePhone() {
        if (currentPhone > 0) {
            var phoneToDelete = document.querySelector(`[id="phone_${currentPhone}"]`);
            phoneToDelete.remove();
            currentPhone--;
            if (currentPhone == 0) {
                $('#delete_phone').hide();
            }
            // Remove error message if phones are less than max
            if (errorMessagePhone && currentPhone < maxPhones) {
                errorMessagePhone.remove();
                errorMessagePhone = null;
            }
        }
    }

    // Functionality for adding and deleting schools
    var maxSchools = 3;
    var currentSchool = 1;

    function addSchool() {
        if (currentSchool < maxSchools) {
            currentSchool++;
            var name_input = 'school_'+currentSchool;
            if (currentSchool > 0) {
                $('#delete_school').show();
            }
            var newSchoolSection = document.createElement('div');
            newSchoolSection.classList.add('school-section');
            if (currentSchool == 1) {
                name_input = 'school';
            }
            newSchoolSection.innerHTML = `
                <div id="school_${currentSchool}">
                    <label class="col-lg-12 col-form-label text-right"><strong>School ${currentSchool}</strong> :</label>
                    <div class="col-lg-11">
                        <input name="${name_input}" type="text" class="form-control" required>
                        @error('${name_input}')
                            <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            `;
            document.querySelector('.add-school').insertAdjacentElement('beforebegin', newSchoolSection);
        } else {
            // Add error message if it doesn't exist
            if (!errorMessageSchool) {
                errorMessageSchool = document.createElement('div');
                errorMessageSchool.textContent = 'Max schools is 3';
                errorMessageSchool.style.color = 'red';
                document.querySelector('.add-school').appendChild(errorMessageSchool);
            }
        }
    }


    function deleteSchool() {
        if (currentSchool > 0) {
            var schoolToDelete = document.querySelector(`[id="school_${currentSchool}"]`);
            schoolToDelete.remove();
            currentSchool--;
            if (currentSchool == 0) {
                $('#delete_school').hide();
            }
            // Remove error message if schools are less than max
            if (errorMessageSchool && currentSchool < maxSchools) {
                errorMessageSchool.remove();
                errorMessageSchool = null;
            }
        }
    }

    // Functionality for adding and deleting hobbies
    var maxHobbies = 3;
    var currentHobby = 1;

    function addHobby() {
        if (currentHobby < maxHobbies) {
            currentHobby++;
            if (currentHobby > 0) {
                $('#delete_hobby').show();
            }
            var newHobbySection = document.createElement('div');
            newHobbySection.classList.add('hobby-section');
            var hobbyIndex = currentHobby === 1 ? '' : '_' + currentHobby;
            newHobbySection.innerHTML = `
                <div id="hobby${hobbyIndex}">
                    <label class="col-lg-12 col-form-label text-right"><strong>Hobby ${currentHobby}</strong> :</label>
                    <div class="col-lg-11">
                        <select name="hobby${hobbyIndex}" class="form-control select2" style="width: 100%;" required>
                            <option value="">Choose Hobby</option>
                            @foreach($hobby_options as $hobby_option_value => $hobby_option_label)
                                <option value="{{$hobby_option_value}}">{{$hobby_option_label}}</option>
                            @endforeach
                        </select>
                        @error('hobby${hobbyIndex}')
                            <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            `;
            document.querySelector('.add-hobby').insertAdjacentElement('beforebegin', newHobbySection);
            // Initialize select2 for the newly added select element
            $('.select2').select2();
        } else {
            // Add error message if it doesn't exist
            if (!errorMessageHobby) {
                errorMessageHobby = document.createElement('div');
                errorMessageHobby.textContent = 'Max hobbies is 3';
                errorMessageHobby.style.color = 'red';
                document.querySelector('.add-hobby').appendChild(errorMessageHobby);
            }
        }
    }

    function deleteHobby() {
        if (currentHobby > 0) {
            var hobbyToDelete = document.querySelector(`[id="hobby_${currentHobby}"]`);
            hobbyToDelete.remove();
            currentHobby--;
            if (currentHobby == 0) {
                $('#delete_hobby').hide();
            }
            // Remove error message if hobbies are less than max
            if (errorMessageHobby && currentHobby < maxHobbies) {
                errorMessageHobby.remove();
                errorMessageHobby = null;
            }
        }
    }

    function previewImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#uploaded-image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]); // Read the selected file as a URL
        }
    }

    // Set default date format globally
    $.fn.datepicker.defaults.format = "dd-mm-yyyy";

    function toggleActiveTab(tab) {
        const tabs = document.querySelectorAll('.nav-link');
        tabs.forEach(tab => tab.classList.remove('active'));
        tab.classList.add('active');
    }

</script>

@endpush

