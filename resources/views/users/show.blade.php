@extends('layouts.user_type.auth')

@section('content')
    <div style="margin-left: 20px">
        <div class="d-flex flex-row justify-content-between">
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true" onclick="toggleActiveTab(this)" style="border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                            <h6 class="text-white font-weight-bolder pt-1">PROFILE</h6>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="personality-tab" data-bs-toggle="tab" href="#personality" role="tab" aria-controls="personality" aria-selected="false" onclick="toggleActiveTab(this)" style="border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                            <h6 class="text-white font-weight-bolder pt-1">PERSONALITY</h6>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="family-tab" data-bs-toggle="tab" href="#family" role="tab" aria-controls="family" aria-selected="false" onclick="toggleActiveTab(this)" style=" border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                            <h6 class="text-white font-weight-bolder  pt-1">FAMILY</h6>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="organization-tab" data-bs-toggle="tab" href="#organization" role="tab" aria-controls="organization" aria-selected="false" onclick="toggleActiveTab(this)" style="border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                            <h6 class="text-white font-weight-bolder  pt-1">ORGANIZATION</h6>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="personal_account-tab" data-bs-toggle="tab" href="#personal_account" role="tab" aria-controls="personal_account" aria-selected="false" onclick="toggleActiveTab(this)" style="border-bottom-left-radius: 0px; border-bottom-right-radius: 0px; margin-left:2px">
                            <h6 class="text-white font-weight-bolder pt-1">PERSONAL ACCOUNT</h6>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-body px-0 pt-0 pb-2">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="offset-8 col-4" style="position: relative;">
                            <div id="alert_message" style="position: absolute; top: 50%; left: 0; transform: translateY(-50%); z-index: 999;">
                                @if(session('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="width:300px; color: rgb(255, 255, 255);">
                                        {{ session('success') }}
                                        <button type="button" class="close" aria-label="Close" style="background-color: transparent; border: none;" onclick="alertClose()">
                                            <span aria-hidden="true" style="color: rgb(255, 255, 255);">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if(session('failed'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="width:300px; color: rgb(255, 255, 255);">
                                        {{ session('failed') }}
                                        <button type="button" class="close" aria-label="Close" style="background-color: transparent; border: none;" onclick="alertClose()">
                                            <span aria-hidden="true" style="color: rgb(255, 255, 255);">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- Content Personal Profile -->
                    @include('users.profile')
                    <!-- Content Personality -->
                    @include('users.personality')
                    <!-- Content Family -->
                    @include('users.family')
                    <!-- Content Organization -->
                    @include('users.organization')
                    <!-- Content Personal Account -->
                    @include('users.personal_account')
                    <!-- Add more tab panes for additional categories -->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('dashboard')
    <!-- jQuery -->
    <!-- Select2 CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <!-- Select2 JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
    <style>
        .select2-container--default .select2-selection--single {
            height: auto !important;
        }
    </style>

    <!-- Your script -->
    <script>
        $(document).ready(function() {
            var tabName = sessionStorage.getItem('tabName');
            if(tabName === null || tabName === "") {
                loadForm('profile');
                $('#profile').addClass('show active');
                $('a[href="#profile"]').tab('show');
            } else {
                loadForm(tabName);
                $('#' + tabName).addClass('show active');
                $('a[href="#' + tabName + '"]').tab('show');
            }

            $('.select2').select2();

            $('#cancel_btn, #save_btn, #photo_upload_btn, #photo_upload_label, #cancel_btn_personality, #save_btn_personality, #cancel_btn_personal_account, #save_btn_personal_account, #cancel_btn_family, #save_btn_family, #cancel_btn_organization, #save_btn_organization, #plus_btn, #minus_btn').hide();

            $('.add-account-link').hide();

            $('.nav-link').click(function(e) {
                var tabName = $(this).attr('href').substring(1);
                var allowedTabs = ['profile', 'personality', 'family', 'organization', 'personal_account'];
                if (allowedTabs.includes(tabName)) {
                    loadForm(tabName);
                    $(this).tab('show');
                    $('.nav-link').removeClass('active-tab');
                    $(this).addClass('active-tab');
                    sessionStorage.setItem('tabName', tabName);
                }
            });

            function loadForm(tabName) {
                $('.tab-pane').hide();
                $('#' + tabName).show();
                if (tabName == 'profile') {
                    $('#address').each(function() {
                        adjustTextareaHeight(this);
                    });
                }
                if (tabName == 'personality') {
                    $('#character, #favorite_food, #favorite_drink, #allergy, #knowledge').each(function() {
                        adjustTextareaHeight(this);
                    });
                }
            }

            $(".datepicker").datepicker({
                format: 'dd-mm-yy',
                autoclose: true
            });

            $("#date_birth").datepicker({
                format: 'dd-mm-yy',
                autoclose: true
            });

            // Function to dynamically adjust textarea height
            function adjustTextareaHeight(textarea) {
                // Reset rows to 1 to calculate correct scroll height
                textarea.rows = 1;
                // Set the rows attribute based on the scroll height divided by the line height
                textarea.rows = Math.ceil(textarea.scrollHeight / parseFloat($(textarea).css('line-height')));
            }

            // Call adjustTextareaHeight when textarea content changes for all relevant textareas
            $('#character, #favorite_food, #favorite_drink, #allergy, #knowledge, #address').on('input', function() {
                adjustTextareaHeight(this);
            });

            setTimeout(function(){
                $('#alert_message').fadeOut();
            }, 3000);

            fetchData(null, {{ $user->id }}); // Call the function to fetch and populate data when the page loads

            // Function to fetch data from the server
            function fetchData(url = null, selectedUserId) {
                let requestUrl = url || "{{ route('users.organization.datatables.ajax') }}"; // Use the provided URL or the default route
                $.ajax({
                    url: requestUrl,
                    method: 'GET',
                    data: { userId: selectedUserId },
                    success: function(response) {
                        updateTable(response.data); // Call the function to update the table with received data
                        // updatePagination(response.pagination); // Call the function to update pagination links
                    },
                    error: function(xhr, status, error) {
                        console.error(error); // Handle errors if any
                    }
                });
            }

            // Function to update the table with received data
            function updateTable(data) {
                // Clear the existing table rows
                $('tbody').empty();

                // Define a counter variable
                let counter = 1;

                // Loop through the received data and append rows to the table
                $.each(data, function(index, item) {
                    $('tbody').append(`
                        <tr>
                            <td class="ps-4">${counter++}</td>
                            <td>${item.START_YEAR !== null ? item.START_YEAR : ''}</td>
                            <td class="text-center">${item.END_YEAR !== null ? item.END_YEAR : ''}</td>
                            <td class="text-center">${item.COMPANY !== null ? item.COMPANY : ''}</td>
                            <td class="text-center">${item.POSITION !== null ? item.POSITION : ''}</td>
                            <td class="text-center">${item.AREA !== null ? item.AREA : ''}</td>
                            <td class="text-center">
                                ${item.ACTION !== null ? item.ACTION : ''}
                            </td>
                        </tr>
                    `);
                });
            }

            $('#direct_officer_select').select2({
                ajax: {
                    url: '{{ route('users.list') }}',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            search: params.term, // search term
                            page: params.page || 1
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.results,
                            pagination: {
                                more: data.pagination.more
                            }
                        };
                    },
                    cache: true
                },
                placeholder: 'Select a user',
                minimumInputLength: 1,
            });
        });

        function alertClose() {
            sessionStorage.removeItem('success');
            sessionStorage.removeItem('failed');
            $('#alert_message').load(location.href + ' #alert_message');
        }

        // Functionality for adding and deleting phones
        var maxPhones = 3;
        var currentPhone = 1;

        @if(!empty($user->phone_2) && empty($user->phone_3))
            currentPhone = 2;
        @elseif(!empty($user->phone_2) && !empty($user->phone_3))
            currentPhone = 3;
        @endif

        function addPhone() {
            if (currentPhone < maxPhones) {
                currentPhone++;
                var name_input = 'phone_'+currentPhone;
                if (currentPhone > 0) {
                    $('#delete_phone').show();
                }
                var newPhoneSection = document.createElement('div');
                newPhoneSection.classList.add('phone-section');
                if (currentPhone == 1) {
                    name_input = 'phone';
                }
                newPhoneSection.innerHTML = `
                    <div id="phone_${currentPhone}">
                        <label class="col-lg-12 col-form-label text-right"><strong>Phone ${currentPhone}</strong> :</label>
                        <div class="col-lg-11">
                            <input name="${name_input}" type="text" class="form-control" required>
                        </div>
                    </div>
                `;
                document.querySelector('.add-phone').insertAdjacentElement('beforebegin', newPhoneSection);
            } else {
                // Add error message if it doesn't exist
                if (!errorMessagePhone) {
                    errorMessagePhone = document.createElement('div');
                    errorMessagePhone.textContent = 'Max phone is 3';
                    errorMessagePhone.style.color = 'red';
                    document.querySelector('.add-phone').appendChild(errorMessagePhone);
                }
            }
        }

        function deletePhone() {
            if (currentPhone > 0) {
                var phoneToDelete = document.querySelector(`[id="phone_${currentPhone}"]`);
                phoneToDelete.remove();
                currentPhone--;
                if (currentPhone == 0) {
                    $('#delete_phone').hide();
                }
                // Remove error message if phones are less than max
                if (errorMessagePhone && currentPhone < maxPhones) {
                    errorMessagePhone.remove();
                    errorMessagePhone = null;
                }
            }
        }

        // Functionality for adding and deleting schools
        var maxSchools = 3;
        var currentSchool = 1;

        @if(!empty($user->school_2) && empty($user->school_3))
            currentSchool = 2;
        @elseif(!empty($user->school_2) && !empty($user->school_3))
            currentSchool = 3;
        @endif

        function addSchool() {
            if (currentSchool < maxSchools) {
                currentSchool++;
                var name_input = 'school_'+currentSchool;
                if (currentSchool > 0) {
                    $('#delete_school').show();
                }
                var newSchoolSection = document.createElement('div');
                newSchoolSection.classList.add('school-section');
                if (currentSchool == 1) {
                    name_input = 'school';
                }
                newSchoolSection.innerHTML = `
                    <div id="school_${currentSchool}">
                        <label class="col-lg-12 col-form-label text-right"><strong>School ${currentSchool}</strong> :</label>
                        <div class="col-lg-11">
                            <input name="${name_input}" type="text" class="form-control" required>
                            @error('${name_input}')
                                <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                `;
                document.querySelector('.add-school').insertAdjacentElement('beforebegin', newSchoolSection);
            } else {
                // Add error message if it doesn't exist
                if (!errorMessageSchool) {
                    errorMessageSchool = document.createElement('div');
                    errorMessageSchool.textContent = 'Max schools is 3';
                    errorMessageSchool.style.color = 'red';
                    document.querySelector('.add-school').appendChild(errorMessageSchool);
                }
            }
        }


        function deleteSchool() {
            if (currentSchool > 0) {
                var schoolToDelete = document.querySelector(`[id="school_${currentSchool}"]`);
                schoolToDelete.remove();
                currentSchool--;
                if (currentSchool == 0) {
                    $('#delete_school').hide();
                }
                // Remove error message if schools are less than max
                if (errorMessageSchool && currentSchool < maxSchools) {
                    errorMessageSchool.remove();
                    errorMessageSchool = null;
                }
            }
        }

        // Functionality for adding and deleting hobbies
        var maxHobbies = 3;
        var currentHobby = 1;

        @if(!empty($user->hobby_2) && empty($user->hobby_3))
            currentHobby = 2;
        @elseif(!empty($user->hobby_2) && !empty($user->hobby_3))
            currentHobby = 3;
        @endif

        function addHobby() {
            if (currentHobby < maxHobbies) {
                currentHobby++;
                if (currentHobby > 0) {
                    $('#delete_hobby').show();
                }
                var newHobbySection = document.createElement('div');
                newHobbySection.classList.add('hobby-section');
                var hobbyIndex = currentHobby === 1 ? '' : '_' + currentHobby;
                newHobbySection.innerHTML = `
                    <div id="hobby${hobbyIndex}">
                        <label class="col-lg-12 col-form-label text-right"><strong>Hobby ${currentHobby}</strong> :</label>
                        <div class="col-lg-11">
                            <select name="hobby${hobbyIndex}" class="form-control select2" style="width: 100%;" required>
                                <option value="">Choose Hobby</option>
                                @foreach($hobby_options as $hobby_option_value => $hobby_option_label)
                                    <option value="{{$hobby_option_value}}">{{$hobby_option_label}}</option>
                                @endforeach
                            </select>
                            @error('hobby${hobbyIndex}')
                                <div class="alert alert-danger col-12" style="margin-top: 5px; color: white; font-size: small;">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                `;
                document.querySelector('.add-hobby').insertAdjacentElement('beforebegin', newHobbySection);
                // Initialize select2 for the newly added select element
                $('.select2').select2();
            } else {
                // Add error message if it doesn't exist
                if (!errorMessageHobby) {
                    errorMessageHobby = document.createElement('div');
                    errorMessageHobby.textContent = 'Max hobbies is 3';
                    errorMessageHobby.style.color = 'red';
                    document.querySelector('.add-hobby').appendChild(errorMessageHobby);
                }
            }
        }

        function deleteHobby() {
            if (currentHobby > 0) {
                var hobbyToDelete = document.querySelector(`[id="hobby_${currentHobby}"]`);
                hobbyToDelete.remove();
                currentHobby--;
                if (currentHobby == 0) {
                    $('#delete_hobby').hide();
                }
                // Remove error message if hobbies are less than max
                if (errorMessageHobby && currentHobby < maxHobbies) {
                    errorMessageHobby.remove();
                    errorMessageHobby = null;
                }
            }
        }



        // Functionality for adding family members
        var maxFamilyMembers = 10;
        var currentFamilyMember = 1;

        function addFamilyMember(noFamilyMember) {
            currentFamilyMember = $('.family-section').length;
            if (currentFamilyMember < maxFamilyMembers) {
                var newFamilySection = `
                <div class="offset-lg-2 offset-sm-1 my-4 col-lg-8 col-sm-11 col-12">
                    <div class="family-section row" id="data_family_`+(currentFamilyMember+1)+`">
                        <strong class="pt-2">Family Member `+(currentFamilyMember+1)+`</strong>
                        <div class="col-12">
                            <!-- Default family member form fields -->
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right">
                                        <strong>Full Name :</strong>
                                        <span style="color: red;">*</span>
                                    </label>
                                    <div class="col-lg-11">
                                        <input name="family[`+(currentFamilyMember)+`][fullname]" type="text" value="" class="form-control" required>
                                        <div class="text-danger" id="error_fullname" hidden>The fullname field is required.</div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Nick Name</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[`+(currentFamilyMember)+`][nickname]" type="text" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Family Status</strong> : <span style="color: red;">*</span></label>
                                    <div class="col-lg-11">
                                        <select name="family[`+(currentFamilyMember)+`][family_status]" class="form-control select2" style="width: 100%;" required>
                                            <option value="">Choose Status</option>
                                            <option value="couple">Suami/Istri</option>
                                            <option value="kid">Anak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Gender</strong> :</label>
                                    <div class="col-lg-11">
                                        <select name="family[`+(currentFamilyMember)+`][gender]" class="form-control select2" style="width: 100%;">
                                            <option value="">Choose Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Place of Birth</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[`+(currentFamilyMember)+`][place_birth]" type="text" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Date of birth</strong> :</label>
                                    <div class="col-lg-11">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="text" class="form-control" id="date_birth" name="family[`+(currentFamilyMember)+`][date_birth]" value="" autocomplete="off">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Work</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[`+(currentFamilyMember)+`][work]" type="text" value="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="family-school-section">
                                        <div id="family_`+(currentFamilyMember+1)+`_school_1">
                                            <label class="col-lg-12 col-form-label text-right"><strong>School 1</strong> :</label>
                                            <div class="col-lg-11 school-section-`+(currentFamilyMember+1)+`">
                                                <input name="family[`+(currentFamilyMember)+`][school]" type="text" value="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-school-family pt-2 row add-school-family-`+(currentFamilyMember+1)+`">
                                        <div id="add_school_family" class="col-6">
                                            <a href="#" onclick="addSchoolFamily(`+(currentFamilyMember+1)+`)" style="color: #009DB6;">+ Add more school</a>
                                        </div>
                                        <div id="delete_school_family" class="col-6">
                                            <a href="#" onclick="deleteSchoolFamily(`+(currentFamilyMember+1)+`)" style="color: #009DB6;">- Delete school</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style="border-top: 2px solid black;">
                    </div>
                </div>
                `;
                $('.addFamilyMember').before(newFamilySection);

                // Initialize select2 for the newly added select element
                $('.select2').select2();

                $("#date_birth").datepicker({
                    format: 'dd-mm-yy',
                    autoclose: true
                });
            } else {
                // Add error message if it doesn't exist
                if (!errorMessage) {
                    errorMessage = document.createElement('div');
                    errorMessage.textContent = 'Maximum family members reached';
                    errorMessage.style.color = 'red';
                    document.querySelector('.add-family-member').appendChild(errorMessage);
                }
            }
        }

        // Functionality for adding personal accounts
        function addPersonalAccount() {
            var maxPersonalAccounts = 20;
            var currentPersonalAccounts = $('.personal_account-section').length;

            if (currentPersonalAccounts < maxPersonalAccounts) {
                var newPersonalAccountSection = `
                    <div class="personal_account-section row pt-3" id="section`+(currentPersonalAccounts+1)+`">
                        <div class="col-lg-3 col-sm-4 col-5">
                            <label class="col-form-label text-right"><strong>Application</strong> :</label>
                        </div>
                        <div class="col-lg-4 col-sm-7 col-6" style="margin-top: 8px">
                            <select name="personal_account[`+(currentPersonalAccounts)+`]" class="form-control select2" style="width: 100%;" required>
                                <option value="">Choose App</option>
                                @foreach($personal_account_options as $personal_account_option_value => $personal_account_option_label)
                                <option value="{{$personal_account_option_value}}">{{$personal_account_option_label}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-1" style="margin-top: 13px; margin-left:-15px">
                            <img style="height: 15px; width:15px" class="plus_btn" id="plus_btn" src="{{ asset('assets/img/plus.png') }}" alt="..." onclick="addValue(document.getElementById('section`+(currentPersonalAccounts+1)+`'))">
                        </div>
                        <div class="col-lg-7 offset-lg-3 col-md-10 col-sm-10 col-12 offset-md-4 offset-sm-4 offset-1 row" id="input_app_`+(currentPersonalAccounts)+`_0" style="margin-top: 10px">
                            <div class="col-11 input_PA_`+(currentPersonalAccounts)+`" style="margin-left:-13px">
                                <input name="character[`+(currentPersonalAccounts)+`][0]" type="text" value="" class="form-control" required>
                            </div>
                            <div class="col-1" style="margin-top: 5px; margin-left:-15px">
                                <img style="height: 15px; width:15px" class="minus_btn" id="minus_btn" src="{{ asset('assets/img/minus.png') }}" alt="..."onclick="deleteValue(document.getElementById('input_app_`+(currentPersonalAccounts)+`_0'))">
                            </div>
                        </div>
                    </div>
                `;
                $('.addPersonnalAccount').before(newPersonalAccountSection);

                // Initialize select2 for the newly added select element
                $('.select2').select2();
            } else {
                // Show error message if max limit reached
                alert('Max personal accounts limit reached.');
            }
        }

        var maxSchoolsFamily = 3;
        var currentSchoolFamily = 1;

        function addSchoolFamily(id) {
            currentSchoolFamily = $('.school-section-' + id).length;
            if (currentSchoolFamily < maxSchoolsFamily) {
                currentSchoolFamily++;
                var name_input = 'family['+(id-1)+'][school_'+(currentSchoolFamily)+']';

                if (currentSchoolFamily > 0) {
                    $('#delete_school_family').show();
                }
                var newSchoolSection = document.createElement('div');
                newSchoolSection.classList.add('family-school-section');
                if (currentSchoolFamily < 1) {
                    name_input = 'family['+(id-1)+'][school]';
                }
                newSchoolSection.innerHTML = `
                    <div id="family_${id}_school_${currentSchoolFamily}">
                        <label class="col-lg-12 col-form-label text-right"><strong>School ${currentSchoolFamily}</strong> :</label>
                        <div class="col-lg-11 school-section-${id}">
                            <input name="${name_input}" type="text" class="form-control" required>
                        </div>
                    </div>
                `;
                document.querySelector('.add-school-family-'+id).insertAdjacentElement('beforebegin', newSchoolSection);
            } else {
                // Add error message if it doesn't exist
                alert('Max school limit reached.');
            }
        }

        function deleteSchoolFamily(id) {
            currentSchoolFamily = $('.school-section-' + id).length;
            if (currentSchoolFamily > 0) {
                var familySchoolToDelete = document.querySelector(`[id="family_${id}_school_${currentSchoolFamily}"]`);
                familySchoolToDelete.remove();
                currentSchoolFamily--;
                if (currentSchoolFamily == 0) {
                    $('#delete_school_family').hide();
                }
                // Remove error message if schools are less than max
                if (errorMessageSchoolFamily && currentSchoolFamily < maxSchoolsFamily) {
                    errorMessageSchoolFamily.remove();
                    errorMessageSchoolFamily = null;
                }
            }
        }

        function previewImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#uploaded-image').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]); // Read the selected file as a URL
            }
        }

        // Set default date format globally
        $.fn.datepicker.defaults.format = "dd-mm-yyyy";

        function toggleActiveTab(tab) {
            const tabs = document.querySelectorAll('.nav-link');
            tabs.forEach(tab => tab.classList.remove('active'));
            tab.classList.add('active');
        }

        // Add event listener for detail-organization links
        $(document).on('click', '.detail-organization', function(e) {
            e.preventDefault();
            var organizationId = $(this).data('organization-id');

            // Perform AJAX request to fetch organization details
            $.ajax({
                url: "{{ route('users.organization_show', ['user_id' => $user->id, 'organization_id' => ':organizationId']) }}".replace(':organizationId', organizationId),
                method: 'GET',
                success: function(response) {
                    // Populate the modal body with organization details
                    $('#organizationDetailModalBody').html(response);
                    $('.select2').select2();
                },
                error: function(xhr, status, error) {
                    console.error(error);
                    // Handle error
                }
            });
        });

        $(document).on('click', '.delete-organization', function(e) {
            e.preventDefault();
            var deleteUrl = $(this).data('delete-url');

            // Get the CSRF token from the meta tag
            var csrfToken = $('meta[name="csrf-token"]').attr('content');

            // Send a DELETE request to the delete URL
            $.ajax({
                url: deleteUrl,
                method: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': csrfToken // Include the CSRF token in the headers
                },
                success: function(response) {
                    location.reload();
                },
                error: function(xhr, error) {
                }
            });
        });

    </script>

@endpush

