<div class="mx-4 col-12">
    <div class="row col-12" id="organization_detail"  style="height: 500px; overflow-y: scroll;">
        <div class="offset-lg-1 col-11">
            <div >
                <input name="id_organization" type="hidden" value="{{ isset($user->organization['id']) ? $user->organization['id'] : '' }}" class="form-control" hidden>
                <div class="form-group row">
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>Start Year</strong> :</label>
                        <div class="col-lg-11">
                            <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control" id="start_year" name="start_year" value="{{ isset($user->organization['start_year']) ? date('d-m-Y', strtotime($user->organization['start_year'])) : '' }}" autocomplete="off" disabled>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>End Year</strong> :</label>
                        <div class="col-lg-11">
                            <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control" id="end_year" name="end_year" value="{{ isset($user->organization['end_year']) ? date('d-m-Y', strtotime($user->organization['end_year'])) : '' }}" autocomplete="off" disabled>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right">
                            <strong>Company :</strong>
                            <span style="color: red;">*</span>
                        </label>
                        <div class="col-lg-11">
                            <select id="company" name="company" class="form-control select2" style="width: 100%;" disabled>
                                <option value="">Choose Company</option>
                                @foreach($company_options as $company_value => $company_label)
                                    <option value="{{$company_value}}" {{ isset($user->organization['company_id']) && $user->organization['company_id'] == $company_value ? 'selected' : '' }}>{{$company_label}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>Group Company :</strong></label>
                        <div class="col-lg-11">
                            <select id="group_company" name="group_company" class="form-control select2" style="width: 100%;" disabled>
                                <option value="">Choose Group Company</option>
                                @foreach($group_company_options as $group_company_option_value => $group_company_option_label)
                                    <option value="{{$group_company_option_value}}" {{ isset($user->organization['group_company_id']) && $user->organization['group_company_id'] == $group_company_option_value ? 'selected' : ''}}>{{$group_company_option_label}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>Area :</strong></label>
                        <div class="col-lg-11">
                            <select id="area" name="area" class="form-control select2" style="width: 100%;" disabled>
                                <option value="">Choose Area</option>
                                @foreach($area_options as $area_value => $area_label)
                                    <option value="{{$area_value}}" {{ isset($user->organization['area_id']) && $user->organization['area_id'] == $area_value ? 'selected' : '' }}>{{$area_label}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>Sub Area :</strong></label>
                        <div class="col-lg-11">
                            <select id="sub_area" name="sub_area" class="form-control select2" style="width: 100%;" disabled>
                                <option value="">Choose Sub Area</option>
                                @foreach($sub_area_options as $sub_area_value => $sub_area_label)
                                    <option value="{{$sub_area_value}}" {{ isset($user->organization['sub_area_id']) && $user->organization['sub_area_id'] == $sub_area_value ? 'selected' : '' }}>{{$sub_area_label}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>Position :</strong></label>
                        <div class="col-lg-11">
                            <input name="position" type="text" value="{{ optional($user->organization)->position }}" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <label class="col-lg-12 col-form-label text-right"><strong>Direct Officer :</strong></label>
                        <div class="col-lg-11">
                            <select id="direct_officer_select" name="direct_officer" class="form-control select2" style="width: 100%;" disabled onchange="handleDirectOfficerChange(this)">
                                <option value="">Choose Direct Officer</option>
                                @if (isset($direct_officer_selected))
                                    <option value="{{$direct_officer_selected->id}}" selected>{{$direct_officer_selected->fullname}}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-lg-11 pt-1" id="section-member">
                    <label id="memberListLabel" class="col-lg-12 col-form-label text-right"></label>
                    <div class="col-lg-11" id="memberListContainer">
                        {{-- Member list will be populated here --}}
                    </div>
                </div>
                <div class="form-group row">

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        @if (isset($direct_officer_selected))
            handleDirectOfficerChangeLogic({{ $direct_officer_selected->id }});
        @endif
        $('.select2').select2({
            dropdownParent: $('#organizationDetailModal')
        });

        // Add event listener for company select
        // Add event listener for company select
        $('#company').change(function() {
            var companyId = $(this).val(); // Get the selected company ID

            // Trigger AJAX request to fetch options for group company, area, and sub area
            $.ajax({
                url: "{{ route('fetch.company.options') }}", // Replace with your route for fetching options
                method: 'GET',
                data: { company_id: companyId },
                success: function(response) {
                    // Update options for group company select
                    updateSelectOptions(response.group_company_options, '#group_company');
                    // Update options for group company select
                    updateSelectOptions(response.area_options, '#area');
                    // Update options for group company select
                    updateSelectOptions(response.sub_area_options, '#sub_area');

                    // Update options for area select
                    // Add similar updateSelectOptions function for area select

                    // Update options for sub area select
                    // Add similar updateSelectOptions function for sub area select

                    // Initialize select2 for the newly added select element
                    // $('.select2').select2({
                    //     dropdownParent: $('#organizationDetailModal')
                    // });
                    $('#direct_officer_select').select2({
                        ajax: {
                            url: '{{ route('users.list') }}',
                            dataType: 'json',
                            delay: 250,
                            data: function (params) {
                                return {
                                    search: params.term, // search term
                                    page: params.page || 1
                                };
                            },
                            processResults: function (data, params) {
                                params.page = params.page || 1;

                                return {
                                    results: data.results,
                                    pagination: {
                                        more: data.pagination.more
                                    }
                                };
                            },
                            cache: true
                        },
                        placeholder: 'Select User',
                        minimumInputLength: 1,
                        dropdownParent: $('#organizationDetailModal')
                    });
                },
                error: function(xhr, status, error) {
                    console.error(error); // Handle error response
                }
            });
        });

        // Enable group_company select just before form submission
        $('form').submit(function(e) {
            // Prevent the default form submission
            e.preventDefault();

            // Enable the group_company select temporarily
            $('#group_company').prop('disabled', false);
            $('#area').prop('disabled', false);
            $('#sub_area').prop('disabled', false);

            // Submit the form
            this.submit();

            // Disable the group_company select again
            $('#group_company').prop('disabled', true);
            $('#area').prop('disabled', true);
            $('#sub_area').prop('disabled', true);
        });


        $('#direct_officer_select').select2({
            ajax: {
                url: '{{ route('users.list') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.results,
                        pagination: {
                            more: data.pagination.more
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Select User',
            minimumInputLength: 1,
            dropdownParent: $('#organizationDetailModal')
        });
    });
    $('#direct_officer_select').select2({
            ajax: {
                url: '{{ route('users.list') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term, // search term
                        page: params.page || 1
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;

                    return {
                        results: data.results,
                        pagination: {
                            more: data.pagination.more
                        }
                    };
                },
                cache: true
            },
            placeholder: 'Select User',
            minimumInputLength: 1,
        });
    // $('.select2').select2({
    //     dropdownParent: $('#organizationDetailModal')
    // });


    function updateSelectOptions(options, selectId) {
        var selectElement = $(selectId);
        selectElement.empty(); // Clear existing options
        var textOption = '';
        if (selectId == '#group_company') {
            $.each(options, function(key, value) {
                selectElement.append($('<option>', { value: key, text: value, selected: 'selected' }));
                // selectElement.prop('disabled', true);
            });
        } else {
            if (selectId == '#area') {
                textOption = 'Area';
            } else if (selectId == '#sub_area') {
                textOption = 'Sub Area';
            }
            selectElement.append($('<option>', { value: '', text: 'Choose '+textOption })); // Add default option
            // Add options from the response
            $.each(options, function(key, value) {
                selectElement.append($('<option>', { value: key, text: value }));
            });
            selectElement.prop('disabled', false);
        }

    }

    function handleDirectOfficerChange(selectElement) {
        // Get the selected value
        var selectedValue = selectElement.value;

        // Call the function to handle the change
        handleDirectOfficerChangeLogic(selectedValue);
    }

    function handleDirectOfficerChangeLogic(selectedValue) {
        if (selectedValue != null) {
            $.ajax({
                url: "{{ route('users.member.ajax') }}",
                method: 'GET',
                data: { user_id: selectedValue },
                success: function(response) {
                    // Handle success response
                    updateMemberList(response);
                },
                error: function(xhr, status, error) {
                    // Handle error response
                    console.error(xhr.responseText);
                }
            });
        }
    }

    @if(isset($user_id) && isset($user_fullname))
        // Create a new option with the user's id and fullname
        var newOption = new Option('{{ $user_fullname }}', '{{ $user_id }}', true, true);
        // Append it to the select element
        $('#searchUser').append(newOption).trigger('change');
        // Optionally, trigger a change event to update any dependent elements
        $('#searchUser').trigger({
            type: 'select2:select',
            params: {
                data: {
                    id: '{{ $user_id }}',
                    text: '{{ $user_fullname }}'
                }
            }
        });
    @endif
    function updateMemberList(memberData) {
        var userData = memberData.target_user; // Ensure userData is declared with var, let, or const
        var memberListContainer = document.getElementById('memberListContainer');
        var memberListLabel = document.getElementById('memberListLabel');

        // Check if userData is not null and has the fullname property
        if (userData && userData.fullname) {
            var memberDataArray = memberData.data;

            // Update the label text with the user's fullname
            memberListLabel.textContent = "List Members of " + userData.fullname + ":";

            // Clear previous content
            memberListContainer.innerHTML = '';

            // Check if memberDataArray is an array and has elements
            if (Array.isArray(memberDataArray) && memberDataArray.length > 0) {

                // Iterate through the member data and create list items
                memberDataArray.forEach(function(member, index) {
                    // Create a div element for each member
                    var memberDiv = document.createElement('div');

                    // Assuming member object has properties like 'username', 'fullname', etc.
                    // Create text content for each member
                    var memberInfo = (index + 1) + '. ' + member.fullname;

                    // Set the text content of the member div
                    memberDiv.textContent = memberInfo;

                    // Append the member div to the member list container
                    memberListContainer.appendChild(memberDiv);
                });
            } else {
                // Handle case where memberDataArray is empty or not an array
                var emptyMessageDiv = document.createElement('div');
                emptyMessageDiv.style.marginLeft = '5px';
                emptyMessageDiv.style.color = 'gray';
                emptyMessageDiv.textContent = '- Data member is empty';

                // Append the empty message div to the member list container
                memberListContainer.appendChild(emptyMessageDiv);

            }
        } else {
            // Handle case where userData is null or does not have the fullname property
            // console.error('User data is null or missing the fullname property.');
        }

    }


</script>
