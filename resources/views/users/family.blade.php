<div class="tab-pane fade" id="family" role="tabpanel" aria-labelledby="family-tab">
    <!-- Family form inputs -->
    <form id="familyForm" class="form-material" role="form" action="{{ route('users.update', $user->id) }}" method="POST">
        <div class="card-header row mx-0" style="background-color: #4AAAC0; height: 75px">
            <div class="col-lg-8 col-sm-11">
                <h5 class="text-white font-weight-bolder">Family</h5>
            </div>
        </div>
        <div class="mx-4 col-11">
            <div class="row col-12">
                @method('PUT')
                {{ csrf_field() }}
                <input name="tabName" type="hidden" value="family" class="form-control" hidden>

                <!-- Display one set of family member fields with empty values if family count is less than 1 -->
                @if(count($user->family) < 1)
                <span id="data_empty_id" class="pt-3">
                        Data Family Member is empty.
                </span>
                <div class="offset-lg-2 offset-sm-1 my-4 col-lg-8 col-sm-11 col-12">
                    <div class="family-section row" id="data_family_1" hidden>
                        <strong class="pt-2">Family Member 1</strong>
                        <!-- Default family member form fields -->
                        <div class="col-12">
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right">
                                        <strong>Full Name :</strong>
                                        <span style="color: red;">*</span>
                                    </label>
                                    <div class="col-lg-11">
                                        <input name="family[0][fullname]" type="text" value="" class="form-control" required disabled>
                                        <div class="text-danger" id="error_fullname" hidden>The fullname field is required.</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Nick Name</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[0][nickname]" type="text" value="" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Family Status</strong> : <span style="color: red;">*</span></label>
                                    <div class="col-lg-11">
                                        <select name="family[0][family_status]" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose Status</option>
                                            <option value="couple">Suami/Istri</option>
                                            <option value="kid">Anak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Gender</strong> :</label>
                                    <div class="col-lg-11">
                                        <select name="family[0][gender]" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Place of Birth</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[0][place_birth]" type="text" value="" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Date of birth</strong> :</label>
                                    <div class="col-lg-11">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="text" class="form-control" id="date_birth" name="family[0][date_birth]" value="" autocomplete="off" disabled>
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Work</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[0][work]" type="text" value="" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="family-school-section">
                                        <div id="family_1_school_1">
                                            <label class="col-lg-12 col-form-label text-right"><strong>School 1</strong> :</label>
                                            <div class="col-lg-11 school-section-1">
                                                <input name="family[0][school]" type="text" value="" class="form-control" disabled>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="add-school-family pt-2 row add-school-family-1">
                                        <div id="add_school_family" class="col-6">
                                            <a href="#" onclick="addSchoolFamily(1)" style="color: #009DB6;" hidden>+ Add more school</a>
                                        </div>
                                        <div id="delete_school_family" class="col-6">
                                            <a href="#" onclick="deleteSchoolFamily(1)" style="color: #009DB6;" hidden>- Delete school</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr style="border-top: 2px solid black;">
                    </div>
                </div>
                @endif

                <!-- Loop through existing family members -->
                @foreach($user->family as $key => $familyMember)
                <div class="offset-lg-2 offset-sm-1 my-4 col-lg-8 col-sm-11 col-12">
                    <div class="family-section row" id="data_family_{{$key}}">
                        <input name="family[{{$key}}][id]" type="hidden" value="{{$familyMember->id}}" class="form-control" hidden>
                        <strong class="pt-2">Family Member {{ $key + 1 }}</strong>
                        <div class="col-12">
                            <!-- Family member form fields -->
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right">
                                        <strong>Full Name :</strong>
                                        <span style="color: red;">*</span>
                                    </label>
                                    <div class="col-lg-11">
                                        <input type="text" name="family[{{$key}}][fullname]" id="fullname{{$key}}" value="{{ $familyMember->fullname }}" class="form-control" required disabled>
                                        <div class="text-danger" id="error_fullname{{$key}}" hidden>The fullname field is required.</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Nick Name</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[{{$key}}][nickname]" type="text" value="{{ $familyMember->nickname }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Family Status</strong> : <span style="color: red;">*</span></label>
                                    <div class="col-lg-11">
                                        <select name="family[{{$key}}][family_status]" class="form-control select2" style="width: 100%;" required disabled>
                                            <option value="">Choose Status</option>
                                            <option value="couple" {{$familyMember->family_status == 'couple' ? 'selected' : ''}}>Suami/Istri</option>
                                            <option value="kid" {{$familyMember->family_status == 'kid' ? 'selected' : ''}}>Anak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Gender</strong> :</label>
                                    <div class="col-lg-11">
                                        <select name="family[{{$key}}][gender]" class="form-control select2" style="width: 100%;" disabled>
                                            <option value="">Choose Gender</option>
                                            <option value="male" {{$familyMember->gender == 'male' ? 'selected' : ''}}>Male</option>
                                            <option value="female" {{$familyMember->gender == 'female' ? 'selected' : ''}}>Female</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Place of Birth</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[{{$key}}][place_birth]" type="text" value="{{$familyMember->place_birth}}" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Date of birth</strong> :</label>
                                    <div class="col-lg-11">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="text" class="form-control" id="date_birth{{$key}}" name="family[{{$key}}][date_birth]" value="{{$familyMember->date_birth}}" autocomplete="off" disabled>
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-6 col-sm-6">
                                    <label class="col-lg-12 col-form-label text-right"><strong>Work</strong> :</label>
                                    <div class="col-lg-11">
                                        <input name="family[{{$key}}][work]" type="text" value="{{$familyMember->work}}" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div class="family-school-section">
                                        <div id="family_{{$key+1}}_school_1">
                                            <label class="col-lg-12 col-form-label text-right"><strong>School 1</strong> :</label>
                                            <div class="col-lg-11 school-section-{{$key+1}}">
                                                <input name="family[{{$key}}][school]" type="text" value="{{$familyMember->school}}" class="form-control" disabled>
                                            </div>
                                        </div>

                                        @if (!empty($familyMember->school_2))
                                        <div id="family_{{$key+1}}_school_2">
                                            <label class="col-lg-12 col-form-label text-right"><strong>School 2</strong> :</label>
                                            <div class="col-lg-11 school-section-{{$key+1}}">
                                                <input name="family[{{$key}}][school_2]" type="text" value="{{$familyMember->school_2}}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        @endif

                                        @if (!empty($familyMember->school_3))
                                        <div id="family_{{$key+1}}_school_3">
                                            <label class="col-lg-12 col-form-label text-right"><strong>School 3</strong> :</label>
                                            <div class="col-lg-11 school-section-{{$key+1}}">
                                                <input name="family[{{$key}}][school_3]" type="text" value="{{$familyMember->school_3}}" class="form-control" disabled>
                                            </div>
                                        </div>
                                        @endif
                                    </div>

                                    <div class="add-school-family pt-2 row add-school-family-{{$key+1}}">
                                        <div id="add_school_family" class="col-6">
                                            <a href="#" onclick="addSchoolFamily({{$key+1}})" style="color: #009DB6;" hidden>+ Add more school</a>
                                        </div>
                                        <div id="delete_school_family" class="col-6">
                                            <a href="#" onclick="deleteSchoolFamily({{$key+1}})" style="color: #009DB6;" hidden>- Delete school</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="border-top: 2px solid black;">
                </div>
                @endforeach
                <!-- Add more family member link -->
                <div class="addFamilyMember pt-1 row">
                    <div id="add_family_member" class="col-6">
                        <a href="#" onclick="addFamilyMember(@php count($user->family)+1 @endphp)" style="color: #009DB6;" hidden>+ Add more family member</a>
                    </div>
                    <div id="delete_family_member" class="col-6">
                        <a href="#" onclick="deleteFamilyMember()" style="color: #009DB6;" hidden>- Delete last family member</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Buttons -->
        <div class="form-group row">
            <div class="offset-lg-6 col-lg-6 offset-md-6 col-md-6 pt-5 offset-sm-6 col-sm-6 offset-1 col-11">
                @if(count($user->family) < 1)
                    <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="edit_btn_family" onclick="editFamilyChanges()">
                        <i class="fa fa-plus icon-tab-action"></i> Add
                    </button>
                @else
                    <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="edit_btn_family" onclick="editFamilyChanges()">
                        <i class="fa fa-pencil icon-tab-action"></i>  Edit
                    </button>
                @endif
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="button" style="background-color: #00526B" id="cancel_btn_family" onclick="cancelFamilyChanges()">
                    <i class="fa fa-times icon-tab-action"></i>  Cancel
                </button>
                <button class="btn btn-sm text-white font-weight-bolder button-tab-action" type="submit" style="background-color: #00526B" id="save_btn_family">
                    <i class="fa fa-save icon-tab-action"></i>  Save
                </button>
            </div>
        </div>
    </form>
</div>

<!-- JavaScript to handle form editing -->
<script>
    var errorMessageSchoolFamily;
    function addFamilyMember() {
        // Get the container element for family members
        var familyContainer = $('#familyForm .row:last');

        // Clone the last family member form and append it to the container
        var newFamilyMember = familyContainer.clone();

        // Increment the family member number in the label
        var familyMemberNumber = familyContainer.find('strong').text().split(' ')[2];
        var newFamilyMemberNumber = parseInt(familyMemberNumber) + 1;
        newFamilyMember.find('strong').text('Family Member ' + newFamilyMemberNumber);

        // Clear input values in the cloned form
        newFamilyMember.find('input, select').val('');

        // Insert the new family member form after the last one
        familyContainer.after(newFamilyMember);

        // Show the newly added family member form
        newFamilyMember.show();

        // Hide the "Add more family member" link in the last form
        familyContainer.find('.add-school-family a').removeAttr('hidden');
        familyContainer.find('.addFamilyMember a').removeAttr('hidden');
    }

    function editFamilyChanges() {
        $('#data_family_1').removeAttr('hidden');
        $('#data_empty_id').attr('hidden', true);

        // Enable all input fields within the family form
        $('#familyForm input, #familyForm select').prop('disabled', false);

        // Show the "Add more family member" link
        $('.add-school-family a').removeAttr('hidden');
        $('.addFamilyMember a').removeAttr('hidden');

        // Hide the edit button and show the cancel and save buttons
        $('#edit_btn_family').hide();
        $('#cancel_btn_family').show();
        $('#save_btn_family').show();

        $('#family-section').removeAttr('hidden');
        $('#data_empty_id').attr('hidden', true);
    }

    function cancelFamilyChanges() {
        // Disable all input fields within the family form
        $('#familyForm input, #familyForm select').prop('disabled', true);
        $('#data_family_1').attr('hidden', true);

        // Hide the "Add more family member" link
        $('.add-school-family a').attr('hidden', true);
        $('.addFamilyMember a').attr('hidden', true);

        // Show the edit button and hide the cancel and save buttons
        $('#edit_btn_family').show();
        $('#cancel_btn_family').hide();
        $('#save_btn_family').hide();
        if ({{ $user->family->count() }} < 1) {
            $('#family-section').attr('hidden', true);
            $('#data_empty_id').removeAttr('hidden');
        }
    }

    function deleteFamilyMember() {
        // Get all family sections
        var familySections = $('.family-section');

        // Check if there is more than one family section
        if (familySections.length > 1) {
            // Remove the last family section
            familySections.last().remove();
        } else {
            // // If there is only one family section, clear its input values instead of removing it
            // familySections.find('input, select').val('');

            familySections.last().remove();
        }
    }

</script>
