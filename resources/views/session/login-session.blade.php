@extends('layouts.user_type.guest')

@section('content')

  <main class="main-content  mt-0">
    <section>
      <div class="page-header min-vh-100">
        <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">

              <div class="oblique position-absolute top-0 h-50 d-md-block d-none me-n7">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n7" style="background-image:url('../assets/img/background-half.jpg')"></div>
              </div>
              <div class="card card-plain mt-6">
                <div class="d-flex justify-content-center align-items-center">
                    <img src="../assets/img/logo.png" class="navbar-brand-img" alt="..." style="height: auto !important; max-height: 100%; width: 250px;">
                </div>
                <div class="d-flex justify-content-center align-items-center my-2">
                    <h3 class="font-weight-bolder text-info text-gradient">Customer Profiling</h3>
                </div>
                <div class="card-body">
                  <form role="form" method="POST" action="/session" id="loginForm">
                    @csrf
                    <label>Email</label>
                    <div class="mb-3">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="" aria-label="Email" aria-describedby="email-addon">
                        @error('email')
                        <p class="text-danger text-xs mt-2">{{ $message }}</p>
                        @enderror
                    </div>
                    <label>Password</label>
                    <div class="mb-3">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="" aria-label="Password" aria-describedby="password-addon">
                        @error('password')
                        <p class="text-danger text-xs mt-2">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="rememberMe" checked="">
                        <label class="form-check-label" for="rememberMe">Remember me</label>
                    </div>
                    <div class="text-center">
                        <button type="button" id="signInButton" class="btn bg-gradient-info w-100 mt-4 mb-0">Sign in</button>
                    </div>
                  </form>
                </div>
                <div class="card-footer text-center pt-0 px-lg-2 px-1">
                <small class="text-muted">Forgot you password? Reset you password
                  <a href="/login/forgot-password" class="text-info text-gradient font-weight-bold">here</a>
                </small>
                  <p class="mb-4 text-sm mx-auto">
                    Don't have an account?
                    <a href="register" class="text-info text-gradient font-weight-bold">Sign up</a>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n7">
                <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n7" style="background-image:url('../assets/img/background-half.jpg')"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <!-- Terms and Conditions Modal -->
  <div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-60" role="document">
      <div class="modal-content">
        <div class="modal-header d-flex justify-content-center align-items-center position-relative" style="background: #0088A1;">
          <h5 class="modal-title" style="color: white" id="termsModalLabel">DISCLAIMER</h5>
        </div>
        <div class="modal-body">
          <p class="text-bold">
            Aplikasi ini adalah Milik PLN Icon Plus Bidang PLN - Penjualan dan Solusi Pelanggan
          </p>
          <p>
            Dengan menggunakan dan/atau login ke dalam aplikasi ini, Anda setuju untuk mematuhi semua syarat dan ketentuan yang berlaku. Setiap pengguna bertanggung jawab untuk menjaga kerahasiaan data dan informasi yang diperoleh atau diakses melalui aplikasi ini. Anda dilarang menyebarluaskan, membagikan, atau mengungkapkan informasi apa pun yang diperoleh dari aplikasi ini kepada pihak manapun tanpa izin tertulis dari pihak yang berwenang.
          </p>
          <p>
            Penggunaan informasi yang melanggar ketentuan, termasuk namun tidak terbatas pada penyalahgunaan atau penyebaran data, dapat mengakibatkan tindakan hukum sesuai dengan peraturan perundang - undangan yang berlaku.
          </p>
          <p>
            Dengan melanjutkan penggunaan aplikasi ini, Anda menyatakan telah memahami dan setuju dengan ketentuan ini.
          </p>
        </div>
        <div class="modal-footer text-center d-flex justify-content-center align-items-center position-relative">
          <button type="button" class="btn btn-danger" style="margin-right: 50px; background: #0088A1" id="declineBtn">TIDAK SETUJU</button>
          <button type="button" class="btn btn-success" style="margin-left: 50px; background: #0088A1" id="acceptBtn">SETUJU</button>
        </div>
      </div>
    </div>
  </div>
  <script>
    document.getElementById('signInButton').addEventListener('click', function (e) {
        e.preventDefault();
        // Show the terms modal
        $('#termsModal').modal('show');
    });
  
    document.getElementById('acceptBtn').addEventListener('click', function () {
        // User accepts terms, proceed with form submission
        $('#termsModal').modal('hide');
        document.getElementById('loginForm').submit();
    });
  
    document.getElementById('declineBtn').addEventListener('click', function () {
        // User declines terms, hide the modal and don't submit the form
        $('#termsModal').modal('hide');
        alert('You must accept the terms and conditions to log in.');
    });
  </script>
  <style>
    .modal-60 {
        width: 60%; /* Set width to 80% of viewport width */
        max-width: 60vw; /* Ensure it scales with viewport width */
    }

    .modal-dialog {
        margin: 30px auto; /* Keep the default margin */
    }
  </style>
@endsection
