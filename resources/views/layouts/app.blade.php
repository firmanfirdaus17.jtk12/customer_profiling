<!-- app.blade.php -->
<!DOCTYPE html>

@if (\Request::is('rtl'))
<html dir="rtl" lang="ar">
@else
<html lang="en">
@endif

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">
  <title>
    Customer Profiling - PLN Icon Plus
  </title>
  <!--     Fonts and icons     -->
  <link href="{{ asset('assets/css/fonts.css') }}" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="{{ asset('assets/css/nucleo-icons.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/nucleo-svg.css') }}" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="{{ asset('assets/css/soft-ui-dashboard.css?v=1.0.5') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ asset('assets/css/jquery.orgchart.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/datepicker.min.css') }}">
  <link href="{{ asset('assets/css/styles.css?v=1.0.5') }}" rel="stylesheet" />
</head>

<body class="g-sidenav-show bg-gray-100 {{ (\Request::is('rtl') ? 'rtl' : (Request::is('virtual-reality') ? 'virtual-reality' : '')) }}" style="height: 100%">
  @auth
    @include('layouts.navbars.auth.sidebar-small')
    @yield('auth')
  @endauth
  @guest
    @yield('guest')
  @endguest

  <!-- Core JS Files -->
  <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
  <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/smooth-scrollbar.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/fullcalendar.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/buttons.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/orgchart.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/fontawesome.js') }}" crossorigin="anonymous"></script>

  @stack('dashboard')
  @stack('organization')
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>

  <!-- Github buttons -->
  <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{ asset('assets/js/soft-ui-dashboard.min.js?v=1.0.3') }}"></script>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
        const sidebar = document.getElementById('sidenav-small-main');
        const toggleButton = document.getElementById('sidebarToggle');

        // Toggle sidebar visibility on button click
        toggleButton.addEventListener('click', function(event) {
            sidebar.classList.toggle('show');
            event.stopPropagation(); // Prevent the click event from propagating to the document
        });

        // Close sidebar when clicking outside of it
        document.addEventListener('click', function(event) {
            if (sidebar.classList.contains('show') && !sidebar.contains(event.target) && !toggleButton.contains(event.target)) {
                sidebar.classList.remove('show');
            }
        });

        // Prevent sidebar from closing when clicking inside it
        sidebar.addEventListener('click', function(event) {
            event.stopPropagation(); // Prevent the click event from propagating to the document
        });
    });

  </script>
</body>

</html>
