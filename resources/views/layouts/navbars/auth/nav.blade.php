<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
    <div class="container-fluid py-1 px-3">
        {{-- <nav aria-label="breadcrumb">
            <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active text-capitalize" aria-current="page">{{ str_replace('-', ' ', Request::path()) }}</li>
            </ol>
            <h6 class="font-weight-bolder mb-0 text-capitalize">{{ str_replace('-', ' ', Request::path()) }}</h6>
        </nav> --}}
        <div class="collapse navbar-collapse d-flex" id="navbar">
            <ul class="navbar-nav col-12">
                <li class="nav-item dropdown pe-2 d-flex align-items-center col-6" style="margin-top:10px">
                    <button class="btn row d-xl-none" type="button" id="sidebarToggle" style="background-color: #ffffff; padding:8px; height:35px">
                        <i class="fas fa-bars" style="color: #00526B; "></i>
                    </button>
                    <img src="{{ asset('assets/img/logo.png') }}" class="navbar-brand-img d-xl-none" alt="..." style="margin-left:20px; margin-bottom:7px; height: auto !important; max-height: 35px; max-width: 400px;">
                </li>
                <li class="nav-item d-flex align-items-center justify-content-end col-3 offset-3" style="margin-bottom:12px">
                    <a href="{{ url('users', ['user_id' => auth()->user()->id]) }}" class="nav-link text-body font-weight-bold px-0">
                        <i class="fa fa-user fa-lg me-sm-1"></i>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</nav>
<!-- End Navbar -->
