<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-sidebar" id="sidenav-main" style="background-color: #4AAAC0;">
    <div class="">
        <div class="d-flex justify-content-center align-items-center p-2 pb-0" style="background-color: #055A6F; height: 100px;">
            <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
            <a class="align-items-center d-flex m-0 navbar-brand text-wrap" href="{{ route('dashboard') }}" style="width: 220px; height: 100px;">
                <img src="{{ asset('assets/img/logo_dark.png') }}" class="navbar-brand-img" alt="..." style="height: auto !important; max-height: 100%; width: 100%;">
            </a>
        </div>

        <hr class="mt-0 pt-3" style="background-color: #00526B;">
        <div class="collapse navbar-collapse w-auto" id="sidenav-collapse-main">
            <ul class="navbar-nav pt-3">
                <li class="nav-item">
                    <a class="nav-link {{ (Request::is('dashboard') ? 'active' : '') }}" href="{{ url('dashboard') }}">
                        <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                            <svg width="12px" height="12px" viewBox="0 0 42 42" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                <title>office</title>
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(-1869.000000, -293.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                        <g transform="translate(1716.000000, 291.000000)">
                                            <g id="office" transform="translate(153.000000, 2.000000)">
                                            <path class="color-background opacity-6" d="M12.25,17.5 L8.75,17.5 L8.75,1.75 C8.75,0.78225 9.53225,0 10.5,0 L31.5,0 C32.46775,0 33.25,0.78225 33.25,1.75 L33.25,12.25 L29.75,12.25 L29.75,3.5 L12.25,3.5 L12.25,17.5 Z"></path>
                                            <path class="color-background" d="M40.25,14 L24.5,14 C23.53225,14 22.75,14.78225 22.75,15.75 L22.75,38.5 L19.25,38.5 L19.25,22.75 C19.25,21.78225 18.46775,21 17.5,21 L1.75,21 C0.78225,21 0,21.78225 0,22.75 L0,40.25 C0,41.21775 0.78225,42 1.75,42 L40.25,42 C41.21775,42 42,41.21775 42,40.25 L42,15.75 C42,14.78225 41.21775,14 40.25,14 Z M12.25,36.75 L7,36.75 L7,33.25 L12.25,33.25 L12.25,36.75 Z M12.25,29.75 L7,29.75 L7,26.25 L12.25,26.25 L12.25,29.75 Z M35,36.75 L29.75,36.75 L29.75,33.25 L35,33.25 L35,36.75 Z M35,29.75 L29.75,29.75 L29.75,26.25 L35,26.25 L35,29.75 Z M35,22.75 L29.75,22.75 L29.75,19.25 L35,19.25 L35,22.75 Z"></path>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <span class="nav-link-text ms-1 {{ (Request::is('dashboard') ? 'text-dark' : 'text-white') }}">Dashboard</span>
                      </a>
                  </li>

                  <li class="nav-item pb-2">
                      <a class="nav-link {{ (Request::is('organization') ? 'active' : '') }}" href="{{ url('organization') }}">
                          <div class="icon icon-shape icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
                              <i style="font-size: 1rem;" class="fas fa-lg fa-list-ul ps-2 pe-2 text-center text-dark {{ (Request::is('organization') ? 'text-white' : 'text-dark') }} " aria-hidden="true"></i>
                          </div>
                          <span class="nav-link-text ms-1 {{ (Request::is('organization') ? 'text-dark' : 'text-white') }}">Organization</span>
                      </a>
                  </li>
              </ul>
          </div>

          <div class="w-auto pt-6" id="sidenav-collapse-main">
              <ul class="navbar-nav">
                  <li class="nav-item pb-6" style="border-radius:10px">
                      <a class="nav-link active d-flex justify-content-center align-items-center " href="{{ url('logout')}}" style="background-color: #f4f7f8">
                          <div class="icon icon-shape icon-sm shadow border-radius-md  text-center me-2 d-flex align-items-center justify-content-center" style="background-color: #009DB6; border-radius:10px">
                              <i style="font-size: 1rem; transform: rotate(180deg);" class="fas fa-sign-out-alt text-center text-dark text-white" aria-hidden="true"></i>
                          </div>
                          <span class="nav-link-text ms-1 ps-2 pe-2">Logout</span>
                      </a>
                  </li>
              </ul>
          </div>
      </div>
  </aside>
