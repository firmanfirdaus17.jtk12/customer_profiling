<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserImport implements ToModel, WithHeadingRow
{
    protected $users = [];

    public function model(array $row)
    {
        $user = new User([
            'fullname' => $row['fullname'],
            'email' => $row['email'],
            'password' => Hash::make('user123'),
        ]);
        $this->users[] = $user;
        return $user;
    }

    public function getUsers()
    {
        return $this->users;
    }
}
