<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupService extends Model
{
    use SoftDeletes;

    protected $table = 'group_service';
    protected $fillable = [
        'organization_id',
        'service_id',
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function service()
    {
        return $this->belongsTo(service::class);
    }
}
