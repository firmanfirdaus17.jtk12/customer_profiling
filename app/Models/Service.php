<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    use SoftDeletes;

    protected $table = 'service';
    protected $fillable = [
        'name',
        'status',
    ];

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'group_service', 'service_id', 'organization_id')
                    ->withTimestamps();
    }
}
