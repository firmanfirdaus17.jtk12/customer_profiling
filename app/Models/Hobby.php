<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hobby extends Model
{
    use SoftDeletes;

    protected $table = 'hobby'; // Corrected table name

    protected $fillable = [
        'name',
        'status',
    ];
}
