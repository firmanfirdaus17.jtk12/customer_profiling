<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupCompany extends Model
{
    use SoftDeletes;

    protected $table = 'group_company';
    protected $fillable = [
        'name',
        'status',
    ];
}
