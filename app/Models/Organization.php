<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{

    protected $table = 'organization';
    protected $fillable = [
        'user_id',
        'company_id',
        'group_company_id',
        'departement_id',
        'area_id',
        'sub_area_id',
        'position',
        'start_year',
        'end_year'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function group_company()
    {
        return $this->belongsTo(GroupCompany::class);
    }

    // public function division()
    // {
    //     return $this->belongsTo(Division::class);
    // }

    public function departement()
    {
        return $this->belongsTo(Department::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function sub_area()
    {
        return $this->belongsTo(Subarea::class);
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'group_service', 'organization_id', 'service_id')
                    ->withTimestamps();
    }
}
