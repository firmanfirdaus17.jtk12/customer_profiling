<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    protected $table = 'department';
    protected $fillable = [
        'name',
        'status',
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
