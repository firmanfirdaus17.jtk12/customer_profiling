<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HobbyUser extends Model
{
    protected $table = 'user_hobby';

    protected $fillable = [
        'user_id',
        'hobby_id',
        'family_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hobby()
    {
        return $this->belongsTo(Hobby::class);
    }
}
