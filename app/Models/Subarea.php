<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subarea extends Model
{
    use SoftDeletes;

    protected $table = 'sub_area';
    protected $fillable = [
        'name',
        'status',
    ];
}
