<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialMediaUser extends Model
{
    protected $table = 'user_social_media';
    protected $fillable = [
        'user_id',
        'social_media_id',
        'value',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function socialMedia()
    {
        return $this->belongsTo(SocialMedia::class);
    }
}
