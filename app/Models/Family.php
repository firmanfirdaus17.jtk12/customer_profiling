<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Family extends Model
{
    use SoftDeletes;

    protected $table = 'family';
    protected $fillable = [
        'user_id',
        'fullname',
        'nickname',
        'family_status',
        'place_birth',
        'date_birth',
        'work',
        'school',
        'school_2',
        'school_3',
        'hobby',
        'gender',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
