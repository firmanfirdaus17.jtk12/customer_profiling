<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyArea extends Model
{
    use SoftDeletes;

    protected $table = 'company_area_subarea';
    protected $fillable = [
        'company_id',
        'area_id',
        'sub_area_id',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function subarea()
    {
        return $this->belongsTo(Subarea::class);
    }
}
