<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'username',
        'role',
        'fullname',
        'nickname',
        'place_birth',
        'date_birth',
        'religion',
        'phone',
        'phone_2',
        'phone_3',
        'gender',
        'status_marital',
        'character',
        'favorite_food',
        'favorite_drink',
        'favorite_color',
        'allergy',
        'school',
        'school_2',
        'school_3',
        'ethnic',
        'address',
        'direct_officer',
        'status',
        'email',
        'password',
        'nip',
        'knowledge',
        'photo'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function families()
    {
        return $this->hasMany(Family::class, 'family_user_id');
    }

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function socialMediaAccounts()
    {
        return $this->belongsToMany(SocialMedia::class, 'user_social_media', 'user_id', 'social_media_id')
                    ->select('user_social_media.id', 'social_media_id', 'value')
                    ->withTimestamps();
    }


    public function hobbies()
    {
        return $this->hasMany(HobbyUser::class);
    }

    public function directOfficer()
    {
        return $this->belongsTo(User::class, 'direct_officer');
    }
}
