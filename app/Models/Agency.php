<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    use SoftDeletes;

    protected $table = 'agency';
    protected $fillable = [
        'name',
        'status',
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
