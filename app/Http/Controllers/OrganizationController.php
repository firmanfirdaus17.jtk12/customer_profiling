<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Ethnic;
use App\Models\GroupCompany;
use App\Models\Hobby;
use App\Models\HobbyUser;
use App\Models\Organization;
use App\Models\SocialMedia;
use App\Models\SocialMediaUser;
use App\Models\Subarea;
use Carbon\Carbon;
use DB;

class OrganizationController extends Controller
{

    public function index()
    {
        $data['users'] = User::orderBy('fullname', 'asc')->limit(20)->pluck('fullname', 'id')->toArray();
        $data['area_options'] = Area::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['sub_area_options'] = Subarea::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        return view('organization', $data);
    }


    public function user(Request $request)
    {
        // Retrieve the user ID from the request
        $user_id = $request->input('userId');
        $count = 1;
        $countPosition = 0;
        $user = User::find($user_id);

        // Current User's Direct Officer
        if (isset($user->direct_officer)) {
            $user_direct_officer = User::find($user->direct_officer);
            $organizationr_direct_officer = Organization::select(
                                    'organization.start_year',
                                    'organization.end_year',
                                    'organization.position',
                                    'area.name as area',
                                    'sub_area.name as subarea',
                                    'company.name as company'

                                )
                                ->where('user_id', $user_direct_officer->id)
                                ->leftJoin('area', 'organization.area_id', '=', 'area.id')
                                ->leftJoin('sub_area', 'organization.sub_area_id', '=', 'sub_area.id')
                                ->leftJoin('company', 'organization.company_id', '=', 'company.id')
                                ->orderBy('organization.start_year', 'desc')
                                ->first();

            $user_info_direct_officer = [
                'userId' => isset($user_direct_officer->id) ? $user_direct_officer->id : null,
                'id' => $count,
                'parentId' => $countPosition, // Adjust this according to your user model
                'name' => isset($user_direct_officer->fullname) ? $user_direct_officer->fullname : null,
                'phone' => isset($user_direct_officer->phone) ? $user_direct_officer->phone : null,
                'email' => isset($user_direct_officer->email) ? $user_direct_officer->email : null,
                'company' => isset($organizationr_direct_officer->company) ? $organizationr_direct_officer->company : null,
                'position' => isset($organizationr_direct_officer->position) ? $organizationr_direct_officer->position : null,
                'area' => isset($organizationr_direct_officer->area) ? $organizationr_direct_officer->area : null,
                'subarea' => isset($organizationr_direct_officer->subarea) ? $organizationr_direct_officer->subarea : null,
                'photo' => isset($user_direct_officer->photo) ? asset($user_direct_officer->photo) : asset('assets/img/people_gray.png'),
            ];
            $users[]=$user_info_direct_officer;
            $count++;
            $countPosition++;
        }

        // Current User
        $organization = Organization::select(
            'organization.start_year',
            'organization.end_year',
            'organization.position',
            'area.name as area',
            'sub_area.name as subarea',
            'company.name as company'

        )
        ->where('user_id', $user_id)
        ->leftJoin('area', 'organization.area_id', '=', 'area.id')
        ->leftJoin('sub_area', 'organization.sub_area_id', '=', 'sub_area.id')
        ->leftJoin('company', 'organization.company_id', '=', 'company.id')
        ->orderBy('organization.start_year', 'desc')
        ->first();

        $user_info = [
            'userId' => isset($user->id) ? $user->id : null,
            'id' =>  $count,
            'parentId' => $countPosition, // Adjust this according to your user model
            'name' => isset($user->fullname) ? $user->fullname : null,
            'phone' => isset($user->phone) ? $user->phone : null,
            'email' => isset($user->email) ? $user->email : null,
            'company' => isset($organization->company) ? $organization->company : null,
            'position' => isset($organization->position) ? $organization->position : null,
            'area' => isset($organization->area) ? $organization->area : null,
            'subarea' => isset($organization->subarea) ? $organization->subarea : null,
            'photo' => isset($user->photo) ? asset($user->photo) : asset('assets/img/people_gray.png'),
        ];
        $users[]=$user_info;
        $count++;
        $countPosition++;


        // Current User's Member

        // Find the organization by its ID
        $members = User::with(['organizations.company', 'organizations.sub_area', 'organizations.area'])->where('direct_officer', '=', $user_id)->get();

        foreach ($members as $key => $value) {
            $userInfo = [
                'userId' => isset($value->id) ? $value->id : null,
                'id' => $count,
                'parentId' => $countPosition, // Adjust this according to your user model
                'name' => isset($value->fullname) ? $value->fullname : null,
                'phone' => isset($value->phone) ? $value->phone : null,
                'email' => isset($value->email) ? $value->email : null,
                'company' => isset($value['organizations'][0]['company']['name']) ? $value['organizations'][0]['company']['name'] : null,
                'position' => isset($value['organizations'][0]['position']) ? $value['organizations'][0]['position'] : null,
                'division' => isset($value['organizations'][0]['sub_area']['name']) ? $value['organizations'][0]['sub_area']['name'] : null,
                'area' => isset($value['organizations'][0]['area']['name']) ? $value['organizations'][0]['area']['name'] : null,
                'subarea' => isset($value['organizations'][0]['sub_area']['name']) ? $value['organizations'][0]['sub_area']['name'] : null,
                'photo' => isset($value->photo) ? asset($value->photo) : asset('assets/img/people_gray.png'),
            ];
            $users[]=$userInfo;
            $count++;
        }
        return response()->json(['users' => $users]);
    }

    public function userDivision(Request $request)
    {
        $companyId = $request->companyId;
        $areaId = $request->areaId;
        $subAreaId = $request->subAreaId;
        $usersInSubArea = User::join('organization', 'users.id', '=', 'organization.user_id')
            ->where('organization.company_id', $companyId)
            ->where('organization.area_id', $areaId)
            ->where('organization.sub_area_id', $subAreaId)
            ->select('users.*')
            ->get();

        $topLeaders = [];

        // Step 2: Iterasi setiap user dan cari direct_officer tertinggi
        foreach ($usersInSubArea as $user) {
            $currentUser = $user;
        
            // Lakukan pengecekan terus menerus ke direct_officer
            while ($currentUser && $currentUser->direct_officer) {
                $nextUser = User::join('organization', 'users.id', '=', 'organization.user_id')
                    ->where('organization.company_id', $companyId)
                    ->where('organization.area_id', $areaId)
                    ->where('organization.sub_area_id', $subAreaId)
                    ->where('users.id', $currentUser->direct_officer)
                    ->select('users.*')
                    ->first();
        
                // Jika tidak ditemukan direct_officer dalam sub-area yang sama, berhenti
                if (!$nextUser) {
                    break;
                }
        
                // Set currentUser ke direct_officer berikutnya
                $currentUser = $nextUser;
            }
        
            // Setelah keluar dari loop, pastikan $currentUser bukan null dan tambahkan ke topLeaders
            if ($currentUser) {
                $topLeaders[] = $currentUser;
            }
        }
        
        // Step 3: Filter untuk user yang unik (agar tidak ada duplikat)
        $topLeaders = collect($topLeaders)->unique('id');
            
        // Step 4: Membuat array JSON untuk chart
        $dataNodes = [];
        $idCounter = 1;

        // Helper function to recursively get subordinates
        function addSubordinates($userId, $level, $pid, &$dataNodes, &$idCounter) {
            // Tambahkan user ke dataNodes
            $user = User::with(['organizations.company', 'organizations.sub_area', 'organizations.area'])
                ->where('id', '=', $userId)
                ->first();

            $dataNodes[] = [
                'userId' => $user->id,
                'id' => $idCounter,
                'pid' => $pid,
                'Name' => isset($user->fullname) ? $user->fullname : null,
                'Phone' => isset($user->phone) ? $user->phone : null,
                'Email' => isset($user->email) ? $user->email : null,
                'Company' => isset($user['organizations'][0]['company']['name']) ? $user['organizations'][0]['company']['name'] : null,
                'Position' => isset($user['organizations'][0]['position']) ? $user['organizations'][0]['position'] : null,
                'division' => isset($user['organizations'][0]['sub_area']['name']) ? $user['organizations'][0]['sub_area']['name'] : null,
                'Area' => isset($user['organizations'][0]['area']['name']) ? $user['organizations'][0]['area']['name'] : null,
                'Subarea' => isset($user['organizations'][0]['sub_area']['name']) ? $user['organizations'][0]['sub_area']['name'] : null,
                'img' => isset($user->photo) ? asset($user->photo) : asset('assets/img/people_gray.png'),
            ];

            $currentId = $idCounter;
            $idCounter++;

            // Cari bawahannya
            $subordinates = User::where('direct_officer', $user->id)->get();

            foreach ($subordinates as $subordinate) {
                addSubordinates($subordinate->id, $level + 1, $currentId, $dataNodes, $idCounter);
            }
        }

        // Step 5: Masukkan setiap leader dan bawahan mereka
        foreach ($topLeaders as $leader) {
            addSubordinates($leader->id, 0, 0, $dataNodes, $idCounter);
        }
        $currentLength = count($dataNodes); // Menghitung panjang array saat ini

        // Jika panjang array lebih dari 200, ambil hanya 200 elemen pertama
        if ($currentLength > 200) {
            $dataNodes = array_slice($dataNodes, 0, 200);
        }

        return response()->json($dataNodes);
    }

    public function show($id)
    {
        $data['users'] = User::orderBy('fullname', 'asc')->limit(20)->pluck('fullname', 'id')->toArray();
        $data['area_options'] = Area::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['sub_area_options'] = Subarea::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['user_id'] = $id;
        $user_choose = User::find($id);
        $data['user_fullname'] = $user_choose['fullname'];
        return view('organization', $data);
    }


    public function destroy(Request $request)
    {
        $organization = $request->input('organization_id');

        $organization = Organization::find($organization);
        if ($organization) {
            $organization->delete();
        }

        // Return the Organization information as JSON response
         return redirect()->back()->with('success', 'Organization data deleted successfully.');
    }


}


?>
