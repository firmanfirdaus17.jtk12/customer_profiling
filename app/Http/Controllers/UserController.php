<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Company;
use App\Models\CompanyArea;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Ethnic;
use App\Models\Family;
use App\Models\GroupCompany;
use App\Models\Hobby;
use App\Models\HobbyUser;
use App\Models\Organization;
use App\Models\SocialMedia;
use App\Models\SocialMediaUser;
use App\Models\Subarea;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Laravel\Facades\Image;
use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Gd\Driver;


class UserController extends Controller
{

    public function index()
    {
        $users = User::limit(10)->get();
        return view('users.index', compact('users'));
    }

    public function create()
    {
        $data['ethnic_options'] = Ethnic::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['hobby_options'] = Hobby::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['personal_account_options'] = SocialMedia::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['area_options'] = [];
        $data['sub_area_options'] = [];
        $data['direct_officer_options'] = [];
        $data['group_company_options'] = GroupCompany::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('id', 'asc')->pluck('name', 'id')->toArray();
        $data['knowledge_options'] = [];
        return view('users.create', $data);
    }

    public function store(Request $request)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'fullname' => $request->tabName == 'profile' ? 'required|max:255' : 'nullable|max:255',
            'email' => $request->tabName == 'profile' ? 'required|email|max:255|unique:users,email' : 'nullable|max:255|unique:users,email',
            'username' => 'nullable|max:255',
            'role' => 'nullable|max:255',
            'nickname' => 'nullable|max:255',
            'place_birth' => 'nullable|max:255',
            'date_birth' => 'nullable',
            'religion' => 'nullable',
            'phone' => 'nullable|max:255',
            'phone_2' => 'nullable|max:255',
            'phone_3' => 'nullable|max:255',
            'school' => 'nullable|max:255',
            'school_2' => 'nullable|max:255',
            'school_3' => 'nullable|max:255',
            'hobby' => 'nullable|max:255',
            'hobby_2' => 'nullable|max:255',
            'hobby_3' => 'nullable|max:255',
            'ethnic' => 'nullable|max:255',
            'gender' => 'nullable|max:255',
            'status_marital' => 'nullable',
            'address' => 'nullable|max:255',
            'status' => 'nullable',
            'nip' => $request->tabName == 'profile' ? 'required|max:255|unique:users,nip' : 'nullable|max:255',
            'photo' => 'nullable|image'
        ]);
        foreach ($validatedData as $key => $value) {
            // Check if value of an element is null, then delete the element
            if ($value === null) {
                unset($validatedData[$key]);
            }
        }

        if ($request->tabName == 'profile' && isset($validatedData['date_birth'])) {
            $validatedData['date_birth'] = Carbon::createFromFormat('d-m-Y', $validatedData['date_birth'])->format('Y-m-d');
        }

        if ($request->tabName == 'profile') {
            $validatedData['phone'] = isset($validatedData['phone']) ? $validatedData['phone'] : null;
            $validatedData['phone_2'] = isset($validatedData['phone_2']) ? $validatedData['phone_2'] : null;
            $validatedData['phone_3'] = isset($validatedData['phone_3']) ? $validatedData['phone_3'] : null;
            $validatedData['school'] = isset($validatedData['school']) ? $validatedData['school'] : null;
            $validatedData['school_2'] = isset($validatedData['school_2']) ? $validatedData['school_2'] : null;
            $validatedData['school_3'] = isset($validatedData['school_3']) ? $validatedData['school_3'] : null;
        }

        if ($request->hasFile('photo')) {
            $image = $request->file('photo');

            // Check file size
            $imageSize = $image->getSize(); // Size in bytes
            if ($imageSize > 2 * 1024 * 1024) { // Check if size is greater than 2MB
                // Compress the image
                $manager = new ImageManager(Driver::class);
                $compressedImage = $manager->read($image); // 800 x 600

                // scale down to fixed height
                $compressedImage->scaleDown(height: 500);

                // // scale down to fixed width
                $compressedImage->scaleDown(width: 500); // 200 x 150

                // Save the compressed image
                $imageName = time().'.'.$image->getClientOriginalExtension();
                $compressedImage->save(public_path('photos/'.$imageName));

                $validatedData['photo'] = 'photos/'.$imageName;
            } else {
                // Move the image without compression
                $imageName = time().'.'.$image->extension();
                $image->move(public_path('photos'), $imageName);
                $validatedData['photo'] = 'photos/'.$imageName;
            }
        }

        // Create a new user record
        $user = User::create($validatedData);

        session()->flash('success', 'User created successfully.');
        // Redirect to the user detail page
        return redirect()->route('users.show', ['user' => $user->id]);
    }

    public function show(User $user)
    {
        if ($user->date_birth) {
            $user->date_birth = Carbon::createFromFormat('Y-m-d', $user->date_birth)->format('d-m-Y');
        }
        $data['user'] = User::with(['hobbies.hobby', 'socialMediaAccounts'])->find($user->id);

        foreach ($data['user']['hobbies'] as $key => $value) {
            if ($key == 0) {
                $data['user']['hobby'] = $value['hobby']['name'];
            } else {
                $data['user']['hobby_'.$key+1] = $value['hobby']['name'];
            }
        }
        $data['user']['family'] = Family::where('user_id', $user->id)->orderBy('id')->get();
        foreach ($data['user']['family'] as $key => $value) {
            if (isset($value->date_birth)) {
                $data['user']['family'][$key]['date_birth'] = Carbon::createFromFormat('Y-m-d', $value->date_birth)->format('d-m-Y');
            }
        }

        $data['user']['organization'] = Organization::with(['company', 'group_company', 'area', 'sub_area'])->where('user_id', $user->id)->orderBy('start_year', 'desc')->first();
        // Check if the result is null
        if ($data['user']['organization'] === null) {
            $data['user']['organization'] = [];
        }

        $data['ethnic_options'] = Ethnic::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['hobby_options'] = Hobby::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['personal_account_options'] = SocialMedia::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['area_options'] = [];
        $data['sub_area_options'] = [];
        $data['direct_officer_options'] = [];
        $data['group_company_options'] = GroupCompany::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('id', 'asc')->pluck('name', 'id')->toArray();
        $data['knowledge_options'] = [];
        $data['member_list'] = User::where('direct_officer', $user->id)->pluck('fullname', 'id')->toArray();

        $data['socialMediaAccountsFormat'] = [];
        foreach ($data['user']['socialMediaAccounts'] as $value) {
            // If the social_media_id is not yet in the result array, add it with an empty array for values
            if (!isset($data['socialMediaAccountsFormat'][$value['social_media_id']])) {
                $data['socialMediaAccountsFormat'][$value['social_media_id']] = [
                    'social_media_id' => $value['social_media_id'],
                    'value' => [],
                ];
            }

            // Add the value to the corresponding social_media_id
            $data['socialMediaAccountsFormat'][$value['social_media_id']]['value'][] = $value['value'];
        }
        // Re-index the array to start from 0 if needed
        $data['socialMediaAccountsFormat'] = array_values($data['socialMediaAccountsFormat']);

        return view('users.show', $data);
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'fullname' => $request->tabName == 'profile' ? 'required|max:255' : 'nullable|max:255',
            'email' => $request->tabName == 'profile' ? 'required|email|unique:users,email,' . $user->id : 'nullable|max:255',
            'username' => 'nullable|max:255',
            'role' => 'nullable|max:255',
            'nickname' => 'nullable|max:255',
            'place_birth' => 'nullable|max:255',
            'date_birth' => 'nullable',
            'religion' => 'nullable',
            'phone' => 'nullable|max:255',
            'phone_2' => 'nullable|max:255',
            'phone_3' => 'nullable|max:255',
            'gender' => 'nullable|max:255',
            'status_marital' => 'nullable',
            'character' => 'nullable|max:255',
            'favorite_food' => 'nullable|max:255',
            'favorite_drink' => 'nullable|max:255',
            'favorite_color' => 'nullable|max:255',
            'allergy' => 'nullable|max:255',
            'school' => 'nullable|max:255',
            'school_2' => 'nullable|max:255',
            'school_3' => 'nullable|max:255',
            'hobby' => 'nullable|max:255',
            'hobby_2' => 'nullable|max:255',
            'hobby_3' => 'nullable|max:255',
            'ethnic' => 'nullable|max:255',
            'address' => 'nullable|max:255',
            'direct_officer' => 'nullable|max:255',
            'status' => 'nullable',
            'knowledge' => 'nullable|max:255',
            'nip' => $request->tabName == 'profile' ? 'required|max:255|unique:users,nip,' . $user->id : 'nullable|max:255',
            'photo' => 'nullable|image'
        ]);
        foreach ($validatedData as $key => $value) {
            // Check if value of an element is null, then delete the element
            if ($value === null) {
                unset($validatedData[$key]);
            }
        }

        try {
            if (in_array($request->tabName, ['profile', 'personality'])) {
                if ($request->tabName == 'profile' && isset($validatedData['date_birth'])) {
                    $validatedData['date_birth'] = Carbon::createFromFormat('d-m-Y', $validatedData['date_birth'])->format('Y-m-d');
                }

                if ($request->hasFile('photo')) {
                    $image = $request->file('photo');

                    // Check file size
                    $imageSize = $image->getSize(); // Size in bytes
                    if ($imageSize > 2 * 1024 * 1024) { // Check if size is greater than 2MB
                        // Compress the image
                        $manager = new ImageManager(Driver::class);
                        $compressedImage = $manager->read($image); // 800 x 600

                        // scale down to fixed height
                        $compressedImage->scaleDown(height: 500);

                        // // scale down to fixed width
                        $compressedImage->scaleDown(width: 500); // 200 x 150

                        // Save the compressed image
                        $imageName = time().'.'.$image->getClientOriginalExtension();
                        $compressedImage->save(public_path('photos/'.$imageName));

                        $validatedData['photo'] = 'photos/'.$imageName;
                    } else {
                        // Move the image without compression
                        $imageName = time().'.'.$image->extension();
                        $image->move(public_path('photos'), $imageName);
                        $validatedData['photo'] = 'photos/'.$imageName;
                    }
                }

                // Update the user record
                $user->update($validatedData);

                if ($request->tabName == 'personality') {
                    $hobbies = ['hobby', 'hobby_2', 'hobby_3'];

                    // Delete existing record, make no duplicate
                    $existingRecord = HobbyUser::where('user_id', $user->id)
                                                ->delete();
                    // Loop through the hobbies array and save the hobby data
                    foreach ($hobbies as $hobbyKey) {
                        // Check if the key exists in the validated data
                        if (isset($validatedData[$hobbyKey])) {
                            // Add record
                            $hobbyUser = new HobbyUser();
                            $hobbyUser->user_id = $user->id;
                            $hobbyUser->hobby_id = $validatedData[$hobbyKey];
                            $hobbyUser->save();
                        }
                    }
                }
            }

            if ($request->tabName == 'personal_account') {
                $check_duplicate_app_array = [];
                foreach ($request->personal_account as $keyPA => $valuePA) {
                    if (in_array($valuePA, $check_duplicate_app_array) && $keyPA > 0) {
                        return redirect()->back()->with('failed', 'An error occurred while saving changes. Application names cannot be the same.');
                    }
                    $check_duplicate_app_array[] = $valuePA;
                }


                // Delete first the personnal account
                SocialMediaUser::where('user_id', $user->id)->delete();

                foreach ($request->personal_account as $keyPA => $valuePA) {
                    if ($valuePA !== null && isset($request->character[$keyPA]) && $request->character[$keyPA] !== null) {
                        foreach ($request->character[$keyPA] as $keyC => $valueC) {
                            if ($valueC !== null) {
                                // Create new data
                                $socialMediaUser = new SocialMediaUser();
                                $socialMediaUser->user_id = $user->id;
                                $socialMediaUser->social_media_id = $valuePA;
                                $socialMediaUser->value = $valueC;
                                $socialMediaUser->save();
                            }
                        }
                    }
                }
            }

            if ($request->tabName == 'organization_detail') {
                $organization_start_year = null;
                $organization_end_year = null;
                if (isset($request->start_year)) {
                    $organization_start_year = Carbon::createFromFormat('d-m-Y', $request->start_year)->format('Y-m-d');
                }
                if (isset($request->end_year)) {
                    $organization_end_year = Carbon::createFromFormat('d-m-Y', $request->end_year)->format('Y-m-d');
                }

                if (isset($request['id_organization']) && !empty($request['id_organization'])) {
                    // Update existing data
                    $organization = Organization::find($request['id_organization']);
                } else {
                    // Create new data
                    $organization = new Organization();
                    $organization->user_id = $user->id;
                }

                $organization->company_id = $request->company;
                $organization->group_company_id = $request->group_company;
                $organization->area_id = $request->area;
                $organization->sub_area_id = $request->sub_area;
                $organization->position = $request->position;
                $organization->start_year = $organization_start_year;
                $organization->end_year = $organization_end_year;
                $organization->direct_officer = $request->direct_officer;
                $organization->save();

                $user->update(['direct_officer' =>  $request->direct_officer]);
            }

            if ($request->tabName == 'family') {
                $familyData = $request->input('family');
                $familyIds = Family::where('user_id', $user->id)->pluck('id')->toArray();
                if (!isset($familyData)) {
                    // Delete family records for the user
                    Family::where('user_id', $user->id)->delete();
                } else {
                    // Melakukan loop untuk memperbarui detail keluarga pengguna sesuai data yang dikirimkan
                    foreach ($familyData as $key => $data) {
                        try {
                            if (isset($data['fullname']) && isset($data['family_status'])) {
                                $fam_date_birth = null;
                                if (isset($data['date_birth'])) {
                                    $fam_date_birth = Carbon::createFromFormat('d-m-Y', $data['date_birth'])->format('Y-m-d');
                                }

                                if (isset($data['id'])) {
                                    // Update existing data
                                    $family = Family::find($data['id']);
                                    if ($family) {
                                        // Remove the ID from the array of family IDs that need to be deleted
                                        $keyToDelete = array_search($family->id, $familyIds);
                                        if ($keyToDelete !== false) {
                                            unset($familyIds[$keyToDelete]);
                                        }
                                    }
                                } else {
                                    // Create new data
                                    $family = new Family();
                                    $family->user_id = $user->id;
                                }

                                $family->fullname = $data['fullname'];
                                $family->nickname = $data['nickname'];
                                $family->family_status = $data['family_status'];
                                $family->place_birth = $data['place_birth'];
                                $family->date_birth = $fam_date_birth;
                                $family->work = $data['work'];
                                $family->gender = $data['gender'];
                                $family->school = $data['school'];
                                $family->school_2 = isset($data['school_2']) ? $data['school_2'] : null;
                                $family->school_3 = isset($data['school_3']) ? $data['school_3'] : null;
                                $family->save();
                            }
                        } catch (\Throwable $th) {
                            //throw $th;
                        }
                    }
                    // Delete family records that were not updated or created in the current request
                    if (!empty($familyIds)) {
                        Family::whereIn('id', $familyIds)->delete();
                    }
                }
            }

            session()->flash('success', 'User updated successfully.');

            return redirect()->back()->with('success', 'User data updated successfully.');
        } catch (\Throwable $th) {
            session()->flash('failed', 'Error occurred while saving changes. Please try again later');

            // Return JSON response indicating failure
            return redirect()->back()->with('failed', 'Error occurred while saving changes.');
            // return redirect()->back()->with('failed', 'Error occurred while saving changes. ' . $th);
        }
    }

    public function destroy(User $user)
    {
        // Delete the user record
        $user->delete();

        // Redirect to the index page with success message
        // return redirect()->route('dashboard')->with('success', 'User deleted successfully.');
        return 'User deleted successfully.';
    }

    public function getDatatableData(Request $request)
    {
        $users = User::select(
                        'users.id',
                        'fullname',
                        'C.name as company',
                        'A.name as area',
                        'SA.name as sub_area',
                        'phone',
                        'email'
                    )
                    ->leftJoin('organization as O', function($join) {
                        $join->on('O.user_id', '=', 'users.id')
                            ->whereRaw('O.start_year = (select max(start_year) from organization where user_id = users.id)');
                    })
                    ->leftJoin('company as C', 'C.id', '=', 'O.company_id')
                    ->leftJoin('area as A', 'A.id', '=', 'O.area_id')
                    ->leftJoin('sub_area as SA', 'SA.id', '=', 'O.sub_area_id')
                    ->when(request('search'), function ($query) {
                        $query->where('fullname', 'like', '%' . request('search') . '%');
                    })
                    ->when(request('searchCompany'), function ($query) {
                        $query->where('O.company_id', request('searchCompany'));
                    })
                    ->when(request('searchArea'), function ($query) {
                        $query->where('O.area_id', request('searchArea'));
                    })
                    ->orderBy('users.id', 'desc')
                    ->paginate(10, ['*'], 'page', request('page'));

        $data = $users->map(function ($user) {
            $editUrl = route('users.show', $user->id);
            $deleteUrl = route('users.destroy', $user->id);

            return [
                'NO' => $user->id,
                'NAMA' => $user->fullname,
                'COMPANY' => $user->company,
                'AREA' => $user->area,
                'SUB_AREA' => $user->sub_area,
                'PHONE' => $user->phone,
                'EMAIL' => $user->email,
                'ACTION' => '<a href="' . $editUrl . '" class="mx-3" data-bs-toggle="tooltip" data-bs-original-title="Edit user"><i class="fas fa-eye text-secondary"></i></a>' .
                            '<a href="#" class="mx-3 delete-user" data-delete-url="' . $deleteUrl . '" data-bs-toggle="tooltip" data-bs-original-title="Delete user"><i class="cursor-pointer fas fa-trash text-secondary"></i></a>' // Assign the deleteUrl to the delete button
            ];
        });

        $pagination = $users->links('pagination::bootstrap-5')->toHtml();
        return response()->json(['data' => $data, 'pagination' => $pagination, 'req_page' => $request->page ? $request->page : 1]);
    }


    public function getOrganizationDatatableData(Request $request)
    {
        $userId = $request->input('userId');
        $organizations = Organization::select(
                'organization.id',
                'organization.start_year',
                'organization.end_year',
                'C.name as company',
                'organization.position',
                'A.name as area'
            )
            ->leftJoin('company as C', 'C.id', '=', 'organization.company_id')
            ->leftJoin('area as A', 'A.id', '=', 'organization.area_id')
            ->where('organization.user_id', '=', $userId)
            ->distinct()
            ->paginate(10);

        $data = [];

        foreach ($organizations as $organization) {
            $editUrl = route('users.organization_show', ['user_id' => $userId, 'organization_id' => $organization->id]);
            $deleteUrl = route('users.organization_destroy', ['user_id' => $userId, 'organization_id' => $organization->id]);

            $row = [
                'NO' => $organization->id,
                'START_YEAR' => $organization->start_year,
                'END_YEAR' => $organization->end_year,
                'COMPANY' => $organization->company,
                'POSITION' => $organization->position,
                'AREA' => $organization->area,
                'ACTION' => '<a href="#" class="mx-3 detail-organization" data-organization-id="' . $organization->id . '" data-bs-toggle="modal" data-bs-target="#organizationDetailModal" data-bs-toggle="tooltip" data-bs-placement="top" title="Detail Organization"><i class="fas fa-eye text-secondary"></i></a>' .
                            '<a href="#" class="mx-3 delete-organization" data-delete-url="' . $deleteUrl . '" data-bs-toggle="tooltip" data-bs-placement="top" title="Delete Organization"><i class="cursor-pointer fas fa-trash text-secondary"></i></a>'
            ];
            $data[] = $row;
        }

        return response()->json(['data' => $data]);
    }


    public function getHobbyChartData()
    {
        // Join HobbyUser table with Hobby table
        $hobbies = HobbyUser::select('hobby.name as hobby_name', DB::raw('count(*) as count'))
            ->join('hobby', 'user_hobby.hobby_id', '=', 'hobby.id')
            ->groupBy('user_hobby.hobby_id', 'hobby.name')
            ->get();

        // Convert the collection to a plain array
        $data = $hobbies->map(function ($hobby) {
            return [
                'label' => $hobby->hobby_name,
                'value' => $hobby->count
            ];
        });
        return response()->json($data);
    }

    public function organizationDetail($user_id, $organization_id)
    {
        $user = User::find($user_id);
        if ($user->date_birth) {
            $user->date_birth = Carbon::createFromFormat('Y-m-d', $user->date_birth)->format('d-m-Y');
        }
        $data['user'] = User::with(['hobbies.hobby', 'socialMediaAccounts'])->find($user->id);

        foreach ($data['user']['hobbies'] as $key => $value) {
            if ($key == 0) {
                $data['user']['hobby'] = $value['hobby']['name'];
            } else {
                $data['user']['hobby_'.$key+1] = $value['hobby']['name'];
            }
        }
        $data['user']['family'] = Family::where('user_id', $user->id)->orderBy('id')->get();
        foreach ($data['user']['family'] as $key => $value) {
            if (isset($value->date_birth)) {
                $data['user']['family'][$key]['date_birth'] = Carbon::createFromFormat('Y-m-d', $value->date_birth)->format('d-m-Y');
            }
        }

        $data['user']['organization'] = [];
        $data['ethnic_options'] = Ethnic::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['hobby_options'] = Hobby::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['personal_account_options'] = SocialMedia::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['area_options'] = Area::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['sub_area_options'] = Subarea::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['direct_officer_options'] = User::orderBy('fullname', 'asc')
        ->limit(20)
        ->pluck('fullname', 'id')
        ->except($user->id)
        ->toArray();
        $data['group_company_options'] = GroupCompany::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('id', 'asc')->pluck('name', 'id')->toArray();
        $data['user']['organization'] = Organization::findOrFail($organization_id);
        if ($data['user']['organization']['direct_officer'] != null) {
            $data['direct_officer_selected'] = User::find($data['user']['organization']['direct_officer']);
        }

        // You can return a view with organization details
        return view('users.organization_show', $data);
    }

    public function organizationCreate($user_id)
    {
        $user = User::find($user_id);
        if ($user->date_birth) {
            $user->date_birth = Carbon::createFromFormat('Y-m-d', $user->date_birth)->format('d-m-Y');
        }
        $data['user'] = User::with(['hobbies.hobby', 'socialMediaAccounts'])->find($user->id);

        foreach ($data['user']['hobbies'] as $key => $value) {
            if ($key == 0) {
                $data['user']['hobby'] = $value['hobby']['name'];
            } else {
                $data['user']['hobby_'.$key+1] = $value['hobby']['name'];
            }
        }
        $data['user']['family'] = Family::where('user_id', $user->id)->orderBy('id')->get();
        foreach ($data['user']['family'] as $key => $value) {
            if (isset($value->date_birth)) {
                $data['user']['family'][$key]['date_birth'] = Carbon::createFromFormat('Y-m-d', $value->date_birth)->format('d-m-Y');
            }
        }

        $data['user']['organization'] = [];
        $data['ethnic_options'] = Ethnic::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['hobby_options'] = Hobby::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['personal_account_options'] = SocialMedia::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['area_options'] = [];
        $data['sub_area_options'] = [];
        $data['direct_officer_options'] = User::orderBy('fullname', 'asc')
        ->limit(20)
        ->pluck('fullname', 'id')
        ->except($user->id)
        ->toArray();
        $data['group_company_options'] = GroupCompany::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('id', 'asc')->pluck('name', 'id')->toArray();

        // You can return a view with organization details
        return view('users.organization_show', $data);
    }


    public function organizationDestroy($user_id, $organization_id)
    {
        try {
            // Find the organization by its ID
            $organization = Organization::findOrFail($organization_id);

            // Delete the organization
            $organization->delete();

            // Return a success response
            session()->flash('success', 'User organization deleted successfully.');
            return response()->json(['message' => 'Organization deleted successfully'], 200);
        } catch (\Exception $e) {
            // Return an error response if any exception occurs
            return response()->json(['error' => 'Failed to delete organization'], 500);
        }
    }


    public function getUserMember(Request $request)
    {
        try {
            $user_id = $request->input('user_id');
            // Find the organization by its ID
            $members = User::where('direct_officer', '=', $user_id)->get();
            $user = User::find($user_id);
            return response()->json(['data' =>$members, 'target_user'  => $user ]);
        } catch (\Exception $e) {
            // Return an error response if any exception occurs
            return response()->json(['error' => 'User not found'], 500);
        }
    }

    public function getCompanyRelationOption(Request $request)
    {
        try {
            if ($request->input('company_id') !== null && $request->input('area_id') !== null && $request->input('sub_area_id') !== null) {
                $users_choosen = Organization::where('company_id', $request->input('company_id'))
                    ->where('area_id', $request->input('area_id'))
                    ->where('sub_area_id', $request->input('sub_area_id'))
                    ->pluck('user_id');

                $user_options = User::whereIn('id', $users_choosen)
                    ->orderBy('fullname', 'asc')
                    ->pluck('fullname', 'id');

                return response()->json(['user_options'=> $user_options ]);
            } elseif ($request->input('company_id') !== null && $request->input('area_id') !== null) {
                $area_id = $request->input('area_id');
                $company_id = $request->input('company_id');

                $sub_area_options = CompanyArea::select('SA.name as name', 'company_area_subarea.sub_area_id as id')
                        ->leftJoin('sub_area as SA', 'SA.id', '=', 'company_area_subarea.sub_area_id')
                        ->where('company_id', $company_id)
                        ->where('area_id', $area_id)
                        ->orderBy('SA.name', 'asc')
                        ->pluck('name', 'id')->toArray();
                return response()->json(['sub_area_options' =>$sub_area_options ]);
            } elseif ($request->input('company_id') !== null) {
                $company_id = $request->input('company_id');

                $area_options = CompanyArea::select('A.name as name', 'company_area_subarea.area_id as id')
                        ->leftJoin('area as A', 'A.id', '=', 'company_area_subarea.area_id')
                        ->where('company_id', $company_id)
                        ->orderBy('A.name', 'asc')
                        ->pluck('name', 'id')->toArray();
                $sub_area_options = CompanyArea::select('SA.name as name', 'company_area_subarea.sub_area_id as id')
                        ->leftJoin('sub_area as SA', 'SA.id', '=', 'company_area_subarea.sub_area_id')
                        ->where('company_id', $company_id)
                        ->orderBy('SA.name', 'asc')
                        ->pluck('name', 'id')->toArray();
                $group_company_options = Company::select('G.name as name', 'company.group_company_id as id')
                            ->leftJoin('group_company as G', 'G.id', '=', 'company.group_company_id')
                            ->where('company.id', $company_id)
                            ->orderBy('name', 'asc')
                            ->pluck('name', 'id')->toArray();
                return response()->json(['area_options' =>$area_options, 'sub_area_options' =>$sub_area_options, 'group_company_options'  => $group_company_options ]);
            } elseif ($request->input('user_id') !== null) {
                $user_id = $request->input('user_id');

                $organization = Organization::where('user_id', $user_id)
                        ->orderBy('start_year', 'desc')
                        ->first();
                $user_options = User::orderBy('fullname', 'asc')
                        ->limit(20)
                        ->pluck('fullname', 'id')->toArray();
                $company_options = Company::where('id', $organization->company_id)
                        ->pluck('name', 'id')->toArray();
                $area_options = Area::where('id', $organization->area_id)
                        ->pluck('name', 'id')->toArray();
                $sub_area_options = Subarea::where('id', $organization->sub_area_id)
                        ->pluck('name', 'id')->toArray();
                return response()->json(['area_options' =>$area_options, 'sub_area_options' =>$sub_area_options, 'company_options'  => $company_options, 'user_options'=> $user_options ]);
            }

        } catch (\Exception $e) {
            // Return an error response if any exception occurs
            return response()->json(['error' => 'Data not found'], 500);
        }
    }

    public function getUsers(Request $request)
    {
        $search = $request->input('search');
        $page = $request->input('page', 1);
        $perPage = 20;

        $query = User::query();

        if ($search) {
            $query->where('fullname', 'LIKE', '%' . $search . '%');
        }

        $users = $query->orderBy('fullname', 'asc')
                       ->paginate($perPage, ['*'], 'page', $page);

        $formattedResults = [];
        foreach ($users as $user) {
            $formattedResults[] = [
                'id' => $user->id,
                'text' => $user->fullname,
            ];
        }

        return response()->json([
            'results' => $formattedResults,
            'pagination' => [
                'more' => $users->hasMorePages()
            ]
        ]);
    }

     // New method for fetching a single user by ID
     public function getUserById($id)
     {
         $user = User::find($id);
         if ($user) {
             return response()->json([
                 'id' => $user->id,
                 'text' => $user->fullname,
             ]);
         }
         return response()->json(null, 404);
     }
}


?>
