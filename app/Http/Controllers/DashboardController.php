<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Organization;
use App\Models\User;
use App\Models\Area;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use DB;

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['user'] = Auth()->user();

        $data['users'] = User::paginate(10);
        $month = Carbon::now()->month;
        $day = Carbon::now()->day;
        $data['birthday'] = User::with('organizations.company')->whereMonth('date_birth', $month)
        ->whereDay('date_birth', $day)
        ->get();

        $tomorrowMonth = Carbon::tomorrow()->month;
        $tomorrowDay = Carbon::tomorrow()->day;

        // Retrieve users with date_birth same as tomorrow
        $data['next_birthday'] = User::with('organizations.company')->whereMonth('date_birth', $tomorrowMonth)
            ->whereDay('date_birth', $tomorrowDay)
            ->get();

        $company_users = Organization::join('company', 'organization.company_id', '=', 'company.id')
            ->select('company.name', DB::raw('count(organization.company_id) as user_count'))
            ->where(function ($query) {
                $query->whereNull('end_year')
                        ->orWhere('end_year', '>=', now());
            })
            ->groupBy('company.id', 'company.name')
            ->orderBy('company.name')
            ->get();

        $data['company_names'] = $company_users->pluck('name')->toArray();
        $data['company_count'] = $company_users->pluck('user_count')->toArray();
        $data['area_options'] = Area::orderBy('name', 'asc')->pluck('name', 'id')->toArray();
        $data['company_options'] = Company::orderBy('name', 'asc')->pluck('name', 'id')->toArray();

        return view('dashboard', $data);
    }

}
