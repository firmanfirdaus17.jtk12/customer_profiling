<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Area;
use Illuminate\Support\Facades\Schema;

class AreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete all existing areas
        if (Schema::hasTable('area')) {
            Area::truncate();
        }

        $areas = [
            "KANTOR PUSAT PLN",
            "PUSDIKLAT",
            "PUSERTIF",
            "PUSHARLIS",
            "PUSLITBANG",
            "PUSMANPRO",
            "UID ACEH",
            "UID BALI",
            "UID BANTEN",
            "UID JABAR",
            "UID JATENG & DIY",
            "UID JATIM",
            "UID JAYA",
            "UID KALBAR",
            "UID KALSELTENG",
            "UID KALTIMRA",
            "UID LAMPUNG",
            "UID RIAU & KEPRI",
            "UID S2JB",
            "UID SULSELRABAR",
            "UID SULUTTENGGO",
            "UID SUMBAR",
            "UID SUMUT",
            "UIK SUMBAGSEL",
            "UIK SUMBAGUT",
            "UIK TJB",
            "UIP JBB",
            "UIP JBT",
            "UIP JBTB",
            "UIP KALBAGBAR",
            "UIP KALBAGTIM",
            "UIP MALUKU & PAPUA",
            "UIP NUSRA",
            "UIP SULAWESI",
            "UIP SUMBAGSEL",
            "UIP SUMBAGTENG",
            "UIP SUMBAGUT",
            "UIP2B",
            "UIP3B KALIMANTAN",
            "UIP3B SULAWESI",
            "UIP3B SUMATERA",
            "UIT JBB",
            "UIT JBT",
            "UIT JBTB",
            "UIW BABEL",
            "UIW MMU",
            "UIW NTB",
            "UIW NTT",
            "UIW P2B",
            "KANTOR PUSAT PLN EG",
            "KANTOR PUSAT BAg",
            "KANTOR PUSAT PLN NPS",
            "KANTOR PUSAT PLN NPC",
            "KANTOR PUSAT PLN NR",
            "KANTOR PUSAT PLN SC",
            "KANTOR PUSAT MKP",
            "KANTOR PUSAT NII",
            "KANTOR PUSAT PLN ND",
            "KANTOR PUSAT PLN EPI",
            "KANTOR PUSAT PLN NP",
            "KANTOR PUSAT EMI",
            "KANTOR PUSAT MCTN",
            "KANTOR PUSAT PLN BATAM",
            "KANTOR PUSAT PLN E",
            "KANTOR PUSAT PLN IP",
            "REG 1 JABAR",
            "AL GARUT",
            "REG1 JABAR",
            "REG2 JATENG DIY",
            "REG 3 JATIM",
            "REG 6 RIAU & KEPRI",
            "REG 7 SUMBAGSEL",
            "AL KARAWANG",
            "AL PURWAKARTA",
            "AL MAJALAYA",
            "REG 5 DKI JAKARTA & BANTEN",
            "AL SBYSL & SDRJ",
            "AL SRKT & KLTEN",
            "AL BDG & CIMAHI",
            "REG 2 JATENG & DIY",
            "AL PEKANBARU",
            "AL RENGAT",
            "AL TJG PINANG",
            "AL BANGKA",
            "AL CICARAS",
            "AL MENTENG",
            "AL TJG KARANG",
            "AL SOLOK",
            "AL PADANG",
            "AL PAYAKUMBUH",
            "AL BUKITTINGI",
            "REG4 SUMBAR",
            "AL BOJONEGORO",
            "KANTOR PUSAT HP",
            "AL SBBY BARAT",
            "AL GRESIK",
            "AL SBY UTARA",
            "REG3 JATIM",
            "AL SALATIGA",
            "AL YOGYAKARTA",
            "AL SEMARANG",
            "AL SKBM & CJR",
            "AL BOGOR",
            "KANTOR PUST HP",
            "AL RNTAU PRAPAT",
            "REG6 RIAU KEPRI",
            "AL BANTEN UTARA",
            "REG5 JKT BANTEN",
            "REG 4 SUMBAR"
        ];

        foreach ($areas as $area) {
            Area::create([
                'name' => $area,
                'status' => 1
            ]);
        }
    }
}
