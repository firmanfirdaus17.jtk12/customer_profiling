<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Imports\UserImport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ExcelUserSeeder extends Seeder
{
    public function run()
    {
        // Import users from Excel file
        $import = new UserImport();
        Excel::import($import, storage_path('app/users.xlsx'));

        // Get users from the import
        $users = $import->getUsers();

        // Insert users in bulk
        User::insert($users);
    }
}
