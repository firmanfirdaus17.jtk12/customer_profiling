<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;
use Illuminate\Support\Facades\Schema;

class CompanySeeder extends Seeder
{
    public function run()
    {
        // Delete all existing hobbies
        if (Schema::hasTable('company')) {
            Company::truncate();
        }

        $hobbies = [
            'PT PLN (Persero)',
            'PT PLN ENERGI GAS (PLN EG)',
            'PT PELAYARAN BAHTERA ADHIGUNA (PT BAg)',
            'PT PLN NUSANTARA POWER SERVICES (PLN NPS)',
            'PT PLN NUSANTARA POWER CONSTRUCTION (PLN NPC)',
            'PLN NUSANTARA RENEWABLES',
            'PRIMA LAYANAN NIAGA SUKU CADANG (PLN SC)',
            'MITRA KARYA PRIMA (MKP)',
            'NAVIGAT INNOVATIVE INDONESIA (NII)',
            'PT PLN NUSA DAYA (PLN ND)',
            'PT PLN ENERGI PRIMER INDONESIA (PLN EPI)',
            'PT PLN NUSANTARA POWER (PLN NP)',
            'PT ENERGY MANAGEMENT INDONESIA (EMI)',
            'PT MANDAU CIPTA TENAGA NUSANTARA (MCTN)',
            'PT PLN BATAM ',
            'PT PLN ENJINIRING (PLN E)',
            'PT PLN INDONESIA POWER (PLN IP)',
            'PT PLN HALEYORA POWER (PLN HP)'

        ];        

        foreach ($hobbies as $company) {
            Company::create([
                'name' => $company,
                'status' => 1,
            ]);
        }
    }
}
