<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Service;
use Illuminate\Support\Facades\Schema;

class ServiceSeeder extends Seeder
{
    public function run()
    {
        // Delete all existing Services
        if (Schema::hasTable('service')) {
            Service::truncate();
        }

        $services = [
            'AP2T',
            'CRM',
            'Aplikasi Subsidi DJK',
            'EPSO',
            'BPBL',
            'AP3T',
            'AIL',
            'P2APST',
            'ARENA',
            'AGO',
            'MIMS',
            'MRWI',
            'CCTR SYSTEM',
            'APKT',
            'VCC',
            'ACMT',
            'APKT MOBILE',
            'FSO',
            'ITO',
            'APN',
            'Charge IN',
            'PLN Mobile',
            'ISDS (Kompor Induksi)',
            'Chempion',
            'Checkmate',
            'ChesKP',
            'Web Portal Pelanggan',
            'AMR',
            'F12RB MMNE P2B Jawa Bali',
            'F12RB NEON P2B UIKL Sulawesi',
            'F12RB MMNE P3B Sumatera',
            'MDMS',
            'INFRA MDMS',
            'EPROC',
            'SMAR',
            'AVANGER',
            'DIGIPROC',
            'VMS',
            'EAM DISTIRBUSI',
            'EAM PEMBANGKIT',
            'EAM TRANSMISI',
            'ASET PROPERTI',
            'GIS KORPORAT',
            'BBO',
            'GBMO',
            'SI IPP',
            'PENGENDALIAN GAS',
            'MAPP',
            'E-ARSIP',
            'ENGINEERING CENTER',
            'ENGINEERING INFORMATION CENTER',
            'EBID DOC',
            'EIQC',
            'LIS',
            'ICOFR',
            'BPP',
            'MODIV',
            'L\'METALLICA',
            'AIR TAX',
            'GCG',
            'PLN DAILY',
            'DAMS',
            'Aplikasi EPPID',
            'FRA',
            'ECC',
            'SINOPSIS',
            'PLN CLICK',
            'BARISTA',
            'PORTAL HC',
            'Konsolidasi Data Kepegawaian',
            'WAVE',
            'MAXICO',
            'VIP',
            'AMS KORPORAT',
            'Website Korporat',
            'BFKO',
            'E-TRANSPORT',
            'E-MEETING',
            'PLN FIT',
            'FIX',
            'E-Budget',
            'Simpus',
            'SMARTER',
            'Inspekta',
            'CSMS',
            'Simlitbang',
            'WBS COS IDD',
            'KPKU',
            'Rekrutment',
            'Seleksi',
            'E-Insurance',
            'SIMLOAN',
            'PLN KITA',
            'SIMKPNAS',
            'ERBAS',
            'Dataling',
            'eSPPD',
            'HSSE Mobile',
            'Valiant',
            'AVMS',
            'PSAK71',
            'LMS',
            'SILM',
            'E-RUPTL',
            'Climate Click',
            'MOTION PMO',
            'PLN Cerdas',
            'VR LISDES GEOSPASIAL'
        ];

        foreach ($services as $appName) {
            Service::create([
                'name' => $appName,
                'status' => 1,
            ]);
        }
    }
}
