<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ethnic;
use Illuminate\Support\Facades\Schema;

class EthnicSeeder extends Seeder
{
    public function run()
    {
        // Delete all existing Ethnics
        if (Schema::hasTable('ethnic')) {
            Ethnic::truncate();
        }

        $Ethnics = [
            'JAWA',
            'SUNDA',
            'BATAK',
            'BETAWI',
            'DAYAK',
            'ASMAT',
            'BUGIS',
            'MADURA',
            'MINANG',
            'BADUY',
            'BALI',
            'AMBON',
            'GAYO',
            'TENGGER',
            'SASAK',
            'SUMBAWA',
            'FLORES',
            'TORAJA',
            'OSING',
            'MANDAR'
        ];

        foreach ($Ethnics as $appName) {
            Ethnic::create([
                'name' => $appName,
                'status' => 1,
            ]);
        }
    }
}
