<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    public function run()
    {
        // Delete all existing users
        if (Schema::hasTable('users')) {
            User::truncate();
        }

        $users = [
            [
                'fullname' => 'admin_full',
                'nickname' => 'admin_nick',
                'username' => 'admin',
                'role' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => password_hash('admin', PASSWORD_BCRYPT)
            ],
            [
                'fullname' => 'sales_full',
                'nickname' => 'sales_nick',
                'username' => 'sales',
                'role' => 'sales',
                'email' => 'sales@gmail.com',
                'password' => password_hash('sales', PASSWORD_BCRYPT)
            ]
        ];

        // Insert all users in bulk
        User::insert($users);
    }
}
