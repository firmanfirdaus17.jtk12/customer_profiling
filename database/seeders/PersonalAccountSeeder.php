<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SocialMedia;
use Illuminate\Support\Facades\Schema;

class PersonalAccountSeeder extends Seeder
{
    public function run()
    {
        // Delete all existing social media
        if (Schema::hasTable('social_media')) {
            SocialMedia::truncate();
        }

        $social_media = [
            'Facebook',
            'Twitter',
            'Instagram',
            'LinkedIn',
            'YouTube',
            'TikTok',
            'Snapchat',
            'Pinterest',
            'Reddit',
            'WhatsApp',
            'Telegram',
            'WeChat',
            'Line',
            'Viber',
            'Skype',
            'Tumblr',
            'Flickr',
            'Medium',
            'Twitch',
            'Clubhouse'
        ];

        foreach ($social_media as $appName) {
            SocialMedia::create([
                'name' => $appName,
                'status' => 1,
            ]);
        }
    }
}
