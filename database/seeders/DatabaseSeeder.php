<?php

namespace Database\Seeders;

use App\Models\Subarea;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $this->call(ServiceSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AreaSeeder::class);
        $this->call(EthnicSeeder::class);
        $this->call(PersonalAccountSeeder::class);
        $this->call(HobbySeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(SubAreaSeeder::class);

    }
}
