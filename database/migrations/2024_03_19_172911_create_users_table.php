<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->nullable();
            $table->string('role')->nullable();
            $table->string('fullname')->nullable();
            $table->string('nickname')->nullable();
            $table->string('place_birth')->nullable();
            $table->date('date_birth')->nullable();
            $table->string('religion')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone_2')->nullable();
            $table->string('phone_3')->nullable();
            $table->string('email')->unique();
            $table->string('photo')->unique();
            $table->string('gender')->nullable();
            $table->string('status_marital')->nullable();
            $table->string('character')->nullable();
            $table->string('school')->nullable();
            $table->string('school_2')->nullable();
            $table->string('school_3')->nullable();
            $table->string('ethnic')->nullable();
            $table->text('address')->nullable();
            $table->string('favorite_food')->nullable();
            $table->string('favorite_drink')->nullable();
            $table->string('favorite_color')->nullable();
            $table->string('allergy')->nullable();
            $table->integer('direct_officer')->nullable();
            $table->integer('status')->nullable();
            $table->string('password')->nullable();
            $table->string('nip')->nullable();
            $table->string('knowledge')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
